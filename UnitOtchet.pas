unit UnitOtchet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frxClass, frxPreview, frxDBSet, frxExportImage, StdCtrls,
  Buttons, frxExportPDF, frxExportXLS, frxExportRTF, frxExportXML;

type
  TFormOtchet = class(TForm)
    frxBMPExport1: TfrxBMPExport;
    frxPDFExport1: TfrxPDFExport;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    ComboBox1: TComboBox;
    BitBtn3: TBitBtn;
    frxJPEGExport1: TfrxJPEGExport;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormOtchet: TFormOtchet;

implementation

uses DM, UnitProsmotr;

{$R *.dfm}

procedure TFormOtchet.BitBtn1Click(Sender: TObject);
begin
  if ComboBox1.Text<>'' then
  begin
  case ComboBox1.ItemIndex of
  0: begin
  if FormOtchet.BitBtn2.Tag=0 then
  FormProsmotr.frxReport1.Export(frxPDFExport1)
  else
  FormProsmotr.frxReport2.Export(frxPDFExport1)
  end;
  1: begin
  if FormOtchet.BitBtn2.Tag=0 then
  FormProsmotr.frxReport1.Export(frxBMPExport1)
  else
  FormProsmotr.frxReport2.Export(frxBMPExport1);
  end;
  2: begin
  if FormOtchet.BitBtn2.Tag=0 then
  FormProsmotr.frxReport1.Export(frxJPEGExport1)
  else
  FormProsmotr.frxReport2.Export(frxJPEGExport1);
  end;
  end;
  end
  else
  MessageBox(Self.Handle, PChar('�������� ������'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TFormOtchet.BitBtn2Click(Sender: TObject);
begin
 FormProsmotr.Show;
 case BitBtn2.Tag of
 0: FormProsmotr.frxReport1.ShowReport;
 1: FormProsmotr.frxReport2.ShowReport;
 2: FormProsmotr.frxReport3.ShowReport;
 end;
end;

procedure TFormOtchet.BitBtn3Click(Sender: TObject);
begin
  ComboBox1.ItemIndex:=-1;
  FormOtchet.Close;
end;

end.
