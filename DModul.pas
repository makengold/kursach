unit DModul;

interface

uses
  SysUtils, Classes, IBStoredProc, DB, IBCustomDataSet, IBQuery, IBDatabase;

type
  TDMod = class(TDataModule)
    IBDatabase1: TIBDatabase;
    IBTransaction1: TIBTransaction;
    DS_Nedvizh: TDataSource;
    DS_Remont: TDataSource;
    DS_Vid_Nedvizh: TDataSource;
    DS_Sotrud: TDataSource;
    DS_Otdel: TDataSource;
    DS_Dolzhn: TDataSource;
    DS_Adres: TDataSource;
    DS_Sobit: TDataSource;
    Q_Nedvizh: TIBQuery;
    Q_Adres: TIBQuery;
    Q_Remont: TIBQuery;
    Q_Vid_Nedvizh: TIBQuery;
    Q_Sotrud: TIBQuery;
    Q_Otdel: TIBQuery;
    Q_Dolzhn: TIBQuery;
    Q_Sobit: TIBQuery;
    Nedvizh_Add: TIBStoredProc;
    Nedvizh_Upd: TIBStoredProc;
    Nedvizh_Del: TIBStoredProc;
    Adres_Add: TIBStoredProc;
    Adres_Upd: TIBStoredProc;
    Adres_Del: TIBStoredProc;
    Remont_Add: TIBStoredProc;
    Remont_Del: TIBStoredProc;
    Sotrud_Add: TIBStoredProc;
    Sotrud_Upd: TIBStoredProc;
    Sotrud_Del: TIBStoredProc;
    Otdel_Add: TIBStoredProc;
    Otdel_Upd: TIBStoredProc;
    Otdel_Del: TIBStoredProc;
    Sobit_Add: TIBStoredProc;
    Sobit_Upd: TIBStoredProc;
    Sobit_Del: TIBStoredProc;
    Q_NedvizhNAIMEN: TIBStringField;
    Q_NedvizhNAIMEN1: TIBStringField;
    Q_NedvizhDATA: TIBStringField;
    Q_NedvizhGOROD: TIBStringField;
    Q_NedvizhRAYON: TIBStringField;
    Q_NedvizhULITCA: TIBStringField;
    Q_NedvizhDOM: TIBStringField;
    Q_NedvizhID_NEDVIZH: TIntegerField;
    Q_NedvizhID_VID_NEDVIZH: TIntegerField;
    Q_NedvizhOBSH_PLOSHAD: TIBStringField;
    Q_NedvizhID_ADRESA: TIntegerField;
    Q_NedvizhCENA: TIntegerField;
    Q_NedvizhPLOSHAD_ADM_POMESHEN: TIBStringField;
    Q_NedvizhETAZH: TIntegerField;
    Q_NedvizhETAZHNOST: TIntegerField;
    Q_NedvizhPARKING: TIBStringField;
    Q_NedvizhSROK_EXPLUATAC: TIntegerField;
    Q_NedvizhID_REMONT: TIntegerField;
    Q_NedvizhNOM_KVARTIRI: TIntegerField;
    Q_NedvizhPOCHT_INDEX: TIBStringField;
    Q_AdresID_ADRESA: TIntegerField;
    Q_AdresGOROD: TIBStringField;
    Q_AdresRAYON: TIBStringField;
    Q_AdresULITCA: TIBStringField;
    Q_AdresDOM: TIBStringField;
    Q_RemontID_REMONT: TIntegerField;
    Q_RemontNAIMEN: TIBStringField;
    Q_RemontDATA: TIBStringField;
    Q_Vid_NedvizhID_VID_NEDVIZH: TIntegerField;
    Q_Vid_NedvizhNAIMEN: TIBStringField;
    Q_SotrudNAIMEN: TIBStringField;
    Q_SotrudNAIMEN1: TIBStringField;
    Q_SotrudRUKOVOD: TIBStringField;
    Q_SotrudID_SOTRUD: TIntegerField;
    Q_SotrudFAMILY: TIBStringField;
    Q_SotrudIMYA: TIBStringField;
    Q_SotrudOTCHESTVO: TIBStringField;
    Q_SotrudPOL: TIBStringField;
    Q_SotrudDATA_ROZHDEN: TIBStringField;
    Q_SotrudMESTO_ZHIT: TIBStringField;
    Q_SotrudID_DOLZH: TIntegerField;
    Q_SotrudSER_PASPORT: TIBStringField;
    Q_SotrudNOM_PASPORT: TIBStringField;
    Q_SotrudDATA_VIDACHI_PASPORT: TIBStringField;
    Q_SotrudKEM_VIDAN_PASPORT: TIBStringField;
    Q_SotrudAGRES_REG: TIBStringField;
    Q_SotrudID_OTDELA: TIntegerField;
    Q_SotrudDOM_TELEFON: TIBStringField;
    Q_SotrudRAB_TELEFON: TIBStringField;
    Q_OtdelID_OTDELA: TIntegerField;
    Q_OtdelNAIMEN: TIBStringField;
    Q_OtdelRUKOVOD: TIBStringField;
    Q_DolzhnID_DOLZH: TIntegerField;
    Q_DolzhnNAIMEN: TIBStringField;
    Q_SobitID_SOBIT: TIntegerField;
    Q_SobitNAIMEN: TIBStringField;
    Q_SobitTIP_DOCUM: TIBStringField;
    Q_SobitSODERJ_DOCUM: TIBStringField;
    Q_SobitDATA_SOBIT: TIBStringField;
    Q_SobitOTVETSTV_LICO: TIBStringField;
    Q_SobitID_SOTRUD: TIntegerField;
    Q_SobitFAMILY: TIBStringField;
    Q_SobitIMYA: TIBStringField;
    Q_SobitOTCHESTVO: TIBStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMod: TDMod;

implementation

{$R *.dfm}

end.
