object FormProsmotr: TFormProsmotr
  Left = 191
  Top = 127
  BorderStyle = bsDialog
  Caption = #1055#1088#1077#1076#1087#1088#1086#1089#1084#1086#1090#1088
  ClientHeight = 442
  ClientWidth = 975
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object frxPreview1: TfrxPreview
    Left = 0
    Top = 0
    Width = 975
    Height = 442
    Align = alClient
    OutlineVisible = False
    OutlineWidth = 120
    ThumbnailVisible = False
    UseReportHints = True
  end
  object frxReport1: TfrxReport
    Version = '4.9.31'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    Preview = frxPreview1
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42143.883876736100000000
    ReportOptions.LastChange = 42143.883876736100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 72
    Top = 40
    Datasets = <
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 41.574830000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          Left = 18.897650000000000000
          Top = 3.779530000000000000
          Width = 343.937230000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1038#1056#1111#1056#1105#1057#1027#1056#1109#1056#1108' '#1057#1027#1056#1109#1057#8218#1057#1026#1057#1107#1056#1169#1056#1029#1056#1105#1056#1108#1056#1109#1056#1030' '#1056#1111#1057#1026#1056#181#1056#1169#1056#1111#1057#1026#1056#1105#1057#1039#1057#8218#1056#1105#1057#1039' '#1056#1109#1057#8218':')
          ParentFont = False
        end
        object SysMemo1: TfrxSysMemoView
          Left = 325.039580000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8 = (
            '[DATE]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 18.897650000000000000
          Top = 26.456710000000000000
          Width = 393.071120000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
      object MasterData1: TfrxMasterData
        Height = 52.913420000000000000
        Top = 173.858380000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset1
        DataSetName = 'frxDBDataset1'
        RowCount = 0
        object frxDBDataset1FAMILY: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'FAMILY'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."FAMILY"]')
          ParentFont = False
        end
        object frxDBDataset1IMYA: TfrxMemoView
          Left = 120.944960000000000000
          Top = 3.779530000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'IMYA'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."IMYA"]')
          ParentFont = False
        end
        object frxDBDataset1OTCHESTVO: TfrxMemoView
          Left = 211.653680000000000000
          Top = 3.779530000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'OTCHESTVO'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."OTCHESTVO"]')
          ParentFont = False
        end
        object frxDBDataset1DATA_ROZHDEN: TfrxMemoView
          Left = 332.598640000000000000
          Top = 3.779530000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DATA_ROZHDEN'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."DATA_ROZHDEN"]')
          ParentFont = False
        end
        object frxDBDataset1NAIMEN: TfrxMemoView
          Left = 442.205010000000000000
          Top = 3.779530000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NAIMEN'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."NAIMEN"]')
          ParentFont = False
        end
        object frxDBDataset1NAIMEN1: TfrxMemoView
          Left = 551.811380000000000000
          Top = 3.779530000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NAIMEN1'
          DataSet = frxDBDataset1
          DataSetName = 'frxDBDataset1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset1."NAIMEN1"]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Left = 3.779530000000000000
          Top = 52.913420000000000000
          Width = 714.331170000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line3: TfrxLineView
          Left = 3.779530000000000000
          Height = 52.913420000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line7: TfrxLineView
          Left = 109.606370000000000000
          Height = 52.913420000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line9: TfrxLineView
          Left = 204.094620000000000000
          Height = 52.913420000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line11: TfrxLineView
          Left = 306.141930000000000000
          Height = 52.913420000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line13: TfrxLineView
          Left = 438.425480000000000000
          Height = 52.913420000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line15: TfrxLineView
          Left = 548.031850000000000000
          Height = 52.913420000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line17: TfrxLineView
          Left = 718.110700000000000000
          Height = 52.913420000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 287.244280000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 642.520100000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
        end
      end
      object Header1: TfrxHeader
        Height = 30.236240000000000000
        Top = 120.944960000000000000
        Width = 718.110700000000000000
        object Memo3: TfrxMemoView
          Left = 11.338590000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#164#1056#176#1056#1112#1056#1105#1056#187#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 120.944960000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#152#1056#1112#1057#1039)
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 211.653680000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#1115#1057#8218#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#1109)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 313.700990000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#8221#1056#176#1057#8218#1056#176' '#1057#1026#1056#1109#1056#182#1056#1169#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 442.205010000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#8221#1056#1109#1056#187#1056#182#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034)
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 551.811380000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#1115#1057#8218#1056#1169#1056#181#1056#187)
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = 3.779530000000000000
          Width = 714.331170000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 3.779530000000000000
          Top = 30.236240000000000000
          Width = 714.331170000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line18: TfrxLineView
          Left = 718.110700000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line6: TfrxLineView
          Left = 3.779530000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line8: TfrxLineView
          Left = 109.606370000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line10: TfrxLineView
          Left = 204.094620000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line12: TfrxLineView
          Left = 306.141930000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line14: TfrxLineView
          Left = 438.425480000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line16: TfrxLineView
          Left = 548.031850000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line19: TfrxLineView
          Left = 718.110700000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
      end
    end
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    DataSource = DMod.DS_Sotrud
    BCDToCurrency = False
    Left = 112
    Top = 40
  end
  object frxDBDataset2: TfrxDBDataset
    UserName = 'frxDBDataset2'
    CloseDataSource = False
    DataSource = DMod.DS_Sdelka
    BCDToCurrency = False
    Left = 112
    Top = 80
  end
  object frxReport2: TfrxReport
    Version = '4.9.31'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    Preview = frxPreview1
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42143.948164282400000000
    ReportOptions.LastChange = 42143.948164282400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 72
    Top = 80
    Datasets = <
      item
        DataSet = frxDBDataset2
        DataSetName = 'frxDBDataset2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 49.133890000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo11: TfrxMemoView
          Left = 18.897650000000000000
          Width = 291.023810000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1038#1056#1111#1056#1105#1057#1027#1056#1109#1056#1108' '#1056#1111#1057#1026#1056#1109#1056#1030#1056#1109#1056#1169#1056#1105#1056#1112#1057#8249#1057#8230' '#1057#1027#1056#1169#1056#181#1056#187#1056#1109#1056#1108' '#1056#1109#1057#8218':')
          ParentFont = False
        end
        object SysMemo1: TfrxSysMemoView
          Left = 275.905690000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8 = (
            '[DATE]')
          ParentFont = False
        end
        object Line10: TfrxLineView
          Left = 18.897650000000000000
          Top = 26.456710000000000000
          Width = 343.937230000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
      object MasterData1: TfrxMasterData
        Height = 37.795300000000000000
        Top = 173.858380000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset2
        DataSetName = 'frxDBDataset2'
        RowCount = 0
        object frxDBDataset2GOROD: TfrxMemoView
          Left = 68.031540000000000000
          Width = 321.260050000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'GOROD'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."GOROD"]')
          ParentFont = False
        end
        object frxDBDataset2DOM: TfrxMemoView
          Left = 264.567100000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DOM'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."DOM"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 60.472480000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1110'.')
          ParentFont = False
        end
        object frxDBDataset2ULITCA: TfrxMemoView
          Left = 181.417440000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'ULITCA'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."ULITCA"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 162.519790000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1057#1107#1056#187'.')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 241.889920000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1169#1056#1109#1056#1112)
          ParentFont = False
        end
        object frxDBDataset2NAIMEN: TfrxMemoView
          Left = 3.779530000000000000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NAIMEN'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."NAIMEN"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 3.779530000000000000
          Top = 22.677180000000000000
          Width = 714.331170000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line2: TfrxLineView
          Left = 3.779530000000000000
          Top = 22.677180000000000000
          Height = -45.354360000000000000
          ShowHint = False
          Diagonal = True
        end
        object Line3: TfrxLineView
          Left = 56.692950000000000000
          Top = 22.677180000000000000
          Height = -45.354360000000000000
          ShowHint = False
          Diagonal = True
        end
        object Line6: TfrxLineView
          Left = 321.260050000000000000
          Top = 22.677180000000000000
          Height = -45.354360000000000000
          ShowHint = False
          Diagonal = True
        end
        object frxDBDataset2FAMILY1: TfrxMemoView
          Left = 325.039580000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'FAMILY1'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."FAMILY1"]')
          ParentFont = False
        end
        object frxDBDataset2IMYA1: TfrxMemoView
          Left = 370.393940000000000000
          Width = 238.110390000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'IMYA1'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."IMYA1"]')
          ParentFont = False
        end
        object frxDBDataset2OTCHESTVO1: TfrxMemoView
          Left = 415.748300000000000000
          Width = 253.228510000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'OTCHESTVO1'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."OTCHESTVO1"]')
          ParentFont = False
        end
        object Line7: TfrxLineView
          Left = 483.779840000000000000
          Top = 22.677180000000000000
          Height = -45.354360000000000000
          ShowHint = False
          Diagonal = True
        end
        object frxDBDataset2FAMILY2: TfrxMemoView
          Left = 487.559370000000000000
          Width = 238.110390000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'FAMILY2'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."FAMILY2"]')
          ParentFont = False
        end
        object frxDBDataset2IMYA2: TfrxMemoView
          Left = 544.252320000000000000
          Width = 238.110390000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'IMYA2'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."IMYA2"]')
          ParentFont = False
        end
        object frxDBDataset2OTCHESTVO2: TfrxMemoView
          Left = 578.268090000000000000
          Width = 238.110390000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'OTCHESTVO2'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."OTCHESTVO2"]')
          ParentFont = False
        end
        object Line8: TfrxLineView
          Left = 631.181510000000000000
          Top = 22.677180000000000000
          Height = -45.354360000000000000
          ShowHint = False
          Diagonal = True
        end
        object frxDBDataset2CENA: TfrxMemoView
          Left = 634.961040000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CENA'
          DataSet = frxDBDataset2
          DataSetName = 'frxDBDataset2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset2."CENA"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 687.874460000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1057#1026#1057#1107#1056#177'.')
          ParentFont = False
        end
        object Line9: TfrxLineView
          Left = 718.110700000000000000
          Top = 22.677180000000000000
          Height = -45.354360000000000000
          ShowHint = False
          Diagonal = True
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 272.126160000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 642.520100000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
        end
      end
      object Header1: TfrxHeader
        Height = 22.677180000000000000
        Top = 128.504020000000000000
        Width = 718.110700000000000000
        object Memo5: TfrxMemoView
          Left = 154.960730000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#1106#1056#1169#1057#1026#1056#181#1057#1027)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 11.338590000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111)
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = 3.779530000000000000
          Width = 714.331170000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 3.779530000000000000
          Top = 22.677180000000000000
          Width = 714.331170000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo7: TfrxMemoView
          Left = 325.039580000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#164#1056#152#1056#1115' '#1056#1111#1057#1026#1056#1109#1056#1169#1056#176#1056#1030#1057#8224#1056#176)
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 487.559370000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#164#1056#152#1056#1115' '#1056#1111#1056#1109#1056#1108#1057#1107#1056#1111#1056#176#1057#8218#1056#181#1056#187#1057#1039)
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 634.961040000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#166#1056#181#1056#1029#1056#176)
          ParentFont = False
        end
      end
    end
  end
  object frxReport3: TfrxReport
    Version = '4.9.31'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    Preview = frxPreview1
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42144.436688599500000000
    ReportOptions.LastChange = 42144.529546620400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 72
    Top = 128
    Datasets = <
      item
        DataSet = frxDBDataset3
        DataSetName = 'frxDBDataset3'
      end
      item
        DataSet = frxDBDataset4
        DataSetName = 'frxDBDataset4'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 997.795920000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object frxDBDataset3OTVETSTV_LICO: TfrxMemoView
          Left = 234.330860000000000000
          Top = 56.692950000000000000
          Width = 400.630180000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DataField = 'OTVETSTV_LICO'
          DataSet = frxDBDataset3
          DataSetName = 'frxDBDataset3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset3."OTVETSTV_LICO"]')
          ParentFont = False
        end
        object frxDBDataset3TIP_DOCUM: TfrxMemoView
          Left = 328.819110000000000000
          Top = 222.992270000000000000
          Width = 120.944960000000000000
          Height = 30.236240000000000000
          ShowHint = False
          DataField = 'TIP_DOCUM'
          DataSet = frxDBDataset3
          DataSetName = 'frxDBDataset3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDBDataset3."TIP_DOCUM"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 192.756030000000000000
          Top = 249.448980000000000000
          Width = 362.834880000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1109' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#181' '#1057#1026#1056#176#1056#177#1056#1109#1057#8218#1056#1029#1056#1105#1056#1108#1056#176' '#1056#1029#1056#176' '#1057#1026#1056#176#1056#177#1056#1109#1057#8218#1057#1107)
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 45.354360000000000000
          Top = 37.795300000000000000
          Width = 124.724490000000000000
          Height = 52.913420000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1111#1056#1105#1057#1039' '#1056#1030#1056#181#1057#1026#1056#1029#1056#176
            #1056#160#1057#1107#1056#1108#1056#1109#1056#1030#1056#1109#1056#1169#1056#1105#1057#8218#1056#181#1056#187#1057#1034)
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 151.181200000000000000
          Top = 75.590600000000000000
          Width = 75.590600000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo4: TfrxMemoView
          Left = 166.299320000000000000
          Top = 75.590600000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1111#1056#1109#1056#1169#1056#1111#1056#1105#1057#1027#1057#1034)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 45.354360000000000000
          Top = 151.181200000000000000
          Width = 204.094620000000000000
          Height = 64.252010000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            #1056#160#1056#1105#1057#1036#1056#187#1057#8218#1056#181#1057#1026#1057#1027#1056#1108#1056#176#1057#1039' '#1057#8222#1056#1105#1057#1026#1056#1112#1056#176' '
            '   '#1042#171'La vie est belle'#1042#187)
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 517.795610000000000000
          Top = 151.181200000000000000
          Width = 128.504020000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#176#1057#8218#1056#176' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object SysMemo1: TfrxSysMemoView
          Left = 517.795610000000000000
          Top = 181.417440000000000000
          Width = 128.504020000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DATE]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 336.378170000000000000
          Top = 317.480520000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1029#1057#1039#1057#8218#1057#1034' '#1056#1029#1056#176' '#1057#1026#1056#176#1056#177#1056#1109#1057#8218#1057#1107)
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 506.457020000000000000
          Top = 317.480520000000000000
          Width = 30.236240000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#1027)
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 506.457020000000000000
          Top = 340.157700000000000000
          Width = 30.236240000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1111#1056#1109)
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 536.693260000000000000
          Top = 317.480520000000000000
          Width = 154.960730000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 536.693260000000000000
          Top = 340.157700000000000000
          Width = 154.960730000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object SysMemo2: TfrxSysMemoView
          Left = 559.370440000000000000
          Top = 317.480520000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[DATE]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 536.693260000000000000
          Top = 294.803340000000000000
          Width = 154.960730000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#176#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 309.921460000000000000
          Top = 408.189240000000000000
          Width = 245.669450000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1057#8222#1056#176#1056#1112#1056#1105#1056#187#1056#1105#1057#1039', '#1056#1105#1056#1112#1057#1039', '#1056#1109#1057#8218#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#1109)
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 207.874150000000000000
          Top = 457.323130000000000000
          Width = 328.819110000000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            
              #1056#1030' '#1056#1109#1057#8218#1056#1108#1057#1026#1057#8249#1057#8218#1056#1109#1056#181' '#1056#176#1057#1107#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#181#1057#1026#1056#1029#1056#1109#1056#181' '#1056#1109#1056#177#1057#8240#1056#181#1057#1027#1057#8218#1056#1030#1056#1109' "L' +
              'a vie est belle"')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 302.362400000000000000
          Top = 498.897960000000000000
          Width = 241.889920000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1057#1027#1057#8218#1057#1026#1057#1107#1056#1108#1057#8218#1057#1107#1057#1026#1056#1029#1056#1109#1056#181' '#1056#1111#1056#1109#1056#1169#1057#1026#1056#176#1056#183#1056#1169#1056#181#1056#187#1056#181#1056#1029#1056#1105#1056#181)
          ParentFont = False
        end
        object frxDBDataset4NAIMEN: TfrxMemoView
          Left = 177.637910000000000000
          Top = 536.693260000000000000
          Width = 400.630180000000000000
          Height = 37.795300000000000000
          ShowHint = False
          DataField = 'NAIMEN'
          DataSet = frxDBDataset4
          DataSetName = 'frxDBDataset4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDBDataset4."NAIMEN"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 279.685220000000000000
          Top = 559.370440000000000000
          Width = 260.787570000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#1169#1056#1109#1056#187#1056#182#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ('#1057#1027#1056#1111#1056#181#1057#8224#1056#1105#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', '#1056#1111#1057#1026#1056#1109#1057#8222#1056#181#1057#1027#1057#1027#1056#1105 +
              #1057#1039')')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 249.448980000000000000
          Top = 600.945270000000000000
          Width = 257.008040000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1029#1056#176' '#1056#1111#1056#1109#1057#1027#1057#8218#1056#1109#1057#1039#1056#1029#1056#1029#1057#1107#1057#1035' '#1057#1026#1056#176#1056#177#1056#1109#1057#8218#1057#1107)
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 264.567100000000000000
          Top = 623.622450000000000000
          Width = 291.023810000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#176' '#1056#1029#1056#176' '#1057#1026#1056#176#1056#177#1056#1109#1057#8218#1057#1107', '#1057#8230#1056#176#1057#1026#1056#176#1056#1108#1057#8218#1056#181#1057#1026' ' +
              #1057#1026#1056#176#1056#177#1056#1109#1057#8218#1057#8249)
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 45.354360000000000000
          Top = 672.756340000000000000
          Width = 449.764070000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            
              #1057#1027' '#1056#1109#1056#1108#1056#187#1056#176#1056#1169#1056#1109#1056#1112'                              '#1057#1026#1057#1107#1056#177#1056#187#1056#181#1056#8470', '#1056#1108#1056 +
              #1109#1056#1111#1056#181#1056#181#1056#1108)
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 60.472480000000000000
          Top = 718.110700000000000000
          Width = 498.897960000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            
              #1056#1169#1056#1109#1056#1111#1056#187#1056#176#1057#8218#1056#1109#1056#8470'                              '#1056#1109#1057#8218' '#1056#1169#1056#1109#1056#187#1056#182#1056#1029#1056#1109#1057 +
              #1027#1057#8218#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#1109#1056#1108#1056#187#1056#176#1056#1169#1056#176)
          ParentFont = False
        end
        object Line2: TfrxLineView
          Left = 177.637910000000000000
          Top = 695.433520000000000000
          Width = 143.622140000000000000
          ShowHint = False
          Diagonal = True
        end
        object Line3: TfrxLineView
          Left = 177.637910000000000000
          Top = 740.787880000000000000
          Width = 143.622140000000000000
          ShowHint = False
          Diagonal = True
        end
        object Memo22: TfrxMemoView
          Left = 219.212740000000000000
          Top = 695.433520000000000000
          Width = 241.889920000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1057#8224#1056#1105#1057#8222#1057#1026#1056#176#1056#1112#1056#1105)
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 234.330860000000000000
          Top = 740.787880000000000000
          Width = 241.889920000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '(%)')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 56.692950000000000000
          Top = 771.024120000000000000
          Width = 343.937230000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#1027#1056#1029#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181': '#1056#1118#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#1109#1056#8470' '#1056#1169#1056#1109#1056#1110#1056#1109#1056#1030#1056#1109#1057#1026' '#1056#1109#1057#8218' ')
          ParentFont = False
        end
        object SysMemo3: TfrxSysMemoView
          Left = 374.173470000000000000
          Top = 771.024120000000000000
          Width = 105.826840000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[DATE]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 56.692950000000000000
          Top = 831.496600000000000000
          Width = 283.464750000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#8218#1056#1030#1056#181#1057#8218#1057#1027#1057#8218#1056#1030#1056#181#1056#1029#1056#1029#1056#1109#1056#181' '#1056#187#1056#1105#1057#8224#1056#1109' '#1056#1109#1057#8218#1056#1169#1056#181#1056#187#1056#176)
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = 351.496290000000000000
          Top = 850.394250000000000000
          Width = 102.047310000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo26: TfrxMemoView
          Left = 362.834880000000000000
          Top = 850.394250000000000000
          Width = 241.889920000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#187#1056#1105#1057#8225#1056#1029#1056#176#1057#1039' '#1056#1111#1056#1109#1056#1169#1056#1111#1056#1105#1057#1027#1057#1034)
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 18.897650000000000000
          Top = 876.850960000000000000
          Width = 309.921460000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#1027' '#1056#1111#1057#1026#1056#1105#1056#1108#1056#176#1056#183#1056#1109#1056#1112' '#1057#1026#1056#176#1056#177#1056#1109#1057#8218#1056#1029#1056#1105#1056#1108' '#1056#1109#1056#183#1056#1029#1056#176#1056#1108#1056#1109#1056#1112#1056#187#1056#181#1056#1029)
          ParentFont = False
        end
        object Line5: TfrxLineView
          Left = 336.378170000000000000
          Top = 899.528140000000000000
          Width = 102.047310000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo28: TfrxMemoView
          Left = 347.716760000000000000
          Top = 899.528140000000000000
          Width = 241.889920000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#187#1056#1105#1057#8225#1056#1029#1056#176#1057#1039' '#1056#1111#1056#1109#1056#1169#1056#1111#1056#1105#1057#1027#1057#1034)
          ParentFont = False
        end
        object frxDBDataset3FAMILY: TfrxMemoView
          Left = 200.315090000000000000
          Top = 370.393940000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'FAMILY'
          DataSet = frxDBDataset3
          DataSetName = 'frxDBDataset3'
          Memo.UTF8 = (
            '[frxDBDataset3."FAMILY"]')
        end
        object frxDBDataset3OTCHESTVO: TfrxMemoView
          Left = 510.236550000000000000
          Top = 374.173470000000000000
          Width = 238.110390000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'OTCHESTVO'
          DataSet = frxDBDataset3
          DataSetName = 'frxDBDataset3'
          Memo.UTF8 = (
            '[frxDBDataset3."OTCHESTVO"]')
        end
        object frxDBDataset3IMYA: TfrxMemoView
          Left = 359.055350000000000000
          Top = 370.393940000000000000
          Width = 200.315090000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'IMYA'
          DataSet = frxDBDataset3
          DataSetName = 'frxDBDataset3'
          Memo.UTF8 = (
            '[frxDBDataset3."IMYA"]')
        end
      end
      object MasterData1: TfrxMasterData
        Height = 22.677180000000000000
        Top = 1077.166050000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset4
        DataSetName = 'frxDBDataset4'
        RowCount = 0
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 1160.315710000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 642.520100000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
        end
      end
      object Memo5: TfrxMemoView
        Left = 510.236550000000000000
        Top = 11.338590000000000000
        Width = 230.551330000000000000
        Height = 18.897650000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8 = (
          #1056#1032#1056#1029#1056#1105#1057#8222#1056#1105#1057#8224#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#176#1057#1039' '#1057#8222#1056#1109#1057#1026#1056#1112#1056#176' '#1074#8222#8211' '#1056#1118'-1')
        ParentFont = False
      end
    end
  end
  object frxDBDataset3: TfrxDBDataset
    UserName = 'frxDBDataset3'
    CloseDataSource = False
    DataSource = DMod.DS_Vibor
    BCDToCurrency = False
    Left = 112
    Top = 128
  end
  object frxDBDataset4: TfrxDBDataset
    UserName = 'frxDBDataset4'
    CloseDataSource = False
    DataSource = DMod.DS_Dolzhn
    BCDToCurrency = False
    Left = 112
    Top = 168
  end
end
