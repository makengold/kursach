unit UnitSdelkaUpd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls;

type
  TSdelkaUpd = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label51: TLabel;
    Edit3: TEdit;
    RichEdit1: TRichEdit;
    GroupBox18: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label50: TLabel;
    Label52: TLabel;
    Edit4: TEdit;
    Edit25: TEdit;
    Edit26: TEdit;
    Edit27: TEdit;
    Edit1: TEdit;
    Edit2: TEdit;
    GroupBox21: TGroupBox;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label53: TLabel;
    Edit31: TEdit;
    Edit32: TEdit;
    Edit33: TEdit;
    Edit34: TEdit;
    Edit28: TEdit;
    GroupBox20: TGroupBox;
    Label55: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label64: TLabel;
    Edit29: TEdit;
    Edit30: TEdit;
    Edit35: TEdit;
    Edit36: TEdit;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Edit5: TEdit;
    BitBtn19: TBitBtn;
    ComboBox1: TComboBox;
    RichEdit2: TRichEdit;
    GroupBox5: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    BitBtn20: TBitBtn;
    Edit10: TEdit;
    BitBtn22: TBitBtn;
    BitBtn1: TBitBtn;
    GroupBox6: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Edit11: TEdit;
    Edit12: TEdit;
    Edit13: TEdit;
    Edit14: TEdit;
    BitBtn23: TBitBtn;
    Edit15: TEdit;
    GroupBox7: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Edit16: TEdit;
    Edit17: TEdit;
    Edit18: TEdit;
    Edit19: TEdit;
    BitBtn21: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure BitBtn20Click(Sender: TObject);
    procedure BitBtn23Click(Sender: TObject);
    procedure BitBtn21Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn22Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SdelkaUpd: TSdelkaUpd;

implementation

uses UnitNedvizhChng, UnitProdavec, UnitPokup, UnitAgentChng, DM;

{$R *.dfm}

procedure TSdelkaUpd.BitBtn1Click(Sender: TObject);
begin
  Edit1.Clear;
  SdelkaUpd.Close;
end;

procedure TSdelkaUpd.BitBtn19Click(Sender: TObject);
begin
  ChangeNedvizh.BitBtn1.Tag:=1;
  if ComboBox1.Text<>'' then
  ChangeNedvizh.Show
  else
  ShowMessage('�������� ��� ������������!');
  Exit;
end;

procedure TSdelkaUpd.BitBtn20Click(Sender: TObject);
begin
  FormProdavec.BitBtn3.Tag:=1;
  FormProdavec.Show;
end;

procedure TSdelkaUpd.BitBtn23Click(Sender: TObject);
begin
  FormPokup.BitBtn3.Tag:=1;
  FormPokup.Show;
end;

procedure TSdelkaUpd.BitBtn21Click(Sender: TObject);
begin
  ChangeAgent.BitBtn1.Tag:=1;
  ChangeAgent.Show;
end;

procedure TSdelkaUpd.ComboBox1Change(Sender: TObject);
begin
  DMod.Q_NedvizhChange.Close;
  DMod.Q_NedvizhChange.ParamByName('naimenovanie').AsString:=ComboBox1.Text;
  DMod.Q_NedvizhChange.Open;
  RichEdit2.Clear;
  Edit5.Clear;
end;

procedure TSdelkaUpd.FormShow(Sender: TObject);
begin
  RichEdit2.Clear;
  Edit10.Text:=Edit1.Text;
  if Edit2.Text='��������' then
  ComboBox1.ItemIndex:=0;
  if Edit2.Text='�����' then
  ComboBox1.ItemIndex:=1;
  if Edit2.Text='����' then
  ComboBox1.ItemIndex:=2;
  RichEdit2.Lines.Text:=RichEdit1.Text;
  Edit5.Text:=Edit3.Text;
  Edit6.Text:=Edit4.Text;
  Edit7.Text:=Edit25.Text;
  Edit8.Text:=Edit26.Text;
  Edit9.Text:=Edit27.Text;
  Edit11.Text:=Edit31.Text;
  Edit12.Text:=Edit32.Text;
  Edit13.Text:=Edit33.Text;
  Edit14.Text:=Edit34.Text;
  Edit15.Text:=Edit28.Text;
  Edit16.Text:=Edit29.Text;
  Edit17.Text:=Edit30.Text;
  Edit18.Text:=Edit35.Text;
  Edit19.Text:=Edit36.Text;
end;

procedure TSdelkaUpd.BitBtn22Click(Sender: TObject);
begin
  DMod.Sdelka_Upd.ParamByName('ID_PRODAVCA').AsInteger:=DMod.Q_ProdavecID_PRODAVCA.AsInteger;
  DMod.Sdelka_Upd.ParamByName('ID_POKUP').AsInteger:=DMod.Q_PokupID_POKUP.AsInteger;
  DMod.Sdelka_Upd.ParamByName('ID_SOTRUD').AsInteger:=DMod.Q_AgentChangeID_SOTRUD.AsInteger;
  DMod.Sdelka_Upd.ParamByName('ID_NEDVIZH').AsInteger:=DMod.Q_NedvizhChangeID_NEDVIZH.AsInteger;
  DMod.Sdelka_Upd.ParamByName('NOM_DOGV').AsString:=Edit10.Text;
  DMod.Sdelka_Upd.ParamByName('ID_USL').AsInteger:=DMod.Q_SdelkaID_USL.AsInteger;
  DMod.Sdelka_Upd.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Sdelka.Close;
  DMod.Q_Sdelka.Open;
  MessageBox(Self.Handle, PChar('������ ������� ��������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  SdelkaUpd.Close;
end;

end.
