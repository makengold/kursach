object OtdelUpd: TOtdelUpd
  Left = 530
  Top = 206
  BorderStyle = bsDialog
  Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1086#1090#1076#1077#1083
  ClientHeight = 141
  ClientWidth = 360
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 32
    Width = 91
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1086#1090#1076#1077#1083#1072':'
  end
  object Label2: TLabel
    Left = 8
    Top = 72
    Width = 126
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1088#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1103':'
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 144
    Top = 64
    Width = 201
    Height = 21
    KeyField = 'ID_SOTRUD'
    ListField = 'FIO'
    ListSource = DMod.DS_Sotrud
    TabOrder = 0
    OnEnter = DBLookupComboBox1Enter
  end
  object BitBtn1: TBitBtn
    Left = 40
    Top = 103
    Width = 81
    Height = 33
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 232
    Top = 103
    Width = 83
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = BitBtn2Click
  end
  object Edit1: TEdit
    Left = 144
    Top = 24
    Width = 201
    Height = 21
    ReadOnly = True
    TabOrder = 3
  end
end
