object FormRemont: TFormRemont
  Left = 411
  Top = 225
  BorderStyle = bsDialog
  Caption = #1056#1077#1084#1086#1085#1090
  ClientHeight = 245
  ClientWidth = 504
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 328
    Top = 56
    Width = 22
    Height = 13
    Caption = #1042#1080#1076':'
  end
  object Label2: TLabel
    Left = 328
    Top = 96
    Width = 29
    Height = 13
    Caption = #1044#1072#1090#1072':'
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 289
    Height = 209
    DataSource = DMod.DS_Remont
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    OnTitleClick = DBGrid1TitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'NAIMEN'
        Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1088#1077#1084#1086#1085#1090#1072
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA'
        Title.Caption = #1044#1072#1090#1072' '#1088#1077#1084#1086#1085#1090#1072
        Width = 80
        Visible = True
      end>
  end
  object BitBtn2: TBitBtn
    Left = 408
    Top = 176
    Width = 81
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    OnClick = BitBtn2Click
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 216
    Width = 65
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 2
    OnClick = BitBtn1Click
  end
  object BitBtn4: TBitBtn
    Left = 96
    Top = 216
    Width = 65
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 3
    OnClick = BitBtn4Click
  end
  object BitBtn5: TBitBtn
    Left = 312
    Top = 176
    Width = 81
    Height = 33
    Caption = #1043#1086#1090#1086#1074#1086
    TabOrder = 4
    OnClick = BitBtn5Click
  end
  object Edit1: TEdit
    Left = 368
    Top = 48
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 5
  end
  object Edit2: TEdit
    Left = 368
    Top = 88
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 6
  end
  object CheckBox1: TCheckBox
    Left = 464
    Top = 232
    Width = 97
    Height = 17
    Caption = 'Check'
    TabOrder = 7
    Visible = False
  end
end
