object FormSobitie: TFormSobitie
  Left = 209
  Top = 193
  BorderStyle = bsDialog
  Caption = #1057#1086#1073#1099#1090#1080#1103
  ClientHeight = 348
  ClientWidth = 895
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 465
    Height = 305
    DataSource = DMod.DS_Vibor
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    Columns = <
      item
        Expanded = False
        FieldName = 'ID_SOBIT'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'NAIMEN'
        Title.Caption = #1058#1080#1087' '#1089#1086#1073#1099#1090#1080#1103
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TIP_DOCUM'
        Title.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA_SOBIT'
        Title.Caption = #1044#1072#1090#1072' '#1089#1086#1073#1099#1090#1080#1103
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SODERJ_DOCUM'
        Title.Caption = #1057#1086#1076#1077#1088#1078#1072#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OTVETSTV_LICO'
        Title.Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ID_SOTRUD'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ID_DOLZH'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'FAMILY'
        Title.Caption = #1060#1072#1084#1080#1083#1080#1103
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IMYA'
        Title.Caption = #1048#1084#1103
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OTCHESTVO'
        Title.Caption = #1054#1090#1095#1077#1089#1090#1074#1086
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAIMEN1'
        Title.Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
        Visible = True
      end>
  end
  object GroupBox1: TGroupBox
    Left = 480
    Top = 96
    Width = 409
    Height = 249
    Caption = #1057#1086#1073#1099#1090#1080#1077
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 68
      Height = 13
      Caption = #1058#1080#1087' '#1089#1086#1073#1099#1090#1080#1103':'
    end
    object Label2: TLabel
      Left = 192
      Top = 24
      Width = 79
      Height = 13
      Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072':'
    end
    object Label3: TLabel
      Left = 192
      Top = 80
      Width = 110
      Height = 13
      Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072':'
    end
    object Label4: TLabel
      Left = 16
      Top = 88
      Width = 75
      Height = 13
      Caption = #1044#1072#1090#1072' '#1089#1086#1073#1099#1090#1080#1103':'
    end
    object Label5: TLabel
      Left = 16
      Top = 152
      Width = 107
      Height = 13
      Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086':'
    end
    object RichEdit1: TRichEdit
      Left = 192
      Top = 104
      Width = 209
      Height = 97
      ReadOnly = True
      TabOrder = 0
    end
    object BitBtn4: TBitBtn
      Left = 144
      Top = 208
      Width = 97
      Height = 33
      Caption = #1043#1086#1090#1086#1074#1086
      TabOrder = 1
      OnClick = BitBtn4Click
    end
    object Edit1: TEdit
      Left = 16
      Top = 176
      Width = 169
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object Edit2: TEdit
      Left = 16
      Top = 48
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 3
    end
    object Edit3: TEdit
      Left = 192
      Top = 48
      Width = 129
      Height = 21
      ReadOnly = True
      TabOrder = 4
    end
    object Edit4: TEdit
      Left = 16
      Top = 112
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 5
    end
  end
  object BitBtn1: TBitBtn
    Left = 32
    Top = 312
    Width = 81
    Height = 33
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 2
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 136
    Top = 312
    Width = 81
    Height = 33
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    TabOrder = 3
    OnClick = BitBtn2Click
  end
  object BitBtn3: TBitBtn
    Left = 240
    Top = 312
    Width = 81
    Height = 33
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 4
    OnClick = BitBtn3Click
  end
  object GroupBox2: TGroupBox
    Left = 480
    Top = 0
    Width = 409
    Height = 97
    Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
    TabOrder = 5
    object Label6: TLabel
      Left = 8
      Top = 16
      Width = 52
      Height = 13
      Caption = #1060#1072#1084#1080#1083#1080#1103':'
    end
    object Label7: TLabel
      Left = 152
      Top = 16
      Width = 25
      Height = 13
      Caption = #1048#1084#1103':'
    end
    object Label8: TLabel
      Left = 272
      Top = 16
      Width = 50
      Height = 13
      Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
    end
    object Label9: TLabel
      Left = 88
      Top = 67
      Width = 61
      Height = 13
      Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100':'
    end
    object Edit5: TEdit
      Left = 8
      Top = 32
      Width = 137
      Height = 21
      Enabled = False
      TabOrder = 0
    end
    object Edit6: TEdit
      Left = 152
      Top = 32
      Width = 113
      Height = 21
      Enabled = False
      TabOrder = 1
    end
    object Edit7: TEdit
      Left = 272
      Top = 32
      Width = 129
      Height = 21
      Enabled = False
      TabOrder = 2
    end
    object Edit8: TEdit
      Left = 152
      Top = 64
      Width = 137
      Height = 21
      Enabled = False
      TabOrder = 3
    end
  end
  object BitBtn5: TBitBtn
    Left = 344
    Top = 312
    Width = 97
    Height = 33
    Caption = #1057#1086#1079#1076#1072#1090#1100' '#1086#1090#1095#1077#1090
    TabOrder = 6
    OnClick = BitBtn5Click
  end
end
