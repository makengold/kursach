object SdelkaUpd: TSdelkaUpd
  Left = 112
  Top = 98
  ActiveControl = ComboBox1
  BorderStyle = bsDialog
  Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1077
  ClientHeight = 486
  ClientWidth = 1194
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 1
    Width = 593
    Height = 480
    Caption = #1048#1089#1093#1086#1076#1085#1099#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = 11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 50
      Top = 36
      Width = 87
      Height = 13
      Align = alCustom
      Caption = #1053#1086#1084#1077#1088' '#1076#1086#1075#1086#1074#1086#1088#1072':'
    end
    object GroupBox2: TGroupBox
      Left = 16
      Top = 72
      Width = 273
      Height = 177
      Caption = #1053#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1100
      TabOrder = 0
      object Label4: TLabel
        Left = 112
        Top = 48
        Width = 34
        Height = 13
        Caption = #1040#1076#1088#1077#1089':'
      end
      object Label5: TLabel
        Left = 16
        Top = 26
        Width = 22
        Height = 13
        Caption = #1042#1080#1076':'
      end
      object Label51: TLabel
        Left = 16
        Top = 124
        Width = 29
        Height = 13
        Caption = #1062#1077#1085#1072':'
      end
      object Edit3: TEdit
        Left = 56
        Top = 120
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object RichEdit1: TRichEdit
        Left = 24
        Top = 64
        Width = 225
        Height = 41
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
      object Edit2: TEdit
        Left = 48
        Top = 24
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
    end
    object GroupBox18: TGroupBox
      Left = 16
      Top = 272
      Width = 273
      Height = 177
      Caption = #1055#1088#1086#1076#1072#1074#1077#1094
      TabOrder = 1
      object Label2: TLabel
        Left = 24
        Top = 24
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Label3: TLabel
        Left = 24
        Top = 56
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label50: TLabel
        Left = 24
        Top = 88
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label52: TLabel
        Left = 24
        Top = 120
        Width = 48
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085':'
      end
      object Edit4: TEdit
        Left = 88
        Top = 24
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit25: TEdit
        Left = 88
        Top = 56
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit26: TEdit
        Left = 88
        Top = 88
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit27: TEdit
        Left = 88
        Top = 120
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
    end
    object Edit1: TEdit
      Left = 144
      Top = 32
      Width = 121
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 2
    end
    object GroupBox21: TGroupBox
      Left = 304
      Top = 40
      Width = 273
      Height = 209
      Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100':'
      TabOrder = 3
      object Label60: TLabel
        Left = 24
        Top = 24
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Label61: TLabel
        Left = 24
        Top = 56
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label62: TLabel
        Left = 24
        Top = 88
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label63: TLabel
        Left = 24
        Top = 120
        Width = 48
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085':'
      end
      object Label53: TLabel
        Left = 24
        Top = 152
        Width = 45
        Height = 13
        Caption = #8470' '#1089#1095#1077#1090#1072':'
      end
      object Edit31: TEdit
        Left = 88
        Top = 24
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit32: TEdit
        Left = 88
        Top = 56
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit33: TEdit
        Left = 88
        Top = 88
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit34: TEdit
        Left = 88
        Top = 120
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object Edit28: TEdit
        Left = 88
        Top = 152
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 4
      end
    end
    object GroupBox20: TGroupBox
      Left = 304
      Top = 272
      Width = 273
      Height = 177
      Caption = #1040#1075#1077#1085#1090':'
      TabOrder = 4
      object Label55: TLabel
        Left = 24
        Top = 24
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Label58: TLabel
        Left = 24
        Top = 56
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label59: TLabel
        Left = 24
        Top = 88
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label64: TLabel
        Left = 24
        Top = 120
        Width = 48
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085':'
      end
      object Edit29: TEdit
        Left = 88
        Top = 24
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit30: TEdit
        Left = 88
        Top = 56
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit35: TEdit
        Left = 88
        Top = 88
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit36: TEdit
        Left = 88
        Top = 120
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
    end
  end
  object GroupBox3: TGroupBox
    Left = 616
    Top = 0
    Width = 569
    Height = 481
    Caption = #1048#1079#1084#1077#1085#1077#1085#1085#1099#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = 11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label6: TLabel
      Left = 18
      Top = 36
      Width = 87
      Height = 13
      Align = alCustom
      Caption = #1053#1086#1084#1077#1088' '#1076#1086#1075#1086#1074#1086#1088#1072':'
    end
    object GroupBox4: TGroupBox
      Left = 16
      Top = 64
      Width = 273
      Height = 177
      Caption = #1053#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1100
      TabOrder = 0
      object Label7: TLabel
        Left = 112
        Top = 48
        Width = 34
        Height = 13
        Caption = #1040#1076#1088#1077#1089':'
      end
      object Label8: TLabel
        Left = 16
        Top = 26
        Width = 22
        Height = 13
        Caption = #1042#1080#1076':'
      end
      object Label9: TLabel
        Left = 16
        Top = 124
        Width = 29
        Height = 13
        Caption = #1062#1077#1085#1072':'
      end
      object Edit5: TEdit
        Left = 56
        Top = 120
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object BitBtn19: TBitBtn
        Left = 104
        Top = 144
        Width = 75
        Height = 25
        Caption = #1042#1099#1073#1086#1088
        TabOrder = 1
        OnClick = BitBtn19Click
      end
      object ComboBox1: TComboBox
        Left = 56
        Top = 24
        Width = 121
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        OnChange = ComboBox1Change
        Items.Strings = (
          #1050#1074#1072#1088#1090#1080#1088#1072
          #1057#1082#1083#1072#1076
          #1054#1092#1080#1089)
      end
      object RichEdit2: TRichEdit
        Left = 24
        Top = 64
        Width = 225
        Height = 41
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
    end
    object GroupBox5: TGroupBox
      Left = 16
      Top = 248
      Width = 273
      Height = 177
      Caption = #1055#1088#1086#1076#1072#1074#1077#1094
      TabOrder = 1
      object Label10: TLabel
        Left = 24
        Top = 24
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Label11: TLabel
        Left = 24
        Top = 56
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label12: TLabel
        Left = 24
        Top = 88
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label13: TLabel
        Left = 24
        Top = 120
        Width = 48
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085':'
      end
      object Edit6: TEdit
        Left = 88
        Top = 24
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit7: TEdit
        Left = 88
        Top = 56
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit8: TEdit
        Left = 88
        Top = 88
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit9: TEdit
        Left = 88
        Top = 120
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object BitBtn20: TBitBtn
        Left = 104
        Top = 144
        Width = 75
        Height = 25
        Caption = #1042#1099#1073#1086#1088
        TabOrder = 4
        OnClick = BitBtn20Click
      end
    end
    object Edit10: TEdit
      Left = 112
      Top = 32
      Width = 121
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 2
    end
    object BitBtn22: TBitBtn
      Left = 440
      Top = 432
      Width = 115
      Height = 41
      Caption = #1055#1088#1080#1085#1103#1090#1100' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
      TabOrder = 3
      OnClick = BitBtn22Click
    end
    object BitBtn1: TBitBtn
      Left = 288
      Top = 432
      Width = 91
      Height = 41
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 4
      OnClick = BitBtn1Click
    end
    object GroupBox6: TGroupBox
      Left = 312
      Top = 32
      Width = 241
      Height = 209
      Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100':'
      TabOrder = 5
      object Label14: TLabel
        Left = 24
        Top = 24
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Label15: TLabel
        Left = 24
        Top = 56
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label16: TLabel
        Left = 24
        Top = 88
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label17: TLabel
        Left = 24
        Top = 120
        Width = 48
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085':'
      end
      object Label18: TLabel
        Left = 24
        Top = 152
        Width = 45
        Height = 13
        Caption = #8470' '#1089#1095#1077#1090#1072':'
      end
      object Edit11: TEdit
        Left = 88
        Top = 24
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit12: TEdit
        Left = 88
        Top = 56
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit13: TEdit
        Left = 88
        Top = 88
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit14: TEdit
        Left = 88
        Top = 120
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object BitBtn23: TBitBtn
        Left = 104
        Top = 176
        Width = 75
        Height = 25
        Caption = #1042#1099#1073#1086#1088
        TabOrder = 4
        OnClick = BitBtn23Click
      end
      object Edit15: TEdit
        Left = 88
        Top = 152
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 5
      end
    end
    object GroupBox7: TGroupBox
      Left = 312
      Top = 248
      Width = 241
      Height = 177
      Caption = #1040#1075#1077#1085#1090':'
      TabOrder = 6
      object Label19: TLabel
        Left = 24
        Top = 24
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Label20: TLabel
        Left = 24
        Top = 56
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label21: TLabel
        Left = 24
        Top = 88
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label22: TLabel
        Left = 24
        Top = 120
        Width = 48
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085':'
      end
      object Edit16: TEdit
        Left = 88
        Top = 24
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit17: TEdit
        Left = 88
        Top = 56
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit18: TEdit
        Left = 88
        Top = 88
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit19: TEdit
        Left = 88
        Top = 120
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object BitBtn21: TBitBtn
        Left = 104
        Top = 144
        Width = 75
        Height = 25
        Caption = #1042#1099#1073#1086#1088
        TabOrder = 4
        OnClick = BitBtn21Click
      end
    end
  end
end
