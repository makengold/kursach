unit Autoriz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, IBDatabase;

type
  TFormAutoriz = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    BitBtn1: TBitBtn;
    OpenDialog1: TOpenDialog;
    BitBtn2: TBitBtn;
    Edit3: TEdit;
    BitBtn3: TBitBtn;
    OpenDialog2: TOpenDialog;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormAutoriz: TFormAutoriz;

implementation

uses UnitZastavka, DM, Main;

{$R *.dfm}

procedure TFormAutoriz.BitBtn1Click(Sender: TObject);
begin
  if (DMod.Q_Prava.Locate('LOGIN',Edit1.Text,[])) and (DMod.Q_PravaPASSWORD.Value=Edit2.Text) then
  begin
  case DMod.Q_PravaPRAVA.AsInteger of
  0: begin
  MainForm.TabSheet1.TabVisible:=True;
  MainForm.TabSheet2.TabVisible:=True;                         
  MainForm.TabSheet3.TabVisible:=True;
  MainForm.TabSheet4.TabVisible:=True;
  MainForm.BitBtn1.Visible:=False;
  end;
  1: begin
  MainForm.TabSheet1.TabVisible:=True;
  MainForm.TabSheet2.TabVisible:=False;
  MainForm.TabSheet3.TabVisible:=False;
  MainForm.TabSheet4.TabVisible:=False;
  MainForm.BitBtn1.Visible:=True;
  end;
  2: begin
  MainForm.TabSheet1.TabVisible:=False;
  MainForm.TabSheet2.TabVisible:=True;
  MainForm.TabSheet3.TabVisible:=False;
  MainForm.TabSheet4.TabVisible:=False;
  MainForm.BitBtn1.Visible:=True;
  end;
  3: begin
  MainForm.TabSheet1.TabVisible:=False;
  MainForm.TabSheet2.TabVisible:=False;
  MainForm.TabSheet3.TabVisible:=False;
  MainForm.TabSheet4.TabVisible:=True;
  MainForm.BitBtn1.Visible:=True;
  end;
  end;
  FormAutoriz.Hide;
  MainForm.Show;
  end
  else
  begin
  Edit1.Color:=clRed;
  Edit2.Color:=clRed;
  case MessageBox(Handle,'�������� ����� ��� ������.','������!', MB_OK or MB_ICONWARNING) of
  IDOK: begin
  Edit1.Color:=clWindow;
  Edit2.Color:=clWindow;
  end;
  end;
  end;
end;

procedure TFormAutoriz.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=VK_RETURN then
begin
if (DMod.Q_Prava.Locate('LOGIN',Edit1.Text,[])) and (DMod.Q_PravaPASSWORD.Value=Edit2.Text) then
  begin
  case DMod.Q_PravaPRAVA.AsInteger of
  0: begin
  MainForm.TabSheet1.TabVisible:=True;
  MainForm.TabSheet2.TabVisible:=True;
  MainForm.TabSheet4.TabVisible:=True;
  end;
  1: begin
  MainForm.TabSheet1.TabVisible:=True;
  MainForm.TabSheet2.TabVisible:=False;
  MainForm.TabSheet4.TabVisible:=False;
  end;
  2: begin
  MainForm.TabSheet1.TabVisible:=False;
  MainForm.TabSheet2.TabVisible:=True;
  MainForm.TabSheet4.TabVisible:=False;
  end;
  3: begin
  MainForm.TabSheet1.TabVisible:=False;
  MainForm.TabSheet2.TabVisible:=False;
  MainForm.TabSheet4.TabVisible:=True;
  end;
  end;
  Zastavka.Show;
  FormAutoriz.Hide;
  end
  else
  begin
  Edit1.Color:=clRed;
  Edit2.Color:=clRed;
  case MessageBox(Handle,'�������� ����� ��� ������.','������!', MB_OK or MB_ICONWARNING) of
  IDOK: begin
  Edit1.Color:=clWindow;
  Edit2.Color:=clWindow;
  end;
  end;
  end;
  end;
end;

procedure TFormAutoriz.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  ActiveControl:=Edit2;
end;

procedure TFormAutoriz.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  ActiveControl:=BitBtn1;
end;

procedure TFormAutoriz.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TFormAutoriz.BitBtn2Click(Sender: TObject);
begin
  if OpenDialog2.Execute then
  Edit3.Text:=OpenDialog2.FileName;
end;

procedure TFormAutoriz.BitBtn3Click(Sender: TObject);
begin
 DMod.IBDatabase1.Connected:=False;
 DMod.IBDatabase1.DatabaseName:='';
 DMod.IBDatabase1.DatabaseName:='localhost'+':'+Edit3.Text;
 try
  DMod.IBDatabase1.Connected:=True;
  DMod.Q_Vibor.Open;
  DMod.Q_Vibor2.Open;
  DMod.Q_Prava.Open;
  DMod.Q_Sotrud1.Open;
  DMod.Q_NedvizhChange.Open;
  DMod.Q_AgentChange.Open;
  DMod.Q_Nedvizh.Open;
  DMod.Q_Adres.Open;
  DMod.Q_Remont.Open;
  DMod.Q_Vid_Nedvizh.Open;
  DMod.Q_Sotrud.Open;
  DMod.Q_Otdel.Open;
  DMod.Q_Dolzhn.Open;
  DMod.Q_Sobit.Open;
  DMod.Q_Prodavec.Open;
  DMod.Q_Pokup.Open;
  DMod.Q_Sdelka.Open;
  DMod.Q_Viplati.Open;
  DMod.Q_Users.Open;
 except
  MessageBox(Handle,'������ �����������.','������!', MB_OK or MB_ICONWARNING);
  exit;
end;
  MessageBox(Handle,'���� ������� ����������.','', MB_OK or MB_ICONINFORMATION);
end;

end.
