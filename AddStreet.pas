unit AddStreet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, ExtCtrls;

type
  TAdresAdd = class(TForm)
    GroupBox6: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label12: TLabel;
    ComboBox4: TComboBox;
    ComboBox3: TComboBox;
    CheckBox1: TCheckBox;
    MaskEdit1: TMaskEdit;
    BitBtn3: TBitBtn;
    BitBtn1: TBitBtn;
    OpenDialog1: TOpenDialog;
    Edit1: TEdit;
    CheckBox2: TCheckBox;
    Timer1: TTimer;
    procedure ComboBox3DropDown(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AdresAdd: TAdresAdd;

implementation

uses DM;

{$R *.dfm}

procedure TAdresAdd.ComboBox3DropDown(Sender: TObject);
begin
 case ComboBox4.ItemIndex of
 0: begin
 OpenDialog1.FileName:='C:\Users\SkaZka\Desktop\��������\App\���������.txt';
 ComboBox3.Items.LoadFromFile(OpenDialog1.Filename);
 end;
 1: begin
 OpenDialog1.FileName:='C:\Users\SkaZka\Desktop\��������\App\������������.txt';
 ComboBox3.Items.LoadFromFile(OpenDialog1.Filename);
 end;
 2: begin
 OpenDialog1.FileName:='C:\Users\SkaZka\Desktop\��������\App\������������.txt';
 ComboBox3.Items.LoadFromFile(OpenDialog1.Filename);
 end;
 3: begin
 OpenDialog1.FileName:='C:\Users\SkaZka\Desktop\��������\App\����������.txt';
 ComboBox3.Items.LoadFromFile(OpenDialog1.Filename);
 end;
 4: begin
 OpenDialog1.FileName:='C:\Users\SkaZka\Desktop\��������\App\�������������.txt';
 ComboBox3.Items.LoadFromFile(OpenDialog1.Filename);
 end;
 5: begin
 OpenDialog1.FileName:='C:\Users\SkaZka\Desktop\��������\App\������������.txt';
 ComboBox3.Items.LoadFromFile(OpenDialog1.Filename);
 end;
 6: begin
 OpenDialog1.FileName:='C:\Users\SkaZka\Desktop\��������\App\���������.txt';
 ComboBox3.Items.LoadFromFile(OpenDialog1.Filename);
 end;
 end
end;

procedure TAdresAdd.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Checked=True
  then
  MaskEdit1.EditMask:='999�9;1;_'
end;

procedure TAdresAdd.BitBtn3Click(Sender: TObject);
begin
  if (ComboBox4.Text<>'') and (MaskEdit1.Text<>'') and (ComboBox3.Text<>'') and (Edit1.Text<>'')
  then begin
  DMod.Adres_Add.ParamByName('GOROD').AsString:=ComboBox4.Text;
  DMod.Adres_Add.ParamByName('RAYON').AsString:=ComboBox3.Text;
  DMod.Adres_Add.ParamByName('ULITCA').AsString:=Edit1.Text;
  DMod.Adres_Add.ParamByName('DOM').AsString:=MaskEdit1.Text;
  DMod.Adres_Add.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Adres.Close;
  DMod.Q_Adres.Open;
  MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  ComboBox4.Text:='';
  ComboBox3.Text:='';
  Edit1.Text:='';
  MaskEdit1.Text:='';
  AdresAdd.Timer1.Enabled:=False;
  AdresAdd.Close;
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
end;

procedure TAdresAdd.BitBtn1Click(Sender: TObject);
begin
  ComboBox4.Text:='';
  ComboBox3.Text:='';
  Edit1.Text:='';
  MaskEdit1.Text:='';
  AdresAdd.Timer1.Enabled:=False;
  AdresAdd.Close;
end;

procedure TAdresAdd.CheckBox2Click(Sender: TObject);
begin
  if CheckBox2.Checked=True
  then
  MaskEdit1.EditMask:='999���9;1;_'
end;

procedure TAdresAdd.Timer1Timer(Sender: TObject);
begin
  if (CheckBox1.Checked=True) and not(CheckBox2.Checked=True)
  then MaskEdit1.EditMask:='999�9;1;_';
  if not(CheckBox1.Checked=True) and not(CheckBox2.Checked=True)
  then MaskEdit1.EditMask:='999;1;_';
  if not(CheckBox1.Checked=True) and (CheckBox2.Checked=True)
  then MaskEdit1.EditMask:='999���9;1;_';
  if (CheckBox1.Checked=True) and (CheckBox2.Checked=True)
  then MaskEdit1.EditMask:='999�9���9;1;_';
end;

end.
