object AdresUpd: TAdresUpd
  Left = 503
  Top = 177
  BorderStyle = bsDialog
  Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1072#1076#1088#1077#1089
  ClientHeight = 253
  ClientWidth = 337
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox6: TGroupBox
    Left = 4
    Top = -5
    Width = 329
    Height = 265
    Caption = '='
    TabOrder = 0
    object Label13: TLabel
      Left = 48
      Top = 72
      Width = 34
      Height = 13
      Caption = #1056#1072#1081#1086#1085':'
    end
    object Label14: TLabel
      Left = 48
      Top = 112
      Width = 35
      Height = 13
      Caption = #1059#1083#1080#1094#1072':'
    end
    object Label15: TLabel
      Left = 48
      Top = 152
      Width = 26
      Height = 13
      Caption = #1044#1086#1084':'
    end
    object Label12: TLabel
      Left = 48
      Top = 32
      Width = 33
      Height = 13
      Caption = #1043#1086#1088#1086#1076':'
    end
    object ComboBox4: TComboBox
      Left = 96
      Top = 24
      Width = 145
      Height = 21
      ItemHeight = 13
      Sorted = True
      TabOrder = 0
      Items.Strings = (
        #1045#1082#1072#1090#1077#1088#1080#1085#1073#1091#1088#1075
        #1050#1072#1079#1072#1085#1100
        #1052#1086#1089#1082#1074#1072
        #1053#1080#1078#1085#1080#1081' '#1053#1086#1074#1075#1086#1088#1086#1076
        #1053#1086#1074#1086#1089#1080#1073#1080#1088#1089#1082
        #1057#1072#1084#1072#1088#1072
        #1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075)
    end
    object ComboBox3: TComboBox
      Left = 96
      Top = 64
      Width = 145
      Height = 21
      ItemHeight = 13
      Sorted = True
      TabOrder = 1
      OnDropDown = ComboBox3DropDown
    end
    object CheckBox1: TCheckBox
      Left = 72
      Top = 176
      Width = 97
      Height = 17
      Caption = #1050#1086#1088#1087#1091#1089
      TabOrder = 2
    end
    object MaskEdit1: TMaskEdit
      Left = 96
      Top = 144
      Width = 138
      Height = 21
      EditMask = '999;1;_'
      MaxLength = 3
      TabOrder = 3
      Text = '   '
    end
    object BitBtn3: TBitBtn
      Left = 16
      Top = 216
      Width = 97
      Height = 33
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      TabOrder = 4
      OnClick = BitBtn3Click
    end
    object BitBtn1: TBitBtn
      Left = 200
      Top = 216
      Width = 97
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 5
      OnClick = BitBtn1Click
    end
    object Edit1: TEdit
      Left = 96
      Top = 104
      Width = 140
      Height = 21
      TabOrder = 6
    end
    object CheckBox2: TCheckBox
      Left = 168
      Top = 176
      Width = 97
      Height = 17
      Caption = #1057#1090#1088#1086#1077#1085#1080#1077
      TabOrder = 7
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 300
    Top = 3
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 50
    OnTimer = Timer1Timer
    Left = 268
    Top = 3
  end
end
