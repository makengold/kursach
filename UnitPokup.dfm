object FormPokup: TFormPokup
  Left = 191
  Top = 115
  BorderStyle = bsDialog
  Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
  ClientHeight = 530
  ClientWidth = 1056
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 448
    Top = 488
    Width = 82
    Height = 13
    Caption = #1048#1089#1082#1086#1084#1099#1081' '#1090#1077#1082#1089#1090':'
  end
  object Label11: TLabel
    Left = 352
    Top = 488
    Width = 29
    Height = 13
    Caption = #1055#1086#1083#1077':'
  end
  object Label12: TLabel
    Left = 352
    Top = 464
    Width = 67
    Height = 13
    Caption = #1060#1080#1083#1100#1090#1088#1072#1094#1080#1103':'
  end
  object Label13: TLabel
    Left = 96
    Top = 507
    Width = 35
    Height = 13
    Caption = #1055#1086#1080#1089#1082':'
  end
  object GroupBox10: TGroupBox
    Left = 536
    Top = -7
    Width = 521
    Height = 496
    TabOrder = 0
    object GroupBox5: TGroupBox
      Left = 256
      Top = 16
      Width = 257
      Height = 281
      Caption = #1055#1072#1089#1087#1086#1088#1090#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      TabOrder = 0
      object Label16: TLabel
        Left = 88
        Top = 108
        Width = 75
        Height = 13
        Caption = #1052#1077#1089#1090#1086' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label17: TLabel
        Left = 24
        Top = 84
        Width = 69
        Height = 13
        Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label18: TLabel
        Left = 13
        Top = 20
        Width = 84
        Height = 13
        Caption = #1057#1077#1088#1080#1103' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object Label19: TLabel
        Left = 8
        Top = 52
        Width = 87
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object GroupBox7: TGroupBox
        Left = 16
        Top = 152
        Width = 225
        Height = 121
        Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
        TabOrder = 0
        object RichEdit2: TRichEdit
          Left = 16
          Top = 24
          Width = 193
          Height = 81
          ReadOnly = True
          TabOrder = 0
        end
      end
      object Edit30: TEdit
        Left = 16
        Top = 128
        Width = 225
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit31: TEdit
        Left = 104
        Top = 80
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit10: TEdit
        Left = 104
        Top = 16
        Width = 120
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object Edit11: TEdit
        Left = 104
        Top = 48
        Width = 120
        Height = 21
        ReadOnly = True
        TabOrder = 4
      end
    end
    object BitBtn3: TBitBtn
      Left = 432
      Top = 456
      Width = 83
      Height = 33
      Caption = #1043#1086#1090#1086#1074#1086
      TabOrder = 1
      OnClick = BitBtn3Click
    end
    object BitBtn5: TBitBtn
      Left = 24
      Top = 456
      Width = 83
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 2
      OnClick = BitBtn5Click
    end
    object GroupBox1: TGroupBox
      Left = 16
      Top = 168
      Width = 225
      Height = 129
      Caption = #1056#1072#1073#1086#1090#1072
      TabOrder = 3
      object Label5: TLabel
        Left = 72
        Top = 24
        Width = 75
        Height = 13
        Caption = #1052#1077#1089#1090#1086' '#1088#1072#1073#1086#1090#1099':'
      end
      object Label6: TLabel
        Left = 56
        Top = 72
        Width = 107
        Height = 13
        Caption = #1045#1078#1077#1084#1077#1089#1103#1095#1085#1099#1081' '#1076#1086#1093#1086#1076':'
      end
      object Edit5: TEdit
        Left = 8
        Top = 40
        Width = 209
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit6: TEdit
        Left = 40
        Top = 88
        Width = 145
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
    end
    object GroupBox2: TGroupBox
      Left = 112
      Top = 312
      Width = 337
      Height = 121
      Caption = #1041#1072#1085#1082#1086#1074#1089#1082#1080#1081' '#1089#1095#1077#1090
      TabOrder = 4
      object Label7: TLabel
        Left = 16
        Top = 27
        Width = 68
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1089#1095#1077#1090#1072':'
      end
      object Label8: TLabel
        Left = 16
        Top = 59
        Width = 118
        Height = 13
        Caption = #1044#1072#1090#1072' '#1080#1089#1090#1077#1095#1077#1085#1080#1103' '#1082#1072#1088#1090#1099':'
      end
      object Label9: TLabel
        Left = 16
        Top = 90
        Width = 132
        Height = 13
        Caption = #1055#1077#1088#1089#1086#1085#1072#1083#1100#1085#1099#1077' '#1080#1085#1080#1094#1080#1072#1083#1099':'
      end
      object Edit7: TEdit
        Left = 152
        Top = 88
        Width = 161
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit8: TEdit
        Left = 144
        Top = 56
        Width = 89
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit9: TEdit
        Left = 96
        Top = 24
        Width = 217
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
    end
    object GroupBox3: TGroupBox
      Left = 16
      Top = 16
      Width = 225
      Height = 145
      Caption = #1055#1077#1088#1089#1086#1085#1072#1083#1100#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      TabOrder = 5
      object Label4: TLabel
        Left = 16
        Top = 116
        Width = 48
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085':'
      end
      object Label3: TLabel
        Left = 16
        Top = 84
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label2: TLabel
        Left = 16
        Top = 56
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label1: TLabel
        Left = 16
        Top = 24
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Edit3: TEdit
        Left = 72
        Top = 84
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit2: TEdit
        Left = 72
        Top = 56
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit1: TEdit
        Left = 72
        Top = 24
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit4: TEdit
        Left = 72
        Top = 116
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 529
    Height = 449
    DataSource = DMod.DS_Pokup
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    OnTitleClick = DBGrid1TitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'ID_POKUP'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'FAMILY'
        Title.Caption = #1060#1072#1084#1080#1083#1080#1103
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IMYA'
        Title.Caption = #1048#1084#1103
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OTCHESTVO'
        Title.Caption = #1054#1090#1095#1077#1089#1090#1074#1086
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TEL'
        Title.Caption = #1058#1077#1083#1077#1092#1086#1085
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SER_PASPORT'
        Title.Caption = #1057#1077#1088#1080#1103' '#1087#1072#1089#1087#1086#1088#1090#1072
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOM_PASPORT'
        Title.Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KEM_VIDAN_PASPORT'
        Title.Caption = #1052#1077#1089#1090#1086' '#1074#1099#1076#1072#1095#1080
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA_VIDACHI_PASPORT'
        Title.Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ADRES_REG'
        Title.Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MEST_RABOTI'
        Title.Caption = #1052#1077#1089#1090#1086' '#1088#1072#1073#1086#1090#1099
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MES_DOHOD'
        Title.Caption = #1052#1077#1089#1103#1095#1085#1099#1081' '#1076#1086#1093#1086#1076
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOM_BANK_KARTI'
        Title.Caption = #1053#1086#1084#1077#1088' '#1073#1072#1085#1082#1086#1074#1089#1082#1086#1081' '#1082#1072#1088#1090#1099
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA_ISTECH_KARTI'
        Title.Caption = #1044#1072#1090#1072' '#1080#1089#1090#1077#1095#1077#1085#1080#1103
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAIMEN_VLADELCA'
        Title.Caption = #1048#1085#1076#1080#1092#1080#1082#1072#1090#1086#1088' '#1074#1083#1072#1076#1077#1083#1100#1094#1072
        Width = 135
        Visible = True
      end>
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 456
    Width = 83
    Height = 33
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 2
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 120
    Top = 456
    Width = 83
    Height = 33
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    TabOrder = 3
    OnClick = BitBtn2Click
  end
  object BitBtn4: TBitBtn
    Left = 232
    Top = 456
    Width = 83
    Height = 33
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 4
    OnClick = BitBtn4Click
  end
  object Edit12: TEdit
    Left = 448
    Top = 504
    Width = 121
    Height = 21
    TabOrder = 5
    OnChange = Edit12Change
  end
  object ComboBox1: TComboBox
    Left = 352
    Top = 504
    Width = 89
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 6
    Text = #1060#1072#1084#1080#1083#1080#1103
    Items.Strings = (
      #1060#1072#1084#1080#1083#1080#1103
      #1048#1084#1103
      #1054#1090#1095#1077#1089#1090#1074#1086
      #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072)
  end
  object Edit13: TEdit
    Left = 136
    Top = 504
    Width = 177
    Height = 21
    TabOrder = 7
    OnChange = Edit13Change
    OnClick = Edit13Click
    OnExit = Edit13Exit
  end
end
