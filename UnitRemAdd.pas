unit UnitRemAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls;

type
  TRemontAdd = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    ComboBox1: TComboBox;
    DateTimePicker1: TDateTimePicker;
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RemontAdd: TRemontAdd;

implementation

uses Remont, DM;

{$R *.dfm}

procedure TRemontAdd.BitBtn1Click(Sender: TObject);
begin
  if (ComboBox1.Text<>'')
  then begin
  DMod.Remont_Add.ParamByName('NAIMEN').AsString:=ComboBox1.Text;
  DMod.Remont_Add.ParamByName('DATA').AsString:=DateToStr(DateTimePicker1.Date);
  DMod.Remont_Add.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Remont.Close;
  DMod.Q_Remont.Open;
  MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
  ComboBox1.ItemIndex:=-1;
  DateTimePicker1.Date:=StrToDate('01.01.01');
  RemontAdd.Close;
end;

procedure TRemontAdd.BitBtn2Click(Sender: TObject);
begin
  ComboBox1.ItemIndex:=-1;
  DateTimePicker1.Date:=StrToDate('01.01.01');
  RemontAdd.Close;
end;

end.
