unit UnitProdavecEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, Mask;

type
  TProdavecUpd = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    GroupBox5: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    GroupBox7: TGroupBox;
    RichEdit2: TRichEdit;
    Edit30: TEdit;
    Edit31: TEdit;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    MaskEdit7: TMaskEdit;
    GroupBox3: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    MaskEdit3: TMaskEdit;
    MaskEdit4: TMaskEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox4: TGroupBox;
    RichEdit1: TRichEdit;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProdavecUpd: TProdavecUpd;

implementation

uses DM;

{$R *.dfm}

procedure TProdavecUpd.BitBtn2Click(Sender: TObject);
begin
  ProdavecUpd.Close;
end;

procedure TProdavecUpd.FormShow(Sender: TObject);
begin
  RichEdit1.Clear;
  Edit5.Text:=Edit1.Text;
  Edit6.Text:=Edit2.Text;
  Edit7.Text:=Edit3.Text;
  MaskEdit7.Text:=Edit4.Text;
  MaskEdit3.Text:=MaskEdit1.Text;
  MaskEdit4.Text:=MaskEdit2.Text;
  Edit9.Text:=Edit31.Text;
  Edit8.Text:=Edit30.Text;
  RichEdit1.Lines.Text:=RichEdit2.Lines.Text;
end;

procedure TProdavecUpd.BitBtn1Click(Sender: TObject);
begin
     begin
     if (Edit5.Text<>'') and (Edit6.Text<>'') and (Edit7.Text<>'') and (MaskEdit7.Text<>'') and (MaskEdit3.Text<>'') and (MaskEdit4.Text<>'') and (Edit9.Text<>'') and (Edit8.Text<>'') and (RichEdit1.Lines.Text<>'') then
      begin
      DMod.Prodavec_Upd.ParamByName('FAMILY').AsString:=Edit5.Text;
      DMod.Prodavec_Upd.ParamByName('IMYA').AsString:=Edit6.Text;
      DMod.Prodavec_Upd.ParamByName('OTCHESTVO').AsString:=Edit7.Text;
      DMod.Prodavec_Upd.ParamByName('TEL').AsString:=MaskEdit7.Text;
      DMod.Prodavec_Upd.ParamByName('SER_PASPORT').AsString:=MaskEdit3.Text;
      DMod.Prodavec_Upd.ParamByName('NOM_PASPORT').AsString:=MaskEdit4.Text;;
      DMod.Prodavec_Upd.ParamByName('DATA_VIDACH_PASPORT').AsString:=Edit9.Text;
      DMod.Prodavec_Upd.ParamByName('KEM_VIDAN_PASPORT').AsString:=Edit8.Text;
      DMod.Prodavec_Upd.ParamByName('ADRES_REG').AsString:=RichEdit1.Lines.Text;
      DMod.Prodavec_Upd.ParamByName('ID_PRODAVCA').AsInteger:=DMod.Q_ProdavecID_PRODAVCA.AsInteger;
      DMod.Prodavec_Upd.ExecProc;
      DMod.IBTransaction1.CommitRetaining;
      DMod.Q_Prodavec.Close;
      DMod.Q_Prodavec.Open;
      MessageBox(Self.Handle, PChar('������ ������� ��������'), PChar(''), MB_OK+MB_ICONINFORMATION);
      ProdavecUpd.Close;
      end
    else
    MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
    Exit;
    end;
end;

end.
