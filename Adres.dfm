object FormAdres: TFormAdres
  Left = 338
  Top = 137
  BorderStyle = bsDialog
  Caption = #1040#1076#1088#1077#1089
  ClientHeight = 402
  ClientWidth = 719
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 432
    Top = 328
    Width = 67
    Height = 13
    Caption = #1060#1080#1083#1100#1090#1088#1072#1094#1080#1103':'
  end
  object Label6: TLabel
    Left = 432
    Top = 352
    Width = 29
    Height = 13
    Caption = #1055#1086#1083#1077':'
  end
  object Label7: TLabel
    Left = 528
    Top = 352
    Width = 82
    Height = 13
    Caption = #1048#1089#1082#1086#1084#1099#1081' '#1090#1077#1082#1089#1090':'
  end
  object Label8: TLabel
    Left = 16
    Top = 331
    Width = 35
    Height = 13
    Caption = #1055#1086#1080#1089#1082':'
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 425
    Height = 313
    DataSource = DMod.DS_Adres
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    OnTitleClick = DBGrid1TitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'ID_ADRESA'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'GOROD'
        Title.Caption = #1043#1086#1088#1086#1076
        Width = 135
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RAYON'
        Title.Caption = #1056#1072#1081#1086#1085
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ULITCA'
        Title.Caption = #1059#1083#1080#1094#1072
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DOM'
        Title.Caption = #1044#1086#1084
        Width = 75
        Visible = True
      end>
  end
  object BitBtn4: TBitBtn
    Left = 136
    Top = 360
    Width = 97
    Height = 33
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    TabOrder = 1
    OnClick = BitBtn4Click
  end
  object BitBtn5: TBitBtn
    Left = 264
    Top = 360
    Width = 97
    Height = 33
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 2
    OnClick = BitBtn5Click
  end
  object BitBtn6: TBitBtn
    Left = 8
    Top = 360
    Width = 97
    Height = 33
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 3
    OnClick = BitBtn6Click
  end
  object GroupBox1: TGroupBox
    Left = 448
    Top = 24
    Width = 249
    Height = 281
    TabOrder = 4
    object Label1: TLabel
      Left = 32
      Top = 32
      Width = 33
      Height = 13
      Caption = #1043#1086#1088#1086#1076':'
    end
    object Label2: TLabel
      Left = 32
      Top = 64
      Width = 34
      Height = 13
      Caption = #1056#1072#1081#1086#1085':'
    end
    object Label3: TLabel
      Left = 32
      Top = 96
      Width = 35
      Height = 13
      Caption = #1059#1083#1080#1094#1072':'
    end
    object Label4: TLabel
      Left = 32
      Top = 128
      Width = 26
      Height = 13
      Caption = #1044#1086#1084':'
    end
    object BitBtn1: TBitBtn
      Left = 144
      Top = 216
      Width = 97
      Height = 41
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 8
      Top = 216
      Width = 97
      Height = 41
      Caption = #1043#1086#1090#1086#1074#1086
      TabOrder = 1
      OnClick = BitBtn2Click
    end
    object Edit1: TEdit
      Left = 80
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object Edit2: TEdit
      Left = 80
      Top = 64
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object Edit3: TEdit
      Left = 80
      Top = 96
      Width = 121
      Height = 21
      TabOrder = 4
    end
    object MaskEdit1: TMaskEdit
      Left = 80
      Top = 128
      Width = 121
      Height = 21
      ReadOnly = True
      TabOrder = 5
    end
  end
  object CheckBox1: TCheckBox
    Left = 704
    Top = 392
    Width = 97
    Height = 17
    Caption = 'Check'
    TabOrder = 5
    Visible = False
  end
  object Edit4: TEdit
    Left = 528
    Top = 368
    Width = 121
    Height = 21
    TabOrder = 6
    OnChange = Edit4Change
  end
  object ComboBox1: TComboBox
    Left = 432
    Top = 368
    Width = 89
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 7
    Text = #1043#1086#1088#1086#1076
    Items.Strings = (
      #1043#1086#1088#1086#1076
      #1056#1072#1081#1086#1085
      #1059#1083#1080#1094#1072
      #1044#1086#1084)
  end
  object Edit5: TEdit
    Left = 56
    Top = 328
    Width = 177
    Height = 21
    TabOrder = 8
    OnChange = Edit5Change
    OnClick = Edit5Click
    OnExit = Edit5Exit
  end
end
