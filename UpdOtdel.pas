unit UpdOtdel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DBCtrls;

type
  TOtdelUpd = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit1: TEdit;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBLookupComboBox1Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OtdelUpd: TOtdelUpd;

implementation

uses DM;

{$R *.dfm}

procedure TOtdelUpd.BitBtn2Click(Sender: TObject);
begin
  OtdelUpd.Close
end;

procedure TOtdelUpd.BitBtn1Click(Sender: TObject);
begin
  if (Edit1.Text<>'') and (DBLookupComboBox1.KeyValue<>null)
  then begin
  DMod.Otdel_Upd.ParamByName('NAIMEN').AsString:=Edit1.Text;
  DMod.Otdel_Upd.ParamByName('RUKOVOD').AsString:=DBLookupComboBox1.Text;
  DMod.Otdel_Upd.ParamByName('ID_OTDELA').AsInteger:=DMod.Q_OtdelID_OTDELA.AsInteger;
  DMod.Otdel_Upd.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Otdel.Close;
  DMod.Q_Otdel.Open;
  MessageBox(Self.Handle, PChar('������ ������� ��������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  OtdelUpd.Close;
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
end;

procedure TOtdelUpd.DBLookupComboBox1Enter(Sender: TObject);
begin
OtdelUpd.DBLookupComboBox1.ListSource.DataSet.Last;
end;

end.
