unit UnitViplata;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, Buttons;

type
  TFormViplata = class(TForm)
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Label5: TLabel;
    Edit5: TEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BitBtn4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormViplata: TFormViplata;

implementation

uses DM, UnitViplataAdd;

{$R *.dfm}

procedure TFormViplata.BitBtn1Click(Sender: TObject);
begin
  ViplataAdd.ShowModal;
end;

procedure TFormViplata.BitBtn3Click(Sender: TObject);
begin
    case MessageBox(Handle,'�� �������, ��� ������ ������� ����?','��������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    DMod.Viplata_Del.ParamByName('ID_VIPLATI').AsInteger:=DMod.Q_Vibor2ID_VIPLATI.AsInteger;
    DMod.Viplata_Del.ExecProc;
    end;
    IDNO: Exit;
    end;
    DMod.Q_Vibor2.Close;
    DMod.Q_Vibor2.Open;
    MessageBox(Self.Handle, PChar('������ �������'), PChar(''), MB_OK+MB_ICONINFORMATION);
end;

procedure TFormViplata.DBGrid1CellClick(Column: TColumn);
begin
 Edit2.Text:=DMod.Q_Vibor2DATA_VIPL.AsString;
 Edit3.Text:=DMod.Q_Vibor2SUM_VIPL.AsString;
 Edit4.Text:=DMod.Q_Vibor2OSTAT_VIPL.AsString;
end;

procedure TFormViplata.BitBtn4Click(Sender: TObject);
begin
  Edit2.Clear;
  Edit3.Clear;
  Edit4.Clear;
  FormViplata.Close;
end;

end.
