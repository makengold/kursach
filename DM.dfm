object DMod: TDMod
  OldCreateOrder = False
  Left = 147
  Top = 165
  Height = 443
  Width = 1087
  object IBDatabase1: TIBDatabase
    Connected = True
    DatabaseName = '127.0.0.1:C:\MYBD.FDB'
    Params.Strings = (
      'user_name=SYSDBA'
      'password=masterkey'
      'lc_ctype=WIN1251')
    LoginPrompt = False
    IdleTimer = 0
    SQLDialect = 3
    TraceFlags = []
    Left = 24
    Top = 16
  end
  object IBTransaction1: TIBTransaction
    Active = True
    DefaultDatabase = IBDatabase1
    AutoStopAction = saNone
    Left = 24
    Top = 80
  end
  object DS_Nedvizh: TDataSource
    DataSet = Q_Nedvizh
    Left = 200
    Top = 120
  end
  object DS_Remont: TDataSource
    DataSet = Q_Remont
    Left = 328
    Top = 120
  end
  object DS_Vid_Nedvizh: TDataSource
    DataSet = Q_Vid_Nedvizh
    Left = 408
    Top = 120
  end
  object DS_Sotrud: TDataSource
    DataSet = Q_Sotrud
    Left = 480
    Top = 120
  end
  object DS_Otdel: TDataSource
    DataSet = Q_Otdel
    Left = 544
    Top = 120
  end
  object DS_Dolzhn: TDataSource
    DataSet = Q_Dolzhn
    Left = 608
    Top = 120
  end
  object DS_Adres: TDataSource
    DataSet = Q_Adres
    Left = 264
    Top = 120
  end
  object DS_Sobit: TDataSource
    DataSet = Q_Sobit
    Left = 672
    Top = 120
  end
  object Q_Nedvizh: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    vid_nedvizhimosti.naimen,'
      '    vid_remonta.naimen,'
      '    vid_remonta.data,'
      '    adres.gorod,'
      '    adres.rayon,'
      '    adres.ulitca,'
      '    adres.dom,'
      '    nedvizhimost.id_nedvizh,'
      '    nedvizhimost.id_vid_nedvizh,'
      '    nedvizhimost.obsh_ploshad,'
      '    nedvizhimost.id_adresa,'
      '    nedvizhimost.cena,'
      '    nedvizhimost.ploshad_adm_pomeshen,'
      '    nedvizhimost.etazh,'
      '    nedvizhimost.etazhnost,'
      '    nedvizhimost.parking,'
      '    nedvizhimost.srok_expluatac,'
      '    nedvizhimost.id_remont,'
      '    nedvizhimost.nom_kvartiri,'
      '    nedvizhimost.pocht_index'
      'from nedvizhimost'
      
        '   inner join adres on (nedvizhimost.id_adresa = adres.id_adresa' +
        ')'
      
        '   inner join vid_remonta on (nedvizhimost.id_remont = vid_remon' +
        'ta.id_remont)'
      
        '   inner join vid_nedvizhimosti on (nedvizhimost.id_vid_nedvizh ' +
        '= vid_nedvizhimosti.id_vid_nedvizh)')
    Left = 200
    Top = 184
    object Q_NedvizhNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'VID_NEDVIZHIMOSTI.NAIMEN'
      Size = 15
    end
    object Q_NedvizhNAIMEN1: TIBStringField
      FieldName = 'NAIMEN1'
      Origin = 'VID_REMONTA.NAIMEN'
      Size = 40
    end
    object Q_NedvizhDATA: TIBStringField
      FieldName = 'DATA'
      Origin = 'VID_REMONTA.DATA'
      Size = 15
    end
    object Q_NedvizhGOROD: TIBStringField
      FieldName = 'GOROD'
      Origin = 'ADRES.GOROD'
      Size = 40
    end
    object Q_NedvizhRAYON: TIBStringField
      FieldName = 'RAYON'
      Origin = 'ADRES.RAYON'
      Size = 35
    end
    object Q_NedvizhULITCA: TIBStringField
      FieldName = 'ULITCA'
      Origin = 'ADRES.ULITCA'
      Size = 50
    end
    object Q_NedvizhDOM: TIBStringField
      FieldName = 'DOM'
      Origin = 'ADRES.DOM'
      Size = 10
    end
    object Q_NedvizhID_NEDVIZH: TIntegerField
      FieldName = 'ID_NEDVIZH'
      Origin = 'NEDVIZHIMOST.ID_NEDVIZH'
      Required = True
    end
    object Q_NedvizhID_VID_NEDVIZH: TIntegerField
      FieldName = 'ID_VID_NEDVIZH'
      Origin = 'NEDVIZHIMOST.ID_VID_NEDVIZH'
    end
    object Q_NedvizhOBSH_PLOSHAD: TIBStringField
      FieldName = 'OBSH_PLOSHAD'
      Origin = 'NEDVIZHIMOST.OBSH_PLOSHAD'
      Size = 10
    end
    object Q_NedvizhID_ADRESA: TIntegerField
      FieldName = 'ID_ADRESA'
      Origin = 'NEDVIZHIMOST.ID_ADRESA'
    end
    object Q_NedvizhCENA: TIntegerField
      FieldName = 'CENA'
      Origin = 'NEDVIZHIMOST.CENA'
    end
    object Q_NedvizhPLOSHAD_ADM_POMESHEN: TIBStringField
      FieldName = 'PLOSHAD_ADM_POMESHEN'
      Origin = 'NEDVIZHIMOST.PLOSHAD_ADM_POMESHEN'
      Size = 10
    end
    object Q_NedvizhETAZH: TIntegerField
      FieldName = 'ETAZH'
      Origin = 'NEDVIZHIMOST.ETAZH'
    end
    object Q_NedvizhETAZHNOST: TIntegerField
      FieldName = 'ETAZHNOST'
      Origin = 'NEDVIZHIMOST.ETAZHNOST'
    end
    object Q_NedvizhPARKING: TIBStringField
      FieldName = 'PARKING'
      Origin = 'NEDVIZHIMOST.PARKING'
      FixedChar = True
      Size = 5
    end
    object Q_NedvizhSROK_EXPLUATAC: TIntegerField
      FieldName = 'SROK_EXPLUATAC'
      Origin = 'NEDVIZHIMOST.SROK_EXPLUATAC'
    end
    object Q_NedvizhID_REMONT: TIntegerField
      FieldName = 'ID_REMONT'
      Origin = 'NEDVIZHIMOST.ID_REMONT'
    end
    object Q_NedvizhNOM_KVARTIRI: TIntegerField
      FieldName = 'NOM_KVARTIRI'
      Origin = 'NEDVIZHIMOST.NOM_KVARTIRI'
    end
    object Q_NedvizhPOCHT_INDEX: TIBStringField
      FieldName = 'POCHT_INDEX'
      Origin = 'NEDVIZHIMOST.POCHT_INDEX'
      Size = 6
    end
  end
  object Q_Adres: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from ADRES')
    Left = 264
    Top = 184
    object Q_AdresID_ADRESA: TIntegerField
      FieldName = 'ID_ADRESA'
      Origin = 'ADRES.ID_ADRESA'
      Required = True
    end
    object Q_AdresGOROD: TIBStringField
      FieldName = 'GOROD'
      Origin = 'ADRES.GOROD'
      Size = 40
    end
    object Q_AdresRAYON: TIBStringField
      FieldName = 'RAYON'
      Origin = 'ADRES.RAYON'
      Size = 35
    end
    object Q_AdresULITCA: TIBStringField
      FieldName = 'ULITCA'
      Origin = 'ADRES.ULITCA'
      Size = 50
    end
    object Q_AdresDOM: TIBStringField
      FieldName = 'DOM'
      Origin = 'ADRES.DOM'
      Size = 10
    end
  end
  object Q_Remont: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from VID_REMONTA'
      'where ID_REMONT<>1')
    Left = 328
    Top = 184
    object Q_RemontID_REMONT: TIntegerField
      FieldName = 'ID_REMONT'
      Origin = 'VID_REMONTA.ID_REMONT'
      Required = True
    end
    object Q_RemontNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'VID_REMONTA.NAIMEN'
      Size = 40
    end
    object Q_RemontDATA: TIBStringField
      FieldName = 'DATA'
      Origin = 'VID_REMONTA.DATA'
      Size = 15
    end
  end
  object Q_Vid_Nedvizh: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from VID_NEDVIZHIMOSTI')
    Left = 408
    Top = 184
    object Q_Vid_NedvizhID_VID_NEDVIZH: TIntegerField
      FieldName = 'ID_VID_NEDVIZH'
      Origin = 'VID_NEDVIZHIMOSTI.ID_VID_NEDVIZH'
      Required = True
    end
    object Q_Vid_NedvizhNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'VID_NEDVIZHIMOSTI.NAIMEN'
      Size = 15
    end
  end
  object Q_Sotrud: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    OnCalcFields = Q_SotrudCalcFields
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    dolzhnost.naimen,'
      '    otdel.naimen,'
      '    otdel.rukovod,'
      '    sotrudnik.id_sotrud,'
      '    sotrudnik.family,'
      '    sotrudnik.imya,'
      '    sotrudnik.otchestvo,'
      '    sotrudnik.pol,'
      '    sotrudnik.data_rozhden,'
      '    sotrudnik.mesto_zhit,'
      '    sotrudnik.id_dolzh,'
      '    sotrudnik.ser_pasport,'
      '    sotrudnik.nom_pasport,'
      '    sotrudnik.data_vidachi_pasport,'
      '    sotrudnik.kem_vidan_pasport,'
      '    sotrudnik.agres_reg,'
      '    sotrudnik.id_otdela,'
      '    sotrudnik.dom_telefon,'
      '    sotrudnik.rab_telefon'
      'from sotrudnik'
      '   inner join otdel on (sotrudnik.id_otdela = otdel.id_otdela)'
      
        '   inner join dolzhnost on (sotrudnik.id_dolzh = dolzhnost.id_do' +
        'lzh)')
    Left = 480
    Top = 184
    object Q_SotrudNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'DOLZHNOST.NAIMEN'
      Size = 50
    end
    object Q_SotrudNAIMEN1: TIBStringField
      FieldName = 'NAIMEN1'
      Origin = 'OTDEL.NAIMEN'
      Size = 35
    end
    object Q_SotrudRUKOVOD: TIBStringField
      FieldName = 'RUKOVOD'
      Origin = 'OTDEL.RUKOVOD'
      Size = 90
    end
    object Q_SotrudID_SOTRUD: TIntegerField
      FieldName = 'ID_SOTRUD'
      Origin = 'SOTRUDNIK.ID_SOTRUD'
      Required = True
    end
    object Q_SotrudFAMILY: TIBStringField
      FieldName = 'FAMILY'
      Origin = 'SOTRUDNIK.FAMILY'
      Size = 35
    end
    object Q_SotrudIMYA: TIBStringField
      FieldName = 'IMYA'
      Origin = 'SOTRUDNIK.IMYA'
      Size = 25
    end
    object Q_SotrudOTCHESTVO: TIBStringField
      FieldName = 'OTCHESTVO'
      Origin = 'SOTRUDNIK.OTCHESTVO'
      Size = 30
    end
    object Q_SotrudPOL: TIBStringField
      FieldName = 'POL'
      Origin = 'SOTRUDNIK.POL'
      Size = 5
    end
    object Q_SotrudDATA_ROZHDEN: TIBStringField
      FieldName = 'DATA_ROZHDEN'
      Origin = 'SOTRUDNIK.DATA_ROZHDEN'
      Size = 15
    end
    object Q_SotrudMESTO_ZHIT: TIBStringField
      FieldName = 'MESTO_ZHIT'
      Origin = 'SOTRUDNIK.MESTO_ZHIT'
      Size = 120
    end
    object Q_SotrudID_DOLZH: TIntegerField
      FieldName = 'ID_DOLZH'
      Origin = 'SOTRUDNIK.ID_DOLZH'
    end
    object Q_SotrudSER_PASPORT: TIBStringField
      FieldName = 'SER_PASPORT'
      Origin = 'SOTRUDNIK.SER_PASPORT'
      Size = 5
    end
    object Q_SotrudNOM_PASPORT: TIBStringField
      FieldName = 'NOM_PASPORT'
      Origin = 'SOTRUDNIK.NOM_PASPORT'
      Size = 7
    end
    object Q_SotrudDATA_VIDACHI_PASPORT: TIBStringField
      FieldName = 'DATA_VIDACHI_PASPORT'
      Origin = 'SOTRUDNIK.DATA_VIDACHI_PASPORT'
      Size = 15
    end
    object Q_SotrudKEM_VIDAN_PASPORT: TIBStringField
      FieldName = 'KEM_VIDAN_PASPORT'
      Origin = 'SOTRUDNIK.KEM_VIDAN_PASPORT'
      Size = 80
    end
    object Q_SotrudAGRES_REG: TIBStringField
      FieldName = 'AGRES_REG'
      Origin = 'SOTRUDNIK.AGRES_REG'
      Size = 120
    end
    object Q_SotrudID_OTDELA: TIntegerField
      FieldName = 'ID_OTDELA'
      Origin = 'SOTRUDNIK.ID_OTDELA'
    end
    object Q_SotrudDOM_TELEFON: TIBStringField
      FieldName = 'DOM_TELEFON'
      Origin = 'SOTRUDNIK.DOM_TELEFON'
    end
    object Q_SotrudRAB_TELEFON: TIBStringField
      FieldName = 'RAB_TELEFON'
      Origin = 'SOTRUDNIK.RAB_TELEFON'
    end
    object Q_SotrudFIO: TStringField
      FieldKind = fkCalculated
      FieldName = 'FIO'
      Size = 90
      Calculated = True
    end
  end
  object Q_Otdel: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from OTDEL')
    Left = 544
    Top = 184
    object Q_OtdelID_OTDELA: TIntegerField
      FieldName = 'ID_OTDELA'
      Origin = 'OTDEL.ID_OTDELA'
      Required = True
    end
    object Q_OtdelNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'OTDEL.NAIMEN'
      Size = 35
    end
    object Q_OtdelRUKOVOD: TIBStringField
      FieldName = 'RUKOVOD'
      Origin = 'OTDEL.RUKOVOD'
      Size = 90
    end
  end
  object Q_Dolzhn: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from DOLZHNOST')
    Left = 608
    Top = 184
    object Q_DolzhnID_DOLZH: TIntegerField
      FieldName = 'ID_DOLZH'
      Origin = 'DOLZHNOST.ID_DOLZH'
      Required = True
    end
    object Q_DolzhnNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'DOLZHNOST.NAIMEN'
      Size = 50
    end
  end
  object Q_Sobit: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    sobitie.id_sobit,'
      '    sobitie.naimen,'
      '    sobitie.tip_docum,'
      '    sobitie.soderj_docum,'
      '    sobitie.data_sobit,'
      '    sobitie.otvetstv_lico,'
      '    sobitie.id_sotrud,'
      '    sotrudnik.family,'
      '    sotrudnik.imya,'
      '    sotrudnik.otchestvo'
      'from sotrudnik'
      
        '   inner join sobitie on (sotrudnik.id_sotrud = sobitie.id_sotru' +
        'd)')
    Left = 672
    Top = 184
    object Q_SobitID_SOBIT: TIntegerField
      FieldName = 'ID_SOBIT'
      Origin = 'SOBITIE.ID_SOBIT'
      Required = True
    end
    object Q_SobitNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'SOBITIE.NAIMEN'
      Size = 40
    end
    object Q_SobitTIP_DOCUM: TIBStringField
      FieldName = 'TIP_DOCUM'
      Origin = 'SOBITIE.TIP_DOCUM'
      Size = 15
    end
    object Q_SobitSODERJ_DOCUM: TIBStringField
      FieldName = 'SODERJ_DOCUM'
      Origin = 'SOBITIE.SODERJ_DOCUM'
      Size = 180
    end
    object Q_SobitDATA_SOBIT: TIBStringField
      FieldName = 'DATA_SOBIT'
      Origin = 'SOBITIE.DATA_SOBIT'
      Size = 15
    end
    object Q_SobitOTVETSTV_LICO: TIBStringField
      FieldName = 'OTVETSTV_LICO'
      Origin = 'SOBITIE.OTVETSTV_LICO'
      Size = 90
    end
    object Q_SobitID_SOTRUD: TIntegerField
      FieldName = 'ID_SOTRUD'
      Origin = 'SOBITIE.ID_SOTRUD'
    end
    object Q_SobitFAMILY: TIBStringField
      FieldName = 'FAMILY'
      Origin = 'SOTRUDNIK.FAMILY'
      Size = 35
    end
    object Q_SobitIMYA: TIBStringField
      FieldName = 'IMYA'
      Origin = 'SOTRUDNIK.IMYA'
      Size = 25
    end
    object Q_SobitOTCHESTVO: TIBStringField
      FieldName = 'OTCHESTVO'
      Origin = 'SOTRUDNIK.OTCHESTVO'
      Size = 30
    end
  end
  object Nedvizh_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'NEDVIZH_ADD'
    Left = 200
    Top = 240
  end
  object Nedvizh_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'NEDVIZH_UPD'
    Left = 200
    Top = 296
  end
  object Nedvizh_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'NEDVIZH_DEL'
    Left = 200
    Top = 352
  end
  object Adres_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'ADRES_ADD'
    Left = 264
    Top = 240
  end
  object Adres_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'ADRES_UPD'
    Left = 264
    Top = 296
  end
  object Adres_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'ADRES_DEL'
    Left = 264
    Top = 352
  end
  object Remont_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'REMONT_ADD'
    Left = 328
    Top = 240
  end
  object Remont_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'REMONT_DEL'
    Left = 328
    Top = 296
  end
  object Sotrud_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SOTRUD_ADD'
    Left = 480
    Top = 240
  end
  object Sotrud_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SOTRUD_UPD'
    Left = 480
    Top = 296
  end
  object Sotrud_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SOTRUD_DEL'
    Left = 480
    Top = 352
  end
  object Otdel_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'OTDEL_ADD'
    Left = 544
    Top = 240
  end
  object Otdel_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'OTDEL_UPD'
    Left = 544
    Top = 296
  end
  object Otdel_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'OTDEL_DEL'
    Left = 544
    Top = 352
  end
  object Sobit_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SOBIT_ADD'
    Left = 672
    Top = 240
  end
  object Sobit_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SOBIT_UPD'
    Left = 672
    Top = 296
  end
  object Sobit_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SOBIT_DEL'
    Left = 672
    Top = 352
  end
  object Q_Sotrud1: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    OnCalcFields = Q_Sotrud1CalcFields
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    dolzhnost.naimen,'
      '    otdel.naimen,'
      '    otdel.rukovod,'
      '    sotrudnik.id_sotrud,'
      '    sotrudnik.family,'
      '    sotrudnik.imya,'
      '    sotrudnik.otchestvo,'
      '    sotrudnik.pol,'
      '    sotrudnik.data_rozhden,'
      '    sotrudnik.mesto_zhit,'
      '    sotrudnik.id_dolzh,'
      '    sotrudnik.ser_pasport,'
      '    sotrudnik.nom_pasport,'
      '    sotrudnik.data_vidachi_pasport,'
      '    sotrudnik.kem_vidan_pasport,'
      '    sotrudnik.agres_reg,'
      '    sotrudnik.id_otdela,'
      '    sotrudnik.dom_telefon,'
      '    sotrudnik.rab_telefon'
      'from sotrudnik'
      '   inner join otdel on (sotrudnik.id_otdela = otdel.id_otdela)'
      
        '   inner join dolzhnost on (sotrudnik.id_dolzh = dolzhnost.id_do' +
        'lzh)')
    Left = 144
    Top = 80
    object Q_Sotrud1NAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'DOLZHNOST.NAIMEN'
      Size = 50
    end
    object Q_Sotrud1NAIMEN1: TIBStringField
      FieldName = 'NAIMEN1'
      Origin = 'OTDEL.NAIMEN'
      Size = 35
    end
    object Q_Sotrud1RUKOVOD: TIBStringField
      FieldName = 'RUKOVOD'
      Origin = 'OTDEL.RUKOVOD'
      Size = 90
    end
    object Q_Sotrud1ID_SOTRUD: TIntegerField
      FieldName = 'ID_SOTRUD'
      Origin = 'SOTRUDNIK.ID_SOTRUD'
      Required = True
    end
    object Q_Sotrud1FAMILY: TIBStringField
      FieldName = 'FAMILY'
      Origin = 'SOTRUDNIK.FAMILY'
      Size = 35
    end
    object Q_Sotrud1IMYA: TIBStringField
      FieldName = 'IMYA'
      Origin = 'SOTRUDNIK.IMYA'
      Size = 25
    end
    object Q_Sotrud1OTCHESTVO: TIBStringField
      FieldName = 'OTCHESTVO'
      Origin = 'SOTRUDNIK.OTCHESTVO'
      Size = 30
    end
    object Q_Sotrud1POL: TIBStringField
      FieldName = 'POL'
      Origin = 'SOTRUDNIK.POL'
      Size = 5
    end
    object Q_Sotrud1DATA_ROZHDEN: TIBStringField
      FieldName = 'DATA_ROZHDEN'
      Origin = 'SOTRUDNIK.DATA_ROZHDEN'
      Size = 15
    end
    object Q_Sotrud1MESTO_ZHIT: TIBStringField
      FieldName = 'MESTO_ZHIT'
      Origin = 'SOTRUDNIK.MESTO_ZHIT'
      Size = 120
    end
    object Q_Sotrud1ID_DOLZH: TIntegerField
      FieldName = 'ID_DOLZH'
      Origin = 'SOTRUDNIK.ID_DOLZH'
    end
    object Q_Sotrud1SER_PASPORT: TIBStringField
      FieldName = 'SER_PASPORT'
      Origin = 'SOTRUDNIK.SER_PASPORT'
      Size = 5
    end
    object Q_Sotrud1NOM_PASPORT: TIBStringField
      FieldName = 'NOM_PASPORT'
      Origin = 'SOTRUDNIK.NOM_PASPORT'
      Size = 7
    end
    object Q_Sotrud1DATA_VIDACHI_PASPORT: TIBStringField
      FieldName = 'DATA_VIDACHI_PASPORT'
      Origin = 'SOTRUDNIK.DATA_VIDACHI_PASPORT'
      Size = 15
    end
    object Q_Sotrud1KEM_VIDAN_PASPORT: TIBStringField
      FieldName = 'KEM_VIDAN_PASPORT'
      Origin = 'SOTRUDNIK.KEM_VIDAN_PASPORT'
      Size = 80
    end
    object Q_Sotrud1AGRES_REG: TIBStringField
      FieldName = 'AGRES_REG'
      Origin = 'SOTRUDNIK.AGRES_REG'
      Size = 120
    end
    object Q_Sotrud1ID_OTDELA: TIntegerField
      FieldName = 'ID_OTDELA'
      Origin = 'SOTRUDNIK.ID_OTDELA'
    end
    object Q_Sotrud1DOM_TELEFON: TIBStringField
      FieldName = 'DOM_TELEFON'
      Origin = 'SOTRUDNIK.DOM_TELEFON'
    end
    object Q_Sotrud1RAB_TELEFON: TIBStringField
      FieldName = 'RAB_TELEFON'
      Origin = 'SOTRUDNIK.RAB_TELEFON'
    end
    object Q_Sotrud1FIO: TStringField
      FieldKind = fkCalculated
      FieldName = 'FIO'
      Size = 90
      Calculated = True
    end
  end
  object Q_Vibor: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    sobitie.id_sobit,'
      '    sobitie.naimen,'
      '    sobitie.tip_docum,'
      '    sobitie.soderj_docum,'
      '    sobitie.data_sobit,'
      '    sobitie.otvetstv_lico,'
      '    sobitie.id_sotrud,'
      '    sobitie.id_dolzh,'
      '    dolzhnost.naimen,'
      '    sotrudnik.family,'
      '    sotrudnik.otchestvo,'
      '    sotrudnik.imya'
      'from sobitie'
      
        '   inner join dolzhnost on (sobitie.id_dolzh = dolzhnost.id_dolz' +
        'h)'
      
        '   inner join sotrudnik on (sobitie.id_sotrud = sotrudnik.id_sot' +
        'rud)'
      '   where SOTRUDNIK.ID_SOTRUD=:GRISHA_TSAR')
    Left = 88
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'GRISHA_TSAR'
        ParamType = ptUnknown
      end>
    object Q_ViborID_SOBIT: TIntegerField
      FieldName = 'ID_SOBIT'
      Origin = 'SOBITIE.ID_SOBIT'
      Required = True
    end
    object Q_ViborNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'SOBITIE.NAIMEN'
      Size = 40
    end
    object Q_ViborTIP_DOCUM: TIBStringField
      FieldName = 'TIP_DOCUM'
      Origin = 'SOBITIE.TIP_DOCUM'
      Size = 15
    end
    object Q_ViborSODERJ_DOCUM: TIBStringField
      FieldName = 'SODERJ_DOCUM'
      Origin = 'SOBITIE.SODERJ_DOCUM'
      Size = 180
    end
    object Q_ViborDATA_SOBIT: TIBStringField
      FieldName = 'DATA_SOBIT'
      Origin = 'SOBITIE.DATA_SOBIT'
      Size = 15
    end
    object Q_ViborOTVETSTV_LICO: TIBStringField
      FieldName = 'OTVETSTV_LICO'
      Origin = 'SOBITIE.OTVETSTV_LICO'
      Size = 90
    end
    object Q_ViborID_SOTRUD: TIntegerField
      FieldName = 'ID_SOTRUD'
      Origin = 'SOBITIE.ID_SOTRUD'
    end
    object Q_ViborID_DOLZH: TIntegerField
      FieldName = 'ID_DOLZH'
      Origin = 'SOBITIE.ID_DOLZH'
    end
    object Q_ViborNAIMEN1: TIBStringField
      FieldName = 'NAIMEN1'
      Origin = 'DOLZHNOST.NAIMEN'
      Size = 50
    end
    object Q_ViborFAMILY: TIBStringField
      FieldName = 'FAMILY'
      Origin = 'SOTRUDNIK.FAMILY'
      Size = 35
    end
    object Q_ViborOTCHESTVO: TIBStringField
      FieldName = 'OTCHESTVO'
      Origin = 'SOTRUDNIK.OTCHESTVO'
      Size = 30
    end
    object Q_ViborIMYA: TIBStringField
      FieldName = 'IMYA'
      Origin = 'SOTRUDNIK.IMYA'
      Size = 25
    end
  end
  object DS_Sotrud1: TDataSource
    DataSet = Q_Sotrud1
    Left = 144
    Top = 16
  end
  object DS_Vibor: TDataSource
    DataSet = Q_Vibor
    Left = 88
    Top = 16
  end
  object Q_NedvizhChange: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    vid_nedvizhimosti.naimen,'
      '    vid_remonta.naimen,'
      '    vid_remonta.data,'
      '    adres.gorod,'
      '    adres.rayon,'
      '    adres.ulitca,'
      '    adres.dom,'
      '    nedvizhimost.id_nedvizh,'
      '    nedvizhimost.id_vid_nedvizh,'
      '    nedvizhimost.obsh_ploshad,'
      '    nedvizhimost.id_adresa,'
      '    nedvizhimost.cena,'
      '    nedvizhimost.ploshad_adm_pomeshen,'
      '    nedvizhimost.etazh,'
      '    nedvizhimost.etazhnost,'
      '    nedvizhimost.parking,'
      '    nedvizhimost.srok_expluatac,'
      '    nedvizhimost.id_remont,'
      '    nedvizhimost.nom_kvartiri,'
      '    nedvizhimost.pocht_index'
      'from nedvizhimost'
      
        '   inner join adres on (nedvizhimost.id_adresa = adres.id_adresa' +
        ')'
      
        '   inner join vid_remonta on (nedvizhimost.id_remont = vid_remon' +
        'ta.id_remont)'
      
        '   inner join vid_nedvizhimosti on (nedvizhimost.id_vid_nedvizh ' +
        '= vid_nedvizhimosti.id_vid_nedvizh)'
      'where vid_nedvizhimosti.naimen=:naimenovanie')
    Left = 232
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'naimenovanie'
        ParamType = ptUnknown
      end>
    object Q_NedvizhChangeNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'VID_NEDVIZHIMOSTI.NAIMEN'
      Size = 15
    end
    object Q_NedvizhChangeNAIMEN1: TIBStringField
      FieldName = 'NAIMEN1'
      Origin = 'VID_REMONTA.NAIMEN'
      Size = 40
    end
    object Q_NedvizhChangeDATA: TIBStringField
      FieldName = 'DATA'
      Origin = 'VID_REMONTA.DATA'
      Size = 15
    end
    object Q_NedvizhChangeGOROD: TIBStringField
      FieldName = 'GOROD'
      Origin = 'ADRES.GOROD'
      Size = 40
    end
    object Q_NedvizhChangeRAYON: TIBStringField
      FieldName = 'RAYON'
      Origin = 'ADRES.RAYON'
      Size = 35
    end
    object Q_NedvizhChangeULITCA: TIBStringField
      FieldName = 'ULITCA'
      Origin = 'ADRES.ULITCA'
      Size = 50
    end
    object Q_NedvizhChangeDOM: TIBStringField
      FieldName = 'DOM'
      Origin = 'ADRES.DOM'
      Size = 10
    end
    object Q_NedvizhChangeID_NEDVIZH: TIntegerField
      FieldName = 'ID_NEDVIZH'
      Origin = 'NEDVIZHIMOST.ID_NEDVIZH'
      Required = True
    end
    object Q_NedvizhChangeID_VID_NEDVIZH: TIntegerField
      FieldName = 'ID_VID_NEDVIZH'
      Origin = 'NEDVIZHIMOST.ID_VID_NEDVIZH'
    end
    object Q_NedvizhChangeOBSH_PLOSHAD: TIBStringField
      FieldName = 'OBSH_PLOSHAD'
      Origin = 'NEDVIZHIMOST.OBSH_PLOSHAD'
      Size = 10
    end
    object Q_NedvizhChangeID_ADRESA: TIntegerField
      FieldName = 'ID_ADRESA'
      Origin = 'NEDVIZHIMOST.ID_ADRESA'
    end
    object Q_NedvizhChangeCENA: TIntegerField
      FieldName = 'CENA'
      Origin = 'NEDVIZHIMOST.CENA'
    end
    object Q_NedvizhChangePLOSHAD_ADM_POMESHEN: TIBStringField
      FieldName = 'PLOSHAD_ADM_POMESHEN'
      Origin = 'NEDVIZHIMOST.PLOSHAD_ADM_POMESHEN'
      Size = 10
    end
    object Q_NedvizhChangeETAZH: TIntegerField
      FieldName = 'ETAZH'
      Origin = 'NEDVIZHIMOST.ETAZH'
    end
    object Q_NedvizhChangeETAZHNOST: TIntegerField
      FieldName = 'ETAZHNOST'
      Origin = 'NEDVIZHIMOST.ETAZHNOST'
    end
    object Q_NedvizhChangePARKING: TIBStringField
      FieldName = 'PARKING'
      Origin = 'NEDVIZHIMOST.PARKING'
      FixedChar = True
      Size = 5
    end
    object Q_NedvizhChangeSROK_EXPLUATAC: TIntegerField
      FieldName = 'SROK_EXPLUATAC'
      Origin = 'NEDVIZHIMOST.SROK_EXPLUATAC'
    end
    object Q_NedvizhChangeID_REMONT: TIntegerField
      FieldName = 'ID_REMONT'
      Origin = 'NEDVIZHIMOST.ID_REMONT'
    end
    object Q_NedvizhChangeNOM_KVARTIRI: TIntegerField
      FieldName = 'NOM_KVARTIRI'
      Origin = 'NEDVIZHIMOST.NOM_KVARTIRI'
    end
    object Q_NedvizhChangePOCHT_INDEX: TIBStringField
      FieldName = 'POCHT_INDEX'
      Origin = 'NEDVIZHIMOST.POCHT_INDEX'
      Size = 6
    end
  end
  object DS_NedvizhChange: TDataSource
    DataSet = Q_NedvizhChange
    Left = 232
    Top = 8
  end
  object DS_Prodavec: TDataSource
    DataSet = Q_Prodavec
    Left = 744
    Top = 120
  end
  object Q_Prodavec: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from PRODAVEC')
    Left = 744
    Top = 184
    object Q_ProdavecID_PRODAVCA: TIntegerField
      FieldName = 'ID_PRODAVCA'
      Origin = 'PRODAVEC.ID_PRODAVCA'
      Required = True
    end
    object Q_ProdavecFAMILY: TIBStringField
      FieldName = 'FAMILY'
      Origin = 'PRODAVEC.FAMILY'
      Size = 30
    end
    object Q_ProdavecIMYA: TIBStringField
      FieldName = 'IMYA'
      Origin = 'PRODAVEC.IMYA'
      Size = 30
    end
    object Q_ProdavecOTCHESTVO: TIBStringField
      FieldName = 'OTCHESTVO'
      Origin = 'PRODAVEC.OTCHESTVO'
      Size = 30
    end
    object Q_ProdavecTEL: TIBStringField
      FieldName = 'TEL'
      Origin = 'PRODAVEC.TEL'
    end
    object Q_ProdavecSER_PASPORT: TIBStringField
      FieldName = 'SER_PASPORT'
      Origin = 'PRODAVEC.SER_PASPORT'
      Size = 5
    end
    object Q_ProdavecNOM_PASPORT: TIBStringField
      FieldName = 'NOM_PASPORT'
      Origin = 'PRODAVEC.NOM_PASPORT'
      Size = 7
    end
    object Q_ProdavecDATA_VIDACH_PASPORT: TIBStringField
      FieldName = 'DATA_VIDACH_PASPORT'
      Origin = 'PRODAVEC.DATA_VIDACH_PASPORT'
      Size = 10
    end
    object Q_ProdavecKEM_VIDAN_PASPORT: TIBStringField
      FieldName = 'KEM_VIDAN_PASPORT'
      Origin = 'PRODAVEC.KEM_VIDAN_PASPORT'
      Size = 60
    end
    object Q_ProdavecADRES_REG: TIBStringField
      FieldName = 'ADRES_REG'
      Origin = 'PRODAVEC.ADRES_REG'
      Size = 120
    end
  end
  object Prodavec_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'PRODAVEC_ADD'
    Left = 744
    Top = 240
  end
  object Prodavec_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'PRODAVEC_UPD'
    Left = 744
    Top = 296
  end
  object Prodavec_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'PRODAVEC_DEL'
    Left = 744
    Top = 352
  end
  object Q_AgentChange: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    dolzhnost.naimen,'
      '    otdel.naimen,'
      '    otdel.rukovod,'
      '    sotrudnik.id_sotrud,'
      '    sotrudnik.family,'
      '    sotrudnik.imya,'
      '    sotrudnik.otchestvo,'
      '    sotrudnik.pol,'
      '    sotrudnik.data_rozhden,'
      '    sotrudnik.mesto_zhit,'
      '    sotrudnik.id_dolzh,'
      '    sotrudnik.ser_pasport,'
      '    sotrudnik.nom_pasport,'
      '    sotrudnik.data_vidachi_pasport,'
      '    sotrudnik.kem_vidan_pasport,'
      '    sotrudnik.agres_reg,'
      '    sotrudnik.id_otdela,'
      '    sotrudnik.dom_telefon,'
      '    sotrudnik.rab_telefon'
      'from sotrudnik'
      '   inner join otdel on (sotrudnik.id_otdela = otdel.id_otdela)'
      
        '   inner join dolzhnost on (sotrudnik.id_dolzh = dolzhnost.id_do' +
        'lzh)'
      'where dolzhnost.naimen='#39#1040#1075#1077#1085#1090#39)
    Left = 336
    Top = 64
    object Q_AgentChangeNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'DOLZHNOST.NAIMEN'
      Size = 50
    end
    object Q_AgentChangeNAIMEN1: TIBStringField
      FieldName = 'NAIMEN1'
      Origin = 'OTDEL.NAIMEN'
      Size = 35
    end
    object Q_AgentChangeRUKOVOD: TIBStringField
      FieldName = 'RUKOVOD'
      Origin = 'OTDEL.RUKOVOD'
      Size = 90
    end
    object Q_AgentChangeID_SOTRUD: TIntegerField
      FieldName = 'ID_SOTRUD'
      Origin = 'SOTRUDNIK.ID_SOTRUD'
      Required = True
    end
    object Q_AgentChangeFAMILY: TIBStringField
      FieldName = 'FAMILY'
      Origin = 'SOTRUDNIK.FAMILY'
      Size = 35
    end
    object Q_AgentChangeIMYA: TIBStringField
      FieldName = 'IMYA'
      Origin = 'SOTRUDNIK.IMYA'
      Size = 25
    end
    object Q_AgentChangeOTCHESTVO: TIBStringField
      FieldName = 'OTCHESTVO'
      Origin = 'SOTRUDNIK.OTCHESTVO'
      Size = 30
    end
    object Q_AgentChangePOL: TIBStringField
      FieldName = 'POL'
      Origin = 'SOTRUDNIK.POL'
      Size = 5
    end
    object Q_AgentChangeDATA_ROZHDEN: TIBStringField
      FieldName = 'DATA_ROZHDEN'
      Origin = 'SOTRUDNIK.DATA_ROZHDEN'
      Size = 15
    end
    object Q_AgentChangeMESTO_ZHIT: TIBStringField
      FieldName = 'MESTO_ZHIT'
      Origin = 'SOTRUDNIK.MESTO_ZHIT'
      Size = 120
    end
    object Q_AgentChangeID_DOLZH: TIntegerField
      FieldName = 'ID_DOLZH'
      Origin = 'SOTRUDNIK.ID_DOLZH'
    end
    object Q_AgentChangeSER_PASPORT: TIBStringField
      FieldName = 'SER_PASPORT'
      Origin = 'SOTRUDNIK.SER_PASPORT'
      Size = 5
    end
    object Q_AgentChangeNOM_PASPORT: TIBStringField
      FieldName = 'NOM_PASPORT'
      Origin = 'SOTRUDNIK.NOM_PASPORT'
      Size = 7
    end
    object Q_AgentChangeDATA_VIDACHI_PASPORT: TIBStringField
      FieldName = 'DATA_VIDACHI_PASPORT'
      Origin = 'SOTRUDNIK.DATA_VIDACHI_PASPORT'
      Size = 15
    end
    object Q_AgentChangeKEM_VIDAN_PASPORT: TIBStringField
      FieldName = 'KEM_VIDAN_PASPORT'
      Origin = 'SOTRUDNIK.KEM_VIDAN_PASPORT'
      Size = 80
    end
    object Q_AgentChangeAGRES_REG: TIBStringField
      FieldName = 'AGRES_REG'
      Origin = 'SOTRUDNIK.AGRES_REG'
      Size = 120
    end
    object Q_AgentChangeID_OTDELA: TIntegerField
      FieldName = 'ID_OTDELA'
      Origin = 'SOTRUDNIK.ID_OTDELA'
    end
    object Q_AgentChangeDOM_TELEFON: TIBStringField
      FieldName = 'DOM_TELEFON'
      Origin = 'SOTRUDNIK.DOM_TELEFON'
    end
    object Q_AgentChangeRAB_TELEFON: TIBStringField
      FieldName = 'RAB_TELEFON'
      Origin = 'SOTRUDNIK.RAB_TELEFON'
    end
  end
  object DS_AgentChange: TDataSource
    DataSet = Q_AgentChange
    Left = 336
    Top = 8
  end
  object DS_Pokup: TDataSource
    DataSet = Q_Pokup
    Left = 816
    Top = 120
  end
  object Pokup_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'POKUP_ADD'
    Left = 816
    Top = 240
  end
  object Pokup_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'POKUP_UPD'
    Left = 816
    Top = 296
  end
  object Pokup_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'POKUP_DEL'
    Left = 816
    Top = 352
  end
  object DS_Sdelka: TDataSource
    DataSet = Q_Sdelka
    Left = 880
    Top = 120
  end
  object Sdelka_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SDELKA_ADD'
    Left = 880
    Top = 240
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID_POKUP'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_NEDVIZH'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NOM_DOGV'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_PRODAVCA'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_SOTRUD'
        ParamType = ptInput
      end>
  end
  object Sdelka_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SDELKA_UPD'
    Left = 880
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID_USL'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_POKUP'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_NEDVIZH'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NOM_DOGV'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_PRODAVCA'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID_SOTRUD'
        ParamType = ptInput
      end>
  end
  object Sdelka_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SDELKA_DEL'
    Left = 880
    Top = 352
  end
  object Q_Pokup: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from POKUPATEL')
    Left = 816
    Top = 184
    object Q_PokupID_POKUP: TIntegerField
      FieldName = 'ID_POKUP'
      Origin = 'POKUPATEL.ID_POKUP'
      Required = True
    end
    object Q_PokupFAMILY: TIBStringField
      FieldName = 'FAMILY'
      Origin = 'POKUPATEL.FAMILY'
      Size = 30
    end
    object Q_PokupIMYA: TIBStringField
      FieldName = 'IMYA'
      Origin = 'POKUPATEL.IMYA'
      Size = 30
    end
    object Q_PokupOTCHESTVO: TIBStringField
      FieldName = 'OTCHESTVO'
      Origin = 'POKUPATEL.OTCHESTVO'
      Size = 30
    end
    object Q_PokupTEL: TIBStringField
      FieldName = 'TEL'
      Origin = 'POKUPATEL.TEL'
    end
    object Q_PokupSER_PASPORT: TIBStringField
      FieldName = 'SER_PASPORT'
      Origin = 'POKUPATEL.SER_PASPORT'
      Size = 5
    end
    object Q_PokupNOM_PASPORT: TIBStringField
      FieldName = 'NOM_PASPORT'
      Origin = 'POKUPATEL.NOM_PASPORT'
      Size = 7
    end
    object Q_PokupDATA_VIDACHI_PASPORT: TIBStringField
      FieldName = 'DATA_VIDACHI_PASPORT'
      Origin = 'POKUPATEL.DATA_VIDACHI_PASPORT'
      Size = 10
    end
    object Q_PokupKEM_VIDAN_PASPORT: TIBStringField
      FieldName = 'KEM_VIDAN_PASPORT'
      Origin = 'POKUPATEL.KEM_VIDAN_PASPORT'
      Size = 60
    end
    object Q_PokupADRES_REG: TIBStringField
      FieldName = 'ADRES_REG'
      Origin = 'POKUPATEL.ADRES_REG'
      Size = 120
    end
    object Q_PokupMEST_RABOTI: TIBStringField
      FieldName = 'MEST_RABOTI'
      Origin = 'POKUPATEL.MEST_RABOTI'
      Size = 50
    end
    object Q_PokupMES_DOHOD: TIBStringField
      FieldName = 'MES_DOHOD'
      Origin = 'POKUPATEL.MES_DOHOD'
      Size = 50
    end
    object Q_PokupNOM_BANK_KARTI: TIBStringField
      FieldName = 'NOM_BANK_KARTI'
      Origin = 'POKUPATEL.NOM_BANK_KARTI'
      Size = 30
    end
    object Q_PokupDATA_ISTECH_KARTI: TIBStringField
      FieldName = 'DATA_ISTECH_KARTI'
      Origin = 'POKUPATEL.DATA_ISTECH_KARTI'
      Size = 10
    end
    object Q_PokupNAIMEN_VLADELCA: TIBStringField
      FieldName = 'NAIMEN_VLADELCA'
      Origin = 'POKUPATEL.NAIMEN_VLADELCA'
      Size = 30
    end
  end
  object Q_Sdelka: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    nedvizhimost.id_vid_nedvizh,'
      '    nedvizhimost.obsh_ploshad,'
      '    nedvizhimost.id_adresa,'
      '    nedvizhimost.cena,'
      '    nedvizhimost.ploshad_adm_pomeshen,'
      '    nedvizhimost.etazh,'
      '    nedvizhimost.etazhnost,'
      '    nedvizhimost.parking,'
      '    nedvizhimost.srok_expluatac,'
      '    nedvizhimost.id_remont,'
      '    nedvizhimost.nom_kvartiri,'
      '    nedvizhimost.pocht_index,'
      '    usluga.id_usl,'
      '    usluga.id_pokup,'
      '    usluga.id_nedvizh,'
      '    usluga.nom_dogv,'
      '    usluga.id_prodavca,'
      '    usluga.id_sotrud,'
      '    sotrudnik.family,'
      '    sotrudnik.imya,'
      '    sotrudnik.otchestvo,'
      '    sotrudnik.rab_telefon,'
      '    prodavec.family,'
      '    prodavec.imya,'
      '    prodavec.otchestvo,'
      '    prodavec.tel,'
      '    prodavec.ser_pasport,'
      '    prodavec.nom_pasport,'
      '    prodavec.data_vidach_pasport,'
      '    prodavec.kem_vidan_pasport,'
      '    prodavec.adres_reg,'
      '    pokupatel.family,'
      '    pokupatel.imya,'
      '    pokupatel.otchestvo,'
      '    pokupatel.tel,'
      '    pokupatel.ser_pasport,'
      '    pokupatel.nom_pasport,'
      '    pokupatel.data_vidachi_pasport,'
      '    pokupatel.kem_vidan_pasport,'
      '    pokupatel.adres_reg,'
      '    pokupatel.mest_raboti,'
      '    pokupatel.mes_dohod,'
      '    pokupatel.nom_bank_karti,'
      '    pokupatel.data_istech_karti,'
      '    pokupatel.naimen_vladelca,'
      '    adres.gorod,'
      '    adres.rayon,'
      '    adres.ulitca,'
      '    adres.dom,'
      '    vid_nedvizhimosti.naimen,'
      '    vid_remonta.naimen,'
      '    vid_remonta.data'
      'from pokupatel'
      '   inner join usluga on (pokupatel.id_pokup = usluga.id_pokup)'
      
        '   inner join sotrudnik on (usluga.id_sotrud = sotrudnik.id_sotr' +
        'ud)'
      
        '   inner join prodavec on (usluga.id_prodavca = prodavec.id_prod' +
        'avca)'
      
        '   inner join nedvizhimost on (usluga.id_nedvizh = nedvizhimost.' +
        'id_nedvizh)'
      
        '   inner join adres on (nedvizhimost.id_adresa = adres.id_adresa' +
        ')'
      
        '   inner join vid_nedvizhimosti on (nedvizhimost.id_vid_nedvizh ' +
        '= vid_nedvizhimosti.id_vid_nedvizh)'
      
        '   inner join vid_remonta on (nedvizhimost.id_remont = vid_remon' +
        'ta.id_remont)'
      'order by  usluga.nom_dogv ASC')
    Left = 880
    Top = 184
    object Q_SdelkaID_VID_NEDVIZH: TIntegerField
      FieldName = 'ID_VID_NEDVIZH'
      Origin = 'NEDVIZHIMOST.ID_VID_NEDVIZH'
    end
    object Q_SdelkaOBSH_PLOSHAD: TIBStringField
      FieldName = 'OBSH_PLOSHAD'
      Origin = 'NEDVIZHIMOST.OBSH_PLOSHAD'
      Size = 10
    end
    object Q_SdelkaID_ADRESA: TIntegerField
      FieldName = 'ID_ADRESA'
      Origin = 'NEDVIZHIMOST.ID_ADRESA'
    end
    object Q_SdelkaCENA: TIntegerField
      FieldName = 'CENA'
      Origin = 'NEDVIZHIMOST.CENA'
    end
    object Q_SdelkaPLOSHAD_ADM_POMESHEN: TIBStringField
      FieldName = 'PLOSHAD_ADM_POMESHEN'
      Origin = 'NEDVIZHIMOST.PLOSHAD_ADM_POMESHEN'
      Size = 10
    end
    object Q_SdelkaETAZH: TIntegerField
      FieldName = 'ETAZH'
      Origin = 'NEDVIZHIMOST.ETAZH'
    end
    object Q_SdelkaETAZHNOST: TIntegerField
      FieldName = 'ETAZHNOST'
      Origin = 'NEDVIZHIMOST.ETAZHNOST'
    end
    object Q_SdelkaPARKING: TIBStringField
      FieldName = 'PARKING'
      Origin = 'NEDVIZHIMOST.PARKING'
      FixedChar = True
      Size = 5
    end
    object Q_SdelkaSROK_EXPLUATAC: TIntegerField
      FieldName = 'SROK_EXPLUATAC'
      Origin = 'NEDVIZHIMOST.SROK_EXPLUATAC'
    end
    object Q_SdelkaID_REMONT: TIntegerField
      FieldName = 'ID_REMONT'
      Origin = 'NEDVIZHIMOST.ID_REMONT'
    end
    object Q_SdelkaNOM_KVARTIRI: TIntegerField
      FieldName = 'NOM_KVARTIRI'
      Origin = 'NEDVIZHIMOST.NOM_KVARTIRI'
    end
    object Q_SdelkaPOCHT_INDEX: TIBStringField
      FieldName = 'POCHT_INDEX'
      Origin = 'NEDVIZHIMOST.POCHT_INDEX'
      Size = 6
    end
    object Q_SdelkaID_USL: TIntegerField
      FieldName = 'ID_USL'
      Origin = 'USLUGA.ID_USL'
      Required = True
    end
    object Q_SdelkaID_POKUP: TIntegerField
      FieldName = 'ID_POKUP'
      Origin = 'USLUGA.ID_POKUP'
    end
    object Q_SdelkaID_NEDVIZH: TIntegerField
      FieldName = 'ID_NEDVIZH'
      Origin = 'USLUGA.ID_NEDVIZH'
    end
    object Q_SdelkaNOM_DOGV: TIBStringField
      FieldName = 'NOM_DOGV'
      Origin = 'USLUGA.NOM_DOGV'
      Size = 10
    end
    object Q_SdelkaID_PRODAVCA: TIntegerField
      FieldName = 'ID_PRODAVCA'
      Origin = 'USLUGA.ID_PRODAVCA'
    end
    object Q_SdelkaID_SOTRUD: TIntegerField
      FieldName = 'ID_SOTRUD'
      Origin = 'USLUGA.ID_SOTRUD'
    end
    object Q_SdelkaFAMILY: TIBStringField
      FieldName = 'FAMILY'
      Origin = 'SOTRUDNIK.FAMILY'
      Size = 35
    end
    object Q_SdelkaIMYA: TIBStringField
      FieldName = 'IMYA'
      Origin = 'SOTRUDNIK.IMYA'
      Size = 25
    end
    object Q_SdelkaOTCHESTVO: TIBStringField
      FieldName = 'OTCHESTVO'
      Origin = 'SOTRUDNIK.OTCHESTVO'
      Size = 30
    end
    object Q_SdelkaRAB_TELEFON: TIBStringField
      FieldName = 'RAB_TELEFON'
      Origin = 'SOTRUDNIK.RAB_TELEFON'
    end
    object Q_SdelkaFAMILY1: TIBStringField
      FieldName = 'FAMILY1'
      Origin = 'PRODAVEC.FAMILY'
      Size = 30
    end
    object Q_SdelkaIMYA1: TIBStringField
      FieldName = 'IMYA1'
      Origin = 'PRODAVEC.IMYA'
      Size = 30
    end
    object Q_SdelkaOTCHESTVO1: TIBStringField
      FieldName = 'OTCHESTVO1'
      Origin = 'PRODAVEC.OTCHESTVO'
      Size = 30
    end
    object Q_SdelkaTEL: TIBStringField
      FieldName = 'TEL'
      Origin = 'PRODAVEC.TEL'
    end
    object Q_SdelkaSER_PASPORT: TIBStringField
      FieldName = 'SER_PASPORT'
      Origin = 'PRODAVEC.SER_PASPORT'
      Size = 5
    end
    object Q_SdelkaNOM_PASPORT: TIBStringField
      FieldName = 'NOM_PASPORT'
      Origin = 'PRODAVEC.NOM_PASPORT'
      Size = 7
    end
    object Q_SdelkaDATA_VIDACH_PASPORT: TIBStringField
      FieldName = 'DATA_VIDACH_PASPORT'
      Origin = 'PRODAVEC.DATA_VIDACH_PASPORT'
      Size = 10
    end
    object Q_SdelkaKEM_VIDAN_PASPORT: TIBStringField
      FieldName = 'KEM_VIDAN_PASPORT'
      Origin = 'PRODAVEC.KEM_VIDAN_PASPORT'
      Size = 60
    end
    object Q_SdelkaADRES_REG: TIBStringField
      FieldName = 'ADRES_REG'
      Origin = 'PRODAVEC.ADRES_REG'
      Size = 120
    end
    object Q_SdelkaFAMILY2: TIBStringField
      FieldName = 'FAMILY2'
      Origin = 'POKUPATEL.FAMILY'
      Size = 30
    end
    object Q_SdelkaIMYA2: TIBStringField
      FieldName = 'IMYA2'
      Origin = 'POKUPATEL.IMYA'
      Size = 30
    end
    object Q_SdelkaOTCHESTVO2: TIBStringField
      FieldName = 'OTCHESTVO2'
      Origin = 'POKUPATEL.OTCHESTVO'
      Size = 30
    end
    object Q_SdelkaTEL1: TIBStringField
      FieldName = 'TEL1'
      Origin = 'POKUPATEL.TEL'
    end
    object Q_SdelkaSER_PASPORT1: TIBStringField
      FieldName = 'SER_PASPORT1'
      Origin = 'POKUPATEL.SER_PASPORT'
      Size = 5
    end
    object Q_SdelkaNOM_PASPORT1: TIBStringField
      FieldName = 'NOM_PASPORT1'
      Origin = 'POKUPATEL.NOM_PASPORT'
      Size = 7
    end
    object Q_SdelkaDATA_VIDACHI_PASPORT: TIBStringField
      FieldName = 'DATA_VIDACHI_PASPORT'
      Origin = 'POKUPATEL.DATA_VIDACHI_PASPORT'
      Size = 10
    end
    object Q_SdelkaKEM_VIDAN_PASPORT1: TIBStringField
      FieldName = 'KEM_VIDAN_PASPORT1'
      Origin = 'POKUPATEL.KEM_VIDAN_PASPORT'
      Size = 60
    end
    object Q_SdelkaADRES_REG1: TIBStringField
      FieldName = 'ADRES_REG1'
      Origin = 'POKUPATEL.ADRES_REG'
      Size = 120
    end
    object Q_SdelkaMEST_RABOTI: TIBStringField
      FieldName = 'MEST_RABOTI'
      Origin = 'POKUPATEL.MEST_RABOTI'
      Size = 50
    end
    object Q_SdelkaMES_DOHOD: TIBStringField
      FieldName = 'MES_DOHOD'
      Origin = 'POKUPATEL.MES_DOHOD'
      Size = 50
    end
    object Q_SdelkaNOM_BANK_KARTI: TIBStringField
      FieldName = 'NOM_BANK_KARTI'
      Origin = 'POKUPATEL.NOM_BANK_KARTI'
      Size = 30
    end
    object Q_SdelkaDATA_ISTECH_KARTI: TIBStringField
      FieldName = 'DATA_ISTECH_KARTI'
      Origin = 'POKUPATEL.DATA_ISTECH_KARTI'
      Size = 10
    end
    object Q_SdelkaNAIMEN_VLADELCA: TIBStringField
      FieldName = 'NAIMEN_VLADELCA'
      Origin = 'POKUPATEL.NAIMEN_VLADELCA'
      Size = 30
    end
    object Q_SdelkaGOROD: TIBStringField
      FieldName = 'GOROD'
      Origin = 'ADRES.GOROD'
      Size = 40
    end
    object Q_SdelkaRAYON: TIBStringField
      FieldName = 'RAYON'
      Origin = 'ADRES.RAYON'
      Size = 35
    end
    object Q_SdelkaULITCA: TIBStringField
      FieldName = 'ULITCA'
      Origin = 'ADRES.ULITCA'
      Size = 50
    end
    object Q_SdelkaDOM: TIBStringField
      FieldName = 'DOM'
      Origin = 'ADRES.DOM'
      Size = 10
    end
    object Q_SdelkaNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'VID_NEDVIZHIMOSTI.NAIMEN'
      Size = 15
    end
    object Q_SdelkaNAIMEN1: TIBStringField
      FieldName = 'NAIMEN1'
      Origin = 'VID_REMONTA.NAIMEN'
      Size = 40
    end
    object Q_SdelkaDATA: TIBStringField
      FieldName = 'DATA'
      Origin = 'VID_REMONTA.DATA'
      Size = 15
    end
  end
  object Q_Vibor2: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from VIPLATA'
      'where ID_USL=:GRISHA_NETSAR'
      'order by  viplata.ostat_vipl desc')
    Filtered = True
    Left = 88
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'GRISHA_NETSAR'
        ParamType = ptUnknown
      end>
    object Q_Vibor2ID_VIPLATI: TIntegerField
      FieldName = 'ID_VIPLATI'
      Origin = 'VIPLATA.ID_VIPLATI'
      Required = True
    end
    object Q_Vibor2DATA_VIPL: TIBStringField
      FieldName = 'DATA_VIPL'
      Origin = 'VIPLATA.DATA_VIPL'
      Size = 15
    end
    object Q_Vibor2SUM_VIPL: TIBStringField
      FieldName = 'SUM_VIPL'
      Origin = 'VIPLATA.SUM_VIPL'
      Size = 15
    end
    object Q_Vibor2OSTAT_VIPL: TIBStringField
      FieldName = 'OSTAT_VIPL'
      Origin = 'VIPLATA.OSTAT_VIPL'
    end
    object Q_Vibor2ID_USL: TIntegerField
      FieldName = 'ID_USL'
      Origin = 'VIPLATA.ID_USL'
    end
  end
  object DS_Vibor2: TDataSource
    DataSet = Q_Vibor2
    Left = 88
    Top = 136
  end
  object DS_Viplati: TDataSource
    Left = 952
    Top = 120
  end
  object Q_Viplati: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    viplata.data_vipl,'
      '    viplata.sum_vipl,'
      '    viplata.ostat_vipl,'
      '    viplata.id_usl,'
      '    viplata.id_viplati,'
      '    usluga.nom_dogv'
      'from usluga'
      '   inner join viplata on (usluga.id_usl = viplata.id_usl)'
      '   order by  viplata.ostat_vipl desc'
      '')
    Left = 952
    Top = 184
    object Q_ViplatiDATA_VIPL: TIBStringField
      FieldName = 'DATA_VIPL'
      Origin = 'VIPLATA.DATA_VIPL'
      Size = 15
    end
    object Q_ViplatiSUM_VIPL: TIBStringField
      FieldName = 'SUM_VIPL'
      Origin = 'VIPLATA.SUM_VIPL'
      Size = 15
    end
    object Q_ViplatiOSTAT_VIPL: TIBStringField
      FieldName = 'OSTAT_VIPL'
      Origin = 'VIPLATA.OSTAT_VIPL'
    end
    object Q_ViplatiID_USL: TIntegerField
      FieldName = 'ID_USL'
      Origin = 'VIPLATA.ID_USL'
    end
    object Q_ViplatiID_VIPLATI: TIntegerField
      FieldName = 'ID_VIPLATI'
      Origin = 'VIPLATA.ID_VIPLATI'
      Required = True
    end
    object Q_ViplatiNOM_DOGV: TIBStringField
      FieldName = 'NOM_DOGV'
      Origin = 'USLUGA.NOM_DOGV'
      Size = 10
    end
  end
  object Viplata_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'VIPLATA_ADD'
    Left = 952
    Top = 240
  end
  object Viplata_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'VIPLATA_DEL'
    Left = 952
    Top = 296
  end
  object Q_Prava: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from AUTORIZ')
    Left = 24
    Top = 136
    object Q_PravaID_AUTOR: TIntegerField
      FieldName = 'ID_AUTOR'
      Origin = 'AUTORIZ.ID_AUTOR'
      Required = True
    end
    object Q_PravaLOGIN: TIBStringField
      FieldName = 'LOGIN'
      Origin = 'AUTORIZ.LOGIN'
      Size = 25
    end
    object Q_PravaPASSWORD: TIBStringField
      FieldName = 'PASSWORD'
      Origin = 'AUTORIZ.PASSWORD'
    end
    object Q_PravaPRAVA: TIntegerField
      FieldName = 'PRAVA'
      Origin = 'AUTORIZ.PRAVA'
    end
  end
  object DS_Users: TDataSource
    DataSet = Q_Users
    Left = 1016
    Top = 120
  end
  object Q_Users: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from AUTORIZ'
      'where ID_AUTOR<>1')
    Left = 1016
    Top = 184
    object Q_UsersID_AUTOR: TIntegerField
      FieldName = 'ID_AUTOR'
      Origin = 'AUTORIZ.ID_AUTOR'
      Required = True
    end
    object Q_UsersLOGIN: TIBStringField
      FieldName = 'LOGIN'
      Origin = 'AUTORIZ.LOGIN'
      Size = 25
    end
    object Q_UsersPASSWORD: TIBStringField
      FieldName = 'PASSWORD'
      Origin = 'AUTORIZ.PASSWORD'
    end
    object Q_UsersPRAVA: TIntegerField
      FieldName = 'PRAVA'
      Origin = 'AUTORIZ.PRAVA'
    end
  end
  object Users_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'USER_ADD'
    Left = 1016
    Top = 240
  end
  object Users_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'USER_DEL'
    Left = 1016
    Top = 352
  end
  object Users_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'USER_UPD'
    Left = 1016
    Top = 296
  end
  object Q_Znach: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    Left = 408
    Top = 64
  end
end
