unit UnitSotrUpd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, StdCtrls, Buttons, Mask, ComCtrls;

type
  TSotrudUpd = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Edit8: TEdit;
    Edit9: TEdit;
    Edit10: TEdit;
    Edit25: TEdit;
    GroupBox5: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    GroupBox7: TGroupBox;
    Edit30: TEdit;
    Edit31: TEdit;
    GroupBox8: TGroupBox;
    Label24: TLabel;
    GroupBox9: TGroupBox;
    Label26: TLabel;
    Label25: TLabel;
    Edit32: TEdit;
    Edit33: TEdit;
    Edit34: TEdit;
    GroupBox10: TGroupBox;
    GroupBox6: TGroupBox;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Edit11: TEdit;
    Edit12: TEdit;
    Edit13: TEdit;
    ComboBox3: TComboBox;
    DateTimePicker1: TDateTimePicker;
    GroupBox11: TGroupBox;
    GroupBox12: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    BitBtn7: TBitBtn;
    GroupBox16: TGroupBox;
    Label44: TLabel;
    BitBtn13: TBitBtn;
    BitBtn14: TBitBtn;
    DBLookupComboBox2: TDBLookupComboBox;
    GroupBox17: TGroupBox;
    Label48: TLabel;
    Label49: TLabel;
    Edit23: TEdit;
    Edit24: TEdit;
    BitBtn15: TBitBtn;
    GroupBox13: TGroupBox;
    GroupBox15: TGroupBox;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label54: TLabel;
    MaskEdit8: TMaskEdit;
    MaskEdit9: TMaskEdit;
    DateTimePicker4: TDateTimePicker;
    GroupBox14: TGroupBox;
    Edit22: TEdit;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    RichEdit1: TRichEdit;
    RichEdit2: TRichEdit;
    RichEdit3: TRichEdit;
    RichEdit4: TRichEdit;
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBLookupComboBox2Enter(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SotrudUpd: TSotrudUpd;

implementation

uses DM;

{$R *.dfm}

procedure TSotrudUpd.BitBtn7Click(Sender: TObject);
begin
  GroupBox6.Visible:=False;
  GroupBox13.Visible:=True;
end;

procedure TSotrudUpd.BitBtn12Click(Sender: TObject);
begin
  GroupBox6.Visible:=True;
  GroupBox13.Visible:=False;
end;

procedure TSotrudUpd.BitBtn11Click(Sender: TObject);
begin
  GroupBox13.Visible:=False;
  GroupBox16.Visible:=True;
end;

procedure TSotrudUpd.BitBtn14Click(Sender: TObject);
begin
  GroupBox13.Visible:=True;
  GroupBox16.Visible:=False;
end;

procedure TSotrudUpd.FormShow(Sender: TObject);
begin
  Edit11.Text:=Edit1.Text;
  Edit12.Text:=Edit2.Text;
  Edit13.Text:=Edit3.Text;
  if Edit8.Text='���.' then
  ComboBox3.ItemIndex:=0
  else
  ComboBox3.ItemIndex:=1;
  DateTimePicker1.Date:=StrToDate(Edit9.Text);
  RichEdit3.Lines.Add(RichEdit1.Lines.Text);
  MaskEdit6.Text:=Edit10.Text;
  MaskEdit7.Text:=Edit25.Text;
  MaskEdit8.Text:=MaskEdit1.Text;
  MaskEdit9.Text:=MaskEdit2.Text;
  DateTimePicker4.Date:=StrToDate(Edit31.Text);
  Edit22.Text:=Edit30.Text;
  RichEdit4.Lines.Add(RichEdit2.Lines.Text);
  SotrudUpd.Edit32.Text:=DMod.Q_DolzhnNAIMEN.AsString;
  Edit23.Text:=Edit33.Text;
  Edit24.Text:=Edit34.Text;
end;

procedure TSotrudUpd.DBLookupComboBox2Enter(Sender: TObject);
begin
SotrudUpd.DBLookupComboBox2.ListSource.DataSet.Last;
end;

procedure TSotrudUpd.BitBtn13Click(Sender: TObject);
var
i:integer;
begin
 for i:=1 to 24 do
     begin
     if (TEdit(GroupBox6.Controls[i]).Text<>'') and (TEdit(GroupBox13.Controls[i]).Text<>'') and (TEdit(GroupBox16.Controls[i]).Text<>'') and (ComboBox3.Text<>'') and (TMaskEdit(GroupBox13.Controls[i]).Text<>'') and (DBLookupComboBox2.KeyValue<>null)  then
      begin
      DMod.Sotrud_Upd.ParamByName('ID_DOLZH').AsInteger:=DMod.Q_DolzhnID_DOLZH.AsInteger;
      DMod.Sotrud_Upd.ParamByName('ID_OTDELA').AsInteger:=DMod.Q_OtdelID_OTDELA.AsInteger;
      DMod.Sotrud_Upd.ParamByName('FAMILY').AsString:=Edit11.Text;
      DMod.Sotrud_Upd.ParamByName('IMYA').AsString:=Edit12.Text;
      DMod.Sotrud_Upd.ParamByName('OTCHESTVO').AsString:=Edit13.Text;
      DMod.Sotrud_Upd.ParamByName('POL').AsString:=ComboBox3.Text;;
      DMod.Sotrud_Upd.ParamByName('DATA_ROZHDEN').AsString:=DateToStr(DateTimePicker1.Date);
      DMod.Sotrud_Upd.ParamByName('MESTO_ZHIT').AsString:=RichEdit3.Lines.Text;
      DMod.Sotrud_Upd.ParamByName('SER_PASPORT').AsString:=MaskEdit8.Text;
      DMod.Sotrud_Upd.ParamByName('NOM_PASPORT').AsString:=MaskEdit9.Text;
      DMod.Sotrud_Upd.ParamByName('DATA_VIDACHI_PASPORT').AsString:=DateToStr(DateTimePicker4.Date);
      DMod.Sotrud_Upd.ParamByName('KEM_VIDAN_PASPORT').AsString:=Edit22.Text;
      DMod.Sotrud_Upd.ParamByName('AGRES_REG').AsString:=RichEdit4.Lines.Text;
      DMod.Sotrud_Upd.ParamByName('DOM_TELEFON').AsString:=MaskEdit7.Text;
      DMod.Sotrud_Upd.ParamByName('RAB_TELEFON').AsString:=MaskEdit6.Text;
      DMod.Sotrud_Upd.ParamByName('ID_SOTRUD').AsInteger:=DMod.Q_SotrudID_SOTRUD.AsInteger;
      DMod.Sotrud_Upd.ExecProc;
      DMod.IBTransaction1.CommitRetaining;
      DMod.Q_Sotrud.Close;
      DMod.Q_Sotrud.Open;
      MessageBox(Self.Handle, PChar('������� ������� ��������'), PChar(''), MB_OK+MB_ICONINFORMATION);
      SotrudUpd.Close;
      DBLookupComboBox2.KeyValue:=null;
      end
    else
    MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
    Exit;
    end;
end;

end.
