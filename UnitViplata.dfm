object FormViplata: TFormViplata
  Left = 365
  Top = 165
  BorderStyle = bsDialog
  Caption = #1046#1091#1088#1085#1072#1083' '#1074#1099#1087#1083#1072#1090
  ClientHeight = 310
  ClientWidth = 637
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 345
    Height = 265
    DataSource = DMod.DS_Vibor2
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    Columns = <
      item
        Expanded = False
        FieldName = 'ID_VIPLATI'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DATA_VIPL'
        Title.Caption = #1044#1072#1090#1072' '#1074#1099#1087#1083#1072#1090#1099
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUM_VIPL'
        Title.Caption = #1057#1091#1084#1084#1072' '#1074#1099#1087#1083#1072#1090#1099
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OSTAT_VIPL'
        Title.Caption = #1054#1089#1090#1072#1090#1086#1082' '#1082' '#1074#1099#1087#1083#1072#1090#1072#1084
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ID_USL'
        Visible = False
      end>
  end
  object GroupBox1: TGroupBox
    Left = 360
    Top = 0
    Width = 273
    Height = 305
    TabOrder = 1
    object Label1: TLabel
      Left = 48
      Top = 27
      Width = 87
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1076#1086#1075#1086#1074#1086#1088#1072':'
    end
    object Label5: TLabel
      Left = 32
      Top = 58
      Width = 107
      Height = 13
      Caption = #1062#1077#1085#1072' '#1085#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1080':'
    end
    object Edit1: TEdit
      Left = 144
      Top = 24
      Width = 97
      Height = 21
      Enabled = False
      TabOrder = 0
    end
    object GroupBox2: TGroupBox
      Left = 16
      Top = 80
      Width = 241
      Height = 161
      TabOrder = 1
      object Label2: TLabel
        Left = 40
        Top = 40
        Width = 77
        Height = 13
        Caption = #1044#1072#1090#1072' '#1074#1099#1087#1083#1072#1090#1099':'
      end
      object Label3: TLabel
        Left = 32
        Top = 72
        Width = 85
        Height = 13
        Caption = #1057#1091#1084#1084#1072' '#1074#1099#1087#1083#1072#1090#1099':'
      end
      object Label4: TLabel
        Left = 8
        Top = 104
        Width = 108
        Height = 13
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1082' '#1074#1099#1087#1083#1072#1090#1072#1084':'
      end
      object Edit2: TEdit
        Left = 120
        Top = 40
        Width = 113
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit3: TEdit
        Left = 120
        Top = 72
        Width = 113
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit4: TEdit
        Left = 120
        Top = 104
        Width = 113
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
    end
    object BitBtn4: TBitBtn
      Left = 168
      Top = 264
      Width = 91
      Height = 33
      Caption = #1043#1086#1090#1086#1074#1086
      TabOrder = 2
      OnClick = BitBtn4Click
    end
    object Edit5: TEdit
      Left = 144
      Top = 56
      Width = 97
      Height = 21
      Enabled = False
      TabOrder = 3
    end
  end
  object BitBtn1: TBitBtn
    Left = 16
    Top = 272
    Width = 81
    Height = 33
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 2
    OnClick = BitBtn1Click
  end
  object BitBtn3: TBitBtn
    Left = 128
    Top = 272
    Width = 81
    Height = 33
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 3
    OnClick = BitBtn3Click
  end
end
