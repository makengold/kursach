object ChangeAgent: TChangeAgent
  Left = 299
  Top = 125
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088' '#1072#1075#1077#1085#1090#1072
  ClientHeight = 357
  ClientWidth = 755
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 753
    Height = 305
    DataSource = DMod.DS_AgentChange
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NAIMEN'
        Title.Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FAMILY'
        Title.Caption = #1060#1072#1084#1080#1083#1080#1103
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IMYA'
        Title.Caption = #1048#1084#1103
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OTCHESTVO'
        Title.Caption = #1054#1090#1095#1077#1089#1090#1074#1086
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'POL'
        Title.Caption = #1055#1086#1083
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AGRES_REG'
        Title.Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DOM_TELEFON'
        Title.Caption = #1044#1086#1084#1072#1096#1085#1080#1081' '#1090#1077#1083#1077#1092#1086#1085
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RAB_TELEFON'
        Title.Caption = #1056#1072#1073#1086#1095#1080#1081' '#1090#1077#1083#1077#1092#1086#1085
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA_ROZHDEN'
        Title.Caption = #1044#1072#1090#1072' '#1088#1086#1078#1076#1077#1085#1080#1103
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MESTO_ZHIT'
        Title.Caption = #1052#1077#1089#1090#1086' '#1078#1080#1090#1077#1083#1100#1089#1090#1074#1072
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SER_PASPORT'
        Title.Caption = #1057#1077#1088#1080#1103' '#1087#1072#1089#1087#1086#1088#1090#1072
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOM_PASPORT'
        Title.Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA_VIDACHI_PASPORT'
        Title.Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KEM_VIDAN_PASPORT'
        Title.Caption = #1052#1077#1089#1090#1086' '#1074#1099#1076#1072#1095#1080
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAIMEN1'
        Title.Caption = #1054#1090#1076#1077#1083
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RUKOVOD'
        Title.Caption = #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ID_SOTRUD'
        Width = -1
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ID_DOLZH'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ID_OTDELA'
        Visible = False
      end>
  end
  object BitBtn1: TBitBtn
    Left = 648
    Top = 320
    Width = 91
    Height = 33
    Caption = #1043#1086#1090#1086#1074#1086
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 528
    Top = 320
    Width = 91
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = BitBtn2Click
  end
end
