unit UnitSobitie;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, Grids, DBGrids;

type
  TFormSobitie = class(TForm)
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    RichEdit1: TRichEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    GroupBox2: TGroupBox;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    BitBtn5: TBitBtn;
    Label9: TLabel;
    Edit8: TEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSobitie: TFormSobitie;

implementation

uses AddSobit, DM, UnitSotrUpd, UnitSobitUpd, UnitOtchet;

{$R *.dfm}

procedure TFormSobitie.BitBtn1Click(Sender: TObject);
begin
  SobitAdd.ShowModal;
end;

procedure TFormSobitie.BitBtn3Click(Sender: TObject);
begin
    case MessageBox(Handle,'�� �������, ��� ������ ������� ����?','��������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    DMod.Sobit_Del.ParamByName('ID_SOBIT').AsInteger:=DMod.Q_ViborID_SOBIT.AsInteger;
    DMod.Sobit_Del.ExecProc;
    end;
    IDNO: Exit;
    end;
    DMod.Q_Vibor.Close;
    DMod.Q_Vibor.Open;
    MessageBox(Self.Handle, PChar('������ �������'), PChar(''), MB_OK+MB_ICONINFORMATION);
end;

procedure TFormSobitie.DBGrid1CellClick(Column: TColumn);
begin
 Edit2.Text:=DMod.Q_ViborNAIMEN.AsString;
 Edit3.Text:=DMod.Q_ViborTIP_DOCUM.AsString;
 Edit4.Text:=DMod.Q_ViborDATA_SOBIT.AsString;
 Edit1.Text:=DMod.Q_ViborOTVETSTV_LICO.AsString;
 RichEdit1.Lines.Text:=DMod.Q_ViborSODERJ_DOCUM.AsString;
 SobitUpd.Edit2.Text:=DMod.Q_ViborNAIMEN.AsString;
 SobitUpd.Edit3.Text:=DMod.Q_ViborTIP_DOCUM.AsString;
 SobitUpd.Edit4.Text:=DMod.Q_ViborDATA_SOBIT.AsString;
 SobitUpd.Edit1.Text:=DMod.Q_ViborOTVETSTV_LICO.AsString;
 SobitUpd.RichEdit2.Lines.Text:=DMod.Q_ViborSODERJ_DOCUM.AsString;
end;

procedure TFormSobitie.BitBtn4Click(Sender: TObject);
begin
 Edit2.Clear;;
 Edit3.Clear;
 Edit4.Clear;
 Edit1.Clear;
 RichEdit1.Clear;
 FormSobitie.Close;
end;

procedure TFormSobitie.BitBtn2Click(Sender: TObject);
begin
  if SobitUpd.Edit2.Text<>'' then
  SobitUpd.ShowModal
  else
  MessageBox(Self.Handle, PChar('�������� ���� ��� ���������'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TFormSobitie.BitBtn5Click(Sender: TObject);
begin
  FormOtchet.BitBtn2.Tag:=2;
  FormOtchet.ShowModal;
end;

end.
