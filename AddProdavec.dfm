object ProdavecAdd: TProdavecAdd
  Left = 420
  Top = 164
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1088#1086#1076#1072#1074#1094#1072
  ClientHeight = 303
  ClientWidth = 601
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox7: TGroupBox
    Left = 8
    Top = 1
    Width = 585
    Height = 296
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 32
      Width = 52
      Height = 13
      Caption = #1060#1072#1084#1080#1083#1080#1103':'
    end
    object Label2: TLabel
      Left = 16
      Top = 64
      Width = 25
      Height = 13
      Caption = #1048#1084#1103':'
    end
    object Label3: TLabel
      Left = 16
      Top = 96
      Width = 50
      Height = 13
      Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
    end
    object Label4: TLabel
      Left = 16
      Top = 128
      Width = 48
      Height = 13
      Caption = #1058#1077#1083#1077#1092#1086#1085':'
    end
    object Label47: TLabel
      Left = 13
      Top = 156
      Width = 84
      Height = 13
      Caption = #1057#1077#1088#1080#1103' '#1087#1072#1089#1087#1086#1088#1090#1072':'
    end
    object Label54: TLabel
      Left = 16
      Top = 188
      Width = 87
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072':'
    end
    object Label46: TLabel
      Left = 320
      Top = 20
      Width = 69
      Height = 13
      Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080':'
    end
    object Label45: TLabel
      Left = 312
      Top = 52
      Width = 75
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1074#1099#1076#1072#1095#1080':'
    end
    object Edit1: TEdit
      Left = 120
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object Edit2: TEdit
      Left = 120
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object Edit3: TEdit
      Left = 120
      Top = 88
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object MaskEdit7: TMaskEdit
      Left = 120
      Top = 120
      Width = 121
      Height = 21
      EditMask = '+7(999)999-99-99;1;_'
      MaxLength = 16
      TabOrder = 3
      Text = '+7(   )   -  -  '
    end
    object MaskEdit8: TMaskEdit
      Left = 120
      Top = 152
      Width = 119
      Height = 21
      EditMask = '9999;1;_'
      MaxLength = 4
      TabOrder = 4
      Text = '    '
    end
    object MaskEdit9: TMaskEdit
      Left = 120
      Top = 184
      Width = 120
      Height = 21
      EditMask = '999999;1;_'
      MaxLength = 6
      TabOrder = 5
      Text = '      '
    end
    object DateTimePicker4: TDateTimePicker
      Left = 416
      Top = 16
      Width = 125
      Height = 21
      Date = 42104.899212222220000000
      Time = 42104.899212222220000000
      TabOrder = 6
    end
    object Edit22: TEdit
      Left = 416
      Top = 48
      Width = 153
      Height = 21
      TabOrder = 7
    end
    object GroupBox14: TGroupBox
      Left = 288
      Top = 88
      Width = 289
      Height = 121
      Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
      TabOrder = 8
      object Label15: TLabel
        Left = 72
        Top = 24
        Width = 33
        Height = 13
        Caption = #1043#1086#1088#1086#1076':'
      end
      object Label33: TLabel
        Left = 80
        Top = 72
        Width = 26
        Height = 13
        Caption = #1044#1086#1084':'
      end
      object Label42: TLabel
        Left = 72
        Top = 48
        Width = 35
        Height = 13
        Caption = #1059#1083#1080#1094#1072':'
      end
      object Label43: TLabel
        Left = 56
        Top = 96
        Width = 51
        Height = 13
        Caption = #1050#1074#1072#1088#1090#1080#1088#1072':'
      end
      object Edit18: TEdit
        Left = 132
        Top = 20
        Width = 149
        Height = 21
        TabOrder = 0
      end
      object Edit19: TEdit
        Left = 132
        Top = 44
        Width = 149
        Height = 21
        TabOrder = 1
      end
      object Edit20: TEdit
        Left = 132
        Top = 68
        Width = 149
        Height = 21
        TabOrder = 2
      end
      object Edit21: TEdit
        Left = 132
        Top = 92
        Width = 149
        Height = 21
        TabOrder = 3
      end
    end
    object BitBtn5: TBitBtn
      Left = 304
      Top = 255
      Width = 83
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 9
      OnClick = BitBtn5Click
    end
    object BitBtn3: TBitBtn
      Left = 480
      Top = 255
      Width = 83
      Height = 33
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 10
      OnClick = BitBtn3Click
    end
  end
end
