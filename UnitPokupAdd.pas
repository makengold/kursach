unit UnitPokupAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Spin, ComCtrls, StdCtrls, Buttons, Mask;

type
  TPokupAdd = class(TForm)
    GroupBox10: TGroupBox;
    GroupBox5: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Edit30: TEdit;
    BitBtn3: TBitBtn;
    BitBtn5: TBitBtn;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Edit5: TEdit;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    MaskEdit3: TMaskEdit;
    DateTimePicker1: TDateTimePicker;
    Edit7: TEdit;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    Edit3: TEdit;
    Edit2: TEdit;
    Edit1: TEdit;
    SpinEdit1: TSpinEdit;
    GroupBox14: TGroupBox;
    Label15: TLabel;
    Label33: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Edit18: TEdit;
    Edit19: TEdit;
    Edit20: TEdit;
    Edit21: TEdit;
    MaskEdit7: TMaskEdit;
    DateTimePicker2: TDateTimePicker;
    procedure Edit7KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PokupAdd: TPokupAdd;

implementation

uses DM;

{$R *.dfm}

procedure TPokupAdd.Edit7KeyPress(Sender: TObject; var Key: Char);
begin
if not (Key in ['A'..'Z','a'..'z',#8,#32]) then
 Key:=#0;
end;

procedure TPokupAdd.BitBtn5Click(Sender: TObject);
var
i:integer;
begin
  for i:=0 to GroupBox3.ControlCount-1 do
  if GroupBox3.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox3.Controls[i]).Clear;
  Edit5.Clear;
  SpinEdit1.Clear;
  MaskEdit1.Clear;
  MaskEdit2.Clear;
  MaskEdit7.Clear;
  DateTimePicker2.Date:=now;
  Edit30.Clear;
  for i:=0 to GroupBox14.ControlCount-1 do
  if GroupBox14.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox14.Controls[i]).Clear;
  MaskEdit3.Clear;
  DateTimePicker1.Date:=now;
  Edit7.Clear;
  PokupAdd.Close;
end;

procedure TPokupAdd.BitBtn3Click(Sender: TObject);
var
i:integer;
begin
  if (Edit1.Text<>'') and (Edit2.Text<>'') and (Edit3.Text<>'') and (MaskEdit7.Text<>'') and (MaskEdit1.Text<>'') and (MaskEdit2.Text<>'') and (Edit18.Text<>'') and (Edit19.Text<>'') and (Edit20.Text<>'') and (Edit21.Text<>'') and (Edit5.Text<>'') and (MaskEdit3.Text<>'') and (Edit7.Text<>'')
  then begin
  DMod.Pokup_Add.ParamByName('FAMILY').AsString:=Edit1.Text;
  DMod.Pokup_Add.ParamByName('IMYA').AsString:=Edit2.Text;
  DMod.Pokup_Add.ParamByName('OTCHESTVO').AsString:=Edit3.Text;
  DMod.Pokup_Add.ParamByName('TEL').AsString:=MaskEdit7.Text;
  DMod.Pokup_Add.ParamByName('SER_PASPORT').AsString:=MaskEdit1.Text;
  DMod.Pokup_Add.ParamByName('NOM_PASPORT').AsString:=MaskEdit2.Text;
  DMod.Pokup_Add.ParamByName('DATA_VIDACHI_PASPORT').AsString:=DateToStr(DateTimePicker2.Date);
  DMod.Pokup_Add.ParamByName('KEM_VIDAN_PASPORT').AsString:=Edit30.Text;
  DMod.Pokup_Add.ParamByName('ADRES_REG').AsString:='�.'+Edit18.Text+', ��.'+Edit19.Text+', ��� '+Edit20.Text+', ��.'+Edit21.Text;
  DMod.Pokup_Add.ParamByName('MEST_RABOTI').AsString:=Edit5.Text;
  DMod.Pokup_Add.ParamByName('MES_DOHOD').AsString:=SpinEdit1.Text;
  DMod.Pokup_Add.ParamByName('NOM_BANK_KARTI').AsString:=MaskEdit3.Text;
  DMod.Pokup_Add.ParamByName('DATA_ISTECH_KARTI').AsString:=DateToStr(DateTimePicker1.Date);
  DMod.Pokup_Add.ParamByName('NAIMEN_VLADELCA').AsString:=Edit7.Text;
  DMod.Pokup_Add.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Pokup.Close;
  DMod.Q_Pokup.Open;
  MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
  PokupAdd.Close;
  for i:=0 to GroupBox3.ControlCount-1 do
  if GroupBox3.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox3.Controls[i]).Clear;
  Edit5.Clear;
  SpinEdit1.Clear;
  MaskEdit1.Clear;
  MaskEdit2.Clear;
  MaskEdit7.Clear;
  DateTimePicker2.Date:=now;
  Edit30.Clear;
  for i:=0 to GroupBox14.ControlCount-1 do
  if GroupBox14.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox14.Controls[i]).Clear;
  MaskEdit3.Clear;
  DateTimePicker1.Date:=now;
  Edit7.Clear;
end;

end.
