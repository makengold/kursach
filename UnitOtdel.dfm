object FormOtdel: TFormOtdel
  Left = 341
  Top = 191
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088' '#1086#1090#1076#1077#1083#1072
  ClientHeight = 245
  ClientWidth = 692
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 369
    Height = 209
    DataSource = DMod.DS_Otdel
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    OnTitleClick = DBGrid1TitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'ID_OTDELA'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'NAIMEN'
        Title.Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RUKOVOD'
        Title.Caption = #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100
        Width = 180
        Visible = True
      end>
  end
  object GroupBox1: TGroupBox
    Left = 376
    Top = 48
    Width = 305
    Height = 97
    TabOrder = 1
    object Label48: TLabel
      Left = 24
      Top = 32
      Width = 53
      Height = 13
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077':'
    end
    object Label49: TLabel
      Left = 8
      Top = 64
      Width = 74
      Height = 13
      Caption = #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100':'
    end
    object Edit23: TEdit
      Left = 96
      Top = 24
      Width = 201
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object Edit24: TEdit
      Left = 96
      Top = 56
      Width = 201
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
  end
  object BitBtn1: TBitBtn
    Left = 408
    Top = 208
    Width = 81
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 576
    Top = 208
    Width = 81
    Height = 33
    Caption = #1043#1086#1090#1086#1074#1086
    TabOrder = 3
    OnClick = BitBtn2Click
  end
  object BitBtn3: TBitBtn
    Left = 16
    Top = 216
    Width = 75
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 4
    OnClick = BitBtn3Click
  end
  object BitBtn4: TBitBtn
    Left = 224
    Top = 216
    Width = 75
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 5
    OnClick = BitBtn4Click
  end
  object BitBtn5: TBitBtn
    Left = 120
    Top = 216
    Width = 75
    Height = 25
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    TabOrder = 6
    OnClick = BitBtn5Click
  end
end
