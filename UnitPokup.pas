unit UnitPokup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, StdCtrls, Buttons, Mask;

type
  TFormPokup = class(TForm)
    GroupBox5: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    GroupBox7: TGroupBox;
    RichEdit2: TRichEdit;
    Edit30: TEdit;
    Edit31: TEdit;
    BitBtn3: TBitBtn;
    BitBtn5: TBitBtn;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Edit5: TEdit;
    Edit6: TEdit;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Edit7: TEdit;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Edit3: TEdit;
    Label3: TLabel;
    Edit2: TEdit;
    Label2: TLabel;
    Edit1: TEdit;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn4: TBitBtn;
    GroupBox10: TGroupBox;
    Edit4: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Edit10: TEdit;
    Edit11: TEdit;
    Edit12: TEdit;
    Label10: TLabel;
    ComboBox1: TComboBox;
    Label11: TLabel;
    Label12: TLabel;
    Edit13: TEdit;
    Label13: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure Edit12Change(Sender: TObject);
    procedure Edit13Change(Sender: TObject);
    procedure Edit13Click(Sender: TObject);
    procedure Edit13Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  sort:boolean;
  end;

var
  FormPokup: TFormPokup;

implementation

uses DM, UnitPokupAdd, Main, UnitSdelkaUpd, UnitPokupUpd;

{$R *.dfm}

procedure TFormPokup.BitBtn1Click(Sender: TObject);
begin
  PokupAdd.ShowModal;
end;

procedure TFormPokup.DBGrid1CellClick(Column: TColumn);
begin
 Edit1.Text:=DMod.Q_PokupFAMILY.AsString;
 Edit2.Text:=DMod.Q_PokupIMYA.AsString;
 Edit3.Text:=DMod.Q_PokupOTCHESTVO.AsString;
 Edit4.Text:=DMod.Q_PokupTEL.AsString;
 Edit10.Text:=DMod.Q_PokupSER_PASPORT.AsString;
 Edit11.Text:=DMod.Q_PokupNOM_PASPORT.AsString;
 Edit31.Text:=DMod.Q_PokupDATA_VIDACHI_PASPORT.AsString;
 Edit30.Text:=DMod.Q_PokupKEM_VIDAN_PASPORT.AsString;
 RichEdit2.Lines.Text:=DMod.Q_PokupADRES_REG.AsString;
 Edit5.Text:=DMod.Q_PokupMEST_RABOTI.AsString;
 Edit6.Text:=DMod.Q_PokupMES_DOHOD.AsString;
 Edit9.Text:=DMod.Q_PokupNOM_BANK_KARTI.AsString;
 Edit8.Text:=DMod.Q_PokupDATA_ISTECH_KARTI.AsString;
 Edit7.Text:=DMod.Q_PokupNAIMEN_VLADELCA.AsString;
 with PokupUpd do
 begin
  Edit1.Text:=DMod.Q_PokupFAMILY.AsString;
 Edit2.Text:=DMod.Q_PokupIMYA.AsString;
 Edit3.Text:=DMod.Q_PokupOTCHESTVO.AsString;
 Edit4.Text:=DMod.Q_PokupTEL.AsString;
 Edit10.Text:=DMod.Q_PokupSER_PASPORT.AsString;
 Edit11.Text:=DMod.Q_PokupNOM_PASPORT.AsString;
 Edit31.Text:=DMod.Q_PokupDATA_VIDACHI_PASPORT.AsString;
 Edit30.Text:=DMod.Q_PokupKEM_VIDAN_PASPORT.AsString;
 RichEdit2.Lines.Text:=DMod.Q_PokupADRES_REG.AsString;
 Edit5.Text:=DMod.Q_PokupMEST_RABOTI.AsString;
 Edit6.Text:=DMod.Q_PokupMES_DOHOD.AsString;
 Edit9.Text:=DMod.Q_PokupNOM_BANK_KARTI.AsString;
 Edit8.Text:=DMod.Q_PokupDATA_ISTECH_KARTI.AsString;
 Edit7.Text:=DMod.Q_PokupNAIMEN_VLADELCA.AsString;
 end;
end;

procedure TFormPokup.BitBtn4Click(Sender: TObject);
var
i:integer;
begin
    case MessageBox(Handle,'�� �������, ��� ������ ������� ����?','��������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    DMod.Pokup_Del.ParamByName('ID_POKUP').AsInteger:=DMod.Q_PokupID_POKUP.AsInteger;
    try
    DMod.Pokup_Del.ExecProc;
    MessageBox(Self.Handle, PChar('������ �������'), PChar('������'), MB_OK+MB_ICONINFORMATION);
    except
    MessageBox(Self.Handle, PChar('�������� ����������, ��� ��� ������ ������������ � ������ �������'), PChar(''), MB_OK+MB_ICONWARNING);
    end;
    end;
    IDNO: Exit;
    end;
    DMod.Q_Pokup.Close;
    DMod.Q_Pokup.Open;
    for i:=0 to GroupBox3.ControlCount-1 do
    if GroupBox3.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox3.Controls[i]).Clear;
    for i:=0 to GroupBox5.ControlCount-1 do
    if GroupBox5.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox5.Controls[i]).Clear;
    for i:=0 to GroupBox1.ControlCount-1 do
    if GroupBox1.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox1.Controls[i]).Clear;
    for i:=0 to GroupBox2.ControlCount-1 do
    if GroupBox2.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox2.Controls[i]).Clear;
    RichEdit2.Lines.Clear;
end;

procedure TFormPokup.BitBtn5Click(Sender: TObject);
var
i:integer;
begin
  for i:=0 to GroupBox3.ControlCount-1 do
  if GroupBox3.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox3.Controls[i]).Clear;
  for i:=0 to GroupBox5.ControlCount-1 do
  if GroupBox5.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox5.Controls[i]).Clear;
  for i:=0 to GroupBox1.ControlCount-1 do
  if GroupBox1.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox1.Controls[i]).Clear;
  for i:=0 to GroupBox2.ControlCount-1 do
  if GroupBox2.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox2.Controls[i]).Clear;
  RichEdit2.Lines.Clear;
  FormPokup.Close;
end;

procedure TFormPokup.BitBtn3Click(Sender: TObject);
begin
  if BitBtn3.Tag=0 then
  begin
  if Edit1.Text<>'' then begin
  MainForm.Edit31.Text:=Edit1.Text;
  MainForm.Edit32.Text:=Edit2.Text;
  MainForm.Edit33.Text:=Edit3.Text;
  MainForm.Edit34.Text:=Edit4.Text;
  MainForm.Edit28.Text:=Edit9.Text;
  FormPokup.Close;
  end
  else
  ShowMessage('�������� ����');
  end
  else
  begin
  if Edit1.Text<>'' then begin
  SdelkaUpd.Edit11.Text:=Edit1.Text;
  SdelkaUpd.Edit12.Text:=Edit2.Text;
  SdelkaUpd.Edit13.Text:=Edit3.Text;
  SdelkaUpd.Edit14.Text:=Edit4.Text;
  SdelkaUpd.Edit15.Text:=Edit9.Text;
  FormPokup.Close;
  end
  else
  ShowMessage('�������� ����');
  end;
end;

procedure TFormPokup.BitBtn2Click(Sender: TObject);
begin
  if PokupUpd.Edit1.Text<>'' then
  PokupUpd.ShowModal
  else
  MessageBox(Self.Handle, PChar('�������� ���� ��� ���������'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TFormPokup.DBGrid1TitleClick(Column: TColumn);
begin
  if sort=true then
  begin
  DMod.Q_Pokup.Close;
  DMod.Q_Pokup.SQL.Clear;
  DMod.Q_Pokup.SQL.Text:='Select * FROM pokupatel order by FAMILY ASC ';
  DMod.Q_Pokup.Close;
  DMod.Q_Pokup.Open;
  sort:=false;
  end
  else
  begin
  DMod.Q_Pokup.Close;
  DMod.Q_Pokup.SQL.Clear;
  DMod.Q_Pokup.SQL.Text:='Select * FROM pokupatel order by FAMILY DESC';
  DMod.Q_Pokup.Close;
  DMod.Q_Pokup.Open;
  sort:=true;
  end;
end;

procedure TFormPokup.Edit12Change(Sender: TObject);
begin
  DMod.Q_Pokup.Close;
  DMod.Q_Pokup.SQL.Clear;
  if Edit12.Text<>'' then
  begin
  case ComboBox1.ItemIndex of
  0: DMod.Q_Pokup.SQL.Add('select * from POKUPATEL where FAMILY like '+#39'%'+Edit12.Text+'%'+#39);
  1: DMod.Q_Pokup.SQL.Add('select * from POKUPATEL where IMYA like '+#39'%'+Edit12.Text+'%'+#39);
  2: DMod.Q_Pokup.SQL.Add('select * from POKUPATEL where OTCHESTVO like '+#39'%'+Edit12.Text+'%'+#39);
  3: DMod.Q_Pokup.SQL.Add('select * from POKUPATEL where NOM_PASPORT like '+#39'%'+Edit12.Text+'%'+#39);
  end;
  end
  else
  begin
  DMod.Q_Pokup.SQL.Add('select * from POKUPATEL');
  end;
  DMod.Q_Pokup.Open;
end;

procedure TFormPokup.Edit13Change(Sender: TObject);
begin
    DMod.Q_Pokup.Locate('FAMILY',Edit13.Text,[]);
    FormPokup.DBGrid1.Options:=[dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect,dgAlwaysShowSelection,dgConfirmDelete,dgCancelOnExit];
end;

procedure TFormPokup.Edit13Click(Sender: TObject);
begin
  Edit13.Text:='';
end;

procedure TFormPokup.Edit13Exit(Sender: TObject);
begin
  FormPokup.DBGrid1.Options:=[dgTitles,dgIndicator,dgColLines,dgRowLines,dgTabs,dgConfirmDelete,dgCancelOnExit];
end;

end.
