unit Adres;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, Buttons, Mask, DBCtrls, ComCtrls,
  sPageControl;

type
  TFormAdres = class(TForm)
    DBGrid1: TDBGrid;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    GroupBox1: TGroupBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    MaskEdit1: TMaskEdit;
    CheckBox1: TCheckBox;
    Label5: TLabel;
    Edit4: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    ComboBox1: TComboBox;
    Edit5: TEdit;
    Label8: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure Edit4Change(Sender: TObject);
    procedure Edit5Click(Sender: TObject);
    procedure Edit5Change(Sender: TObject);
    procedure Edit5Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  sort:boolean;
  end;

var
  FormAdres: TFormAdres;

implementation

uses AddStreet, DM, Remont, Main, UnitAdresUpd, NedvizhUpd;

{$R *.dfm}

procedure TFormAdres.BitBtn1Click(Sender: TObject);
begin
  Edit1.Clear;
  Edit2.Clear;
  Edit3.Clear;
  MaskEdit1.Text:='';
  FormAdres.Close;
end;

procedure TFormAdres.BitBtn6Click(Sender: TObject);
begin
  AdresAdd.Timer1.Enabled:=True;
  AdresAdd.ShowModal;
end;

procedure TFormAdres.BitBtn2Click(Sender: TObject);
begin
  if CheckBox1.Checked=False then
  begin
  MainForm.Edit5.Text:=Edit1.Text;
  MainForm.Edit6.Text:=Edit2.Text;
  MainForm.Edit7.Text:=Edit3.Text;
  MainForm.Edit8.Text:=MaskEdit1.Text;
  Edit1.Clear;
  Edit2.Clear;
  Edit3.Clear;
  MaskEdit1.Text:='';
  end
  else
  begin
  NedvizhEdit.Edit19.Text:=Edit1.Text;
  NedvizhEdit.Edit20.Text:=Edit2.Text;
  NedvizhEdit.Edit21.Text:=Edit3.Text;
  NedvizhEdit.Edit22.Text:=MaskEdit1.Text;
  Edit1.Clear;
  Edit2.Clear;
  Edit3.Clear;
  MaskEdit1.Text:='';
  end;
  FormAdres.Close;
end;

procedure TFormAdres.DBGrid1CellClick(Column: TColumn);
begin
 Edit1.Text:=DMod.Q_AdresGOROD.AsString;
 Edit2.Text:=DMod.Q_AdresRAYON.AsString;
 Edit3.Text:=DMod.Q_AdresULITCA.AsString;
 MaskEdit1.Text:=DMod.Q_AdresDOM.AsString;
end;

procedure TFormAdres.BitBtn5Click(Sender: TObject);
begin
    case MessageBox(Handle,'�� �������, ��� ������ ������� ����?','��������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    DMod.Adres_Del.ParamByName('ID_ADRESA').AsInteger:=DMod.Q_AdresID_ADRESA.AsInteger;
    try
    DMod.Adres_Del.ExecProc;
    MessageBox(Self.Handle, PChar('������ �������'), PChar('������'), MB_OK+MB_ICONINFORMATION);
    except
    MessageBox(Self.Handle, PChar('�������� ����������, ��� ��� ������ ������������ � ������ �������'), PChar(''), MB_OK+MB_ICONWARNING);
    end;
    end;
    IDNO: Exit;
    end;
    DMod.Q_Adres.Close;
    DMod.Q_Adres.Open;
end;

procedure TFormAdres.BitBtn4Click(Sender: TObject);
begin
   if Edit1.Text<>'' then
   begin
   AdresUpd.ComboBox4.Text:=Edit1.Text;
   AdresUpd.ComboBox3.Text:=Edit2.Text;
   AdresUpd.Edit1.Text:=Edit3.Text;
   AdresUpd.MaskEdit1.Text:=MaskEdit1.Text;
   AdresUpd.Timer1.Enabled:=True;
   AdresUpd.ShowModal;
   end
   else
   MessageBox(Self.Handle, PChar('�������� ���� ��� ���������'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TFormAdres.DBGrid1TitleClick(Column: TColumn);
begin
  if sort=true then
  begin
  DMod.Q_Adres.Close;
  DMod.Q_Adres.SQL.Clear;
  DMod.Q_Adres.SQL.Text:='Select * FROM Adres order by Gorod ASC';
  DMod.Q_Adres.Close;
  DMod.Q_Adres.Open;
  sort:=false;
  end
  else
  begin
  DMod.Q_Adres.Close;
  DMod.Q_Adres.SQL.Clear;
  DMod.Q_Adres.SQL.Text:='Select * FROM Adres order by Gorod DESC';
  DMod.Q_Adres.Close;
  DMod.Q_Adres.Open;
  sort:=true;
  end;
end;

procedure TFormAdres.Edit4Change(Sender: TObject);
begin
  DMod.Q_Adres.Close;
  DMod.Q_Adres.SQL.Clear;
  if Edit4.Text<>'' then
  begin
  case ComboBox1.ItemIndex of
  0: DMod.Q_Adres.SQL.Add('select * from Adres where GOROD like '+#39'%'+Edit4.Text+'%'+#39);
  1: DMod.Q_Adres.SQL.Add('select * from Adres where RAYON like '+#39'%'+Edit4.Text+'%'+#39);
  2: DMod.Q_Adres.SQL.Add('select * from Adres where ULITCA like '+#39'%'+Edit4.Text+'%'+#39);
  3: DMod.Q_Adres.SQL.Add('select * from Adres where DOM like '+#39'%'+Edit4.Text+'%'+#39);
  end;
  end
  else
  begin
  DMod.Q_Adres.SQL.Add('select * from ADRES');
  end;
  DMod.Q_Adres.Open;
end;

procedure TFormAdres.Edit5Click(Sender: TObject);
begin
  Edit1.Text:='';
end;

procedure TFormAdres.Edit5Change(Sender: TObject);
begin
    DMod.Q_Adres.Locate('GOROD',Edit5.Text,[]);
    FormAdres.DBGrid1.Options:=[dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect,dgAlwaysShowSelection,dgConfirmDelete,dgCancelOnExit];
end;

procedure TFormAdres.Edit5Exit(Sender: TObject);
begin
  FormAdres.DBGrid1.Options:=[dgTitles,dgIndicator,dgColLines,dgRowLines,dgTabs,dgConfirmDelete,dgCancelOnExit];
end;

end.
