object DMod: TDMod
  OldCreateOrder = False
  Left = 339
  Top = 157
  Height = 351
  Width = 791
  object IBDatabase1: TIBDatabase
    Connected = True
    DatabaseName = '127.0.0.1:C:\MYBD.FDB'
    Params.Strings = (
      'user_name=SYSDBA'
      'password=masterkey'
      'lc_ctype=WIN1251')
    LoginPrompt = False
    IdleTimer = 0
    SQLDialect = 3
    TraceFlags = []
    Left = 24
    Top = 16
  end
  object IBTransaction1: TIBTransaction
    Active = True
    DefaultDatabase = IBDatabase1
    AutoStopAction = saNone
    Left = 96
    Top = 16
  end
  object DS_Nedvizh: TDataSource
    Left = 184
    Top = 24
  end
  object DS_Remont: TDataSource
    Left = 320
    Top = 24
  end
  object DS_Vid_Nedvizh: TDataSource
    Left = 400
    Top = 24
  end
  object DS_Sotrud: TDataSource
    Left = 472
    Top = 24
  end
  object DS_Otdel: TDataSource
    Left = 536
    Top = 24
  end
  object DS_Dolzhn: TDataSource
    Left = 600
    Top = 24
  end
  object DS_Adres: TDataSource
    Left = 256
    Top = 24
  end
  object DS_Sobit: TDataSource
    Left = 664
    Top = 24
  end
  object Q_Nedvizh: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    vid_nedvizhimosti.naimen,'
      '    vid_remonta.naimen,'
      '    vid_remonta.data,'
      '    adres.gorod,'
      '    adres.rayon,'
      '    adres.ulitca,'
      '    adres.dom,'
      '    nedvizhimost.id_nedvizh,'
      '    nedvizhimost.id_vid_nedvizh,'
      '    nedvizhimost.obsh_ploshad,'
      '    nedvizhimost.id_adresa,'
      '    nedvizhimost.cena,'
      '    nedvizhimost.ploshad_adm_pomeshen,'
      '    nedvizhimost.etazh,'
      '    nedvizhimost.etazhnost,'
      '    nedvizhimost.parking,'
      '    nedvizhimost.srok_expluatac,'
      '    nedvizhimost.id_remont,'
      '    nedvizhimost.nom_kvartiri,'
      '    nedvizhimost.pocht_index'
      'from nedvizhimost'
      
        '   inner join adres on (nedvizhimost.id_adresa = adres.id_adresa' +
        ')'
      
        '   inner join vid_remonta on (nedvizhimost.id_remont = vid_remon' +
        'ta.id_remont)'
      
        '   inner join vid_nedvizhimosti on (nedvizhimost.id_vid_nedvizh ' +
        '= vid_nedvizhimosti.id_vid_nedvizh)')
    Left = 184
    Top = 88
    object Q_NedvizhNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'VID_NEDVIZHIMOSTI.NAIMEN'
      Size = 15
    end
    object Q_NedvizhNAIMEN1: TIBStringField
      FieldName = 'NAIMEN1'
      Origin = 'VID_REMONTA.NAIMEN'
      Size = 40
    end
    object Q_NedvizhDATA: TIBStringField
      FieldName = 'DATA'
      Origin = 'VID_REMONTA.DATA'
      Size = 15
    end
    object Q_NedvizhGOROD: TIBStringField
      FieldName = 'GOROD'
      Origin = 'ADRES.GOROD'
      Size = 40
    end
    object Q_NedvizhRAYON: TIBStringField
      FieldName = 'RAYON'
      Origin = 'ADRES.RAYON'
      Size = 35
    end
    object Q_NedvizhULITCA: TIBStringField
      FieldName = 'ULITCA'
      Origin = 'ADRES.ULITCA'
      Size = 50
    end
    object Q_NedvizhDOM: TIBStringField
      FieldName = 'DOM'
      Origin = 'ADRES.DOM'
      Size = 10
    end
    object Q_NedvizhID_NEDVIZH: TIntegerField
      FieldName = 'ID_NEDVIZH'
      Origin = 'NEDVIZHIMOST.ID_NEDVIZH'
      Required = True
    end
    object Q_NedvizhID_VID_NEDVIZH: TIntegerField
      FieldName = 'ID_VID_NEDVIZH'
      Origin = 'NEDVIZHIMOST.ID_VID_NEDVIZH'
    end
    object Q_NedvizhOBSH_PLOSHAD: TIBStringField
      FieldName = 'OBSH_PLOSHAD'
      Origin = 'NEDVIZHIMOST.OBSH_PLOSHAD'
      Size = 10
    end
    object Q_NedvizhID_ADRESA: TIntegerField
      FieldName = 'ID_ADRESA'
      Origin = 'NEDVIZHIMOST.ID_ADRESA'
    end
    object Q_NedvizhCENA: TIntegerField
      FieldName = 'CENA'
      Origin = 'NEDVIZHIMOST.CENA'
    end
    object Q_NedvizhPLOSHAD_ADM_POMESHEN: TIBStringField
      FieldName = 'PLOSHAD_ADM_POMESHEN'
      Origin = 'NEDVIZHIMOST.PLOSHAD_ADM_POMESHEN'
      Size = 10
    end
    object Q_NedvizhETAZH: TIntegerField
      FieldName = 'ETAZH'
      Origin = 'NEDVIZHIMOST.ETAZH'
    end
    object Q_NedvizhETAZHNOST: TIntegerField
      FieldName = 'ETAZHNOST'
      Origin = 'NEDVIZHIMOST.ETAZHNOST'
    end
    object Q_NedvizhPARKING: TIBStringField
      FieldName = 'PARKING'
      Origin = 'NEDVIZHIMOST.PARKING'
      FixedChar = True
      Size = 5
    end
    object Q_NedvizhSROK_EXPLUATAC: TIntegerField
      FieldName = 'SROK_EXPLUATAC'
      Origin = 'NEDVIZHIMOST.SROK_EXPLUATAC'
    end
    object Q_NedvizhID_REMONT: TIntegerField
      FieldName = 'ID_REMONT'
      Origin = 'NEDVIZHIMOST.ID_REMONT'
    end
    object Q_NedvizhNOM_KVARTIRI: TIntegerField
      FieldName = 'NOM_KVARTIRI'
      Origin = 'NEDVIZHIMOST.NOM_KVARTIRI'
    end
    object Q_NedvizhPOCHT_INDEX: TIBStringField
      FieldName = 'POCHT_INDEX'
      Origin = 'NEDVIZHIMOST.POCHT_INDEX'
      Size = 6
    end
  end
  object Q_Adres: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from ADRES')
    Left = 256
    Top = 88
    object Q_AdresID_ADRESA: TIntegerField
      FieldName = 'ID_ADRESA'
      Origin = 'ADRES.ID_ADRESA'
      Required = True
    end
    object Q_AdresGOROD: TIBStringField
      FieldName = 'GOROD'
      Origin = 'ADRES.GOROD'
      Size = 40
    end
    object Q_AdresRAYON: TIBStringField
      FieldName = 'RAYON'
      Origin = 'ADRES.RAYON'
      Size = 35
    end
    object Q_AdresULITCA: TIBStringField
      FieldName = 'ULITCA'
      Origin = 'ADRES.ULITCA'
      Size = 50
    end
    object Q_AdresDOM: TIBStringField
      FieldName = 'DOM'
      Origin = 'ADRES.DOM'
      Size = 10
    end
  end
  object Q_Remont: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from VID_REMONTA')
    Left = 320
    Top = 88
    object Q_RemontID_REMONT: TIntegerField
      FieldName = 'ID_REMONT'
      Origin = 'VID_REMONTA.ID_REMONT'
      Required = True
    end
    object Q_RemontNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'VID_REMONTA.NAIMEN'
      Size = 40
    end
    object Q_RemontDATA: TIBStringField
      FieldName = 'DATA'
      Origin = 'VID_REMONTA.DATA'
      Size = 15
    end
  end
  object Q_Vid_Nedvizh: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from VID_NEDVIZHIMOSTI')
    Left = 400
    Top = 88
    object Q_Vid_NedvizhID_VID_NEDVIZH: TIntegerField
      FieldName = 'ID_VID_NEDVIZH'
      Origin = 'VID_NEDVIZHIMOSTI.ID_VID_NEDVIZH'
      Required = True
    end
    object Q_Vid_NedvizhNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'VID_NEDVIZHIMOSTI.NAIMEN'
      Size = 15
    end
  end
  object Q_Sotrud: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    dolzhnost.naimen,'
      '    otdel.naimen,'
      '    otdel.rukovod,'
      '    sotrudnik.id_sotrud,'
      '    sotrudnik.family,'
      '    sotrudnik.imya,'
      '    sotrudnik.otchestvo,'
      '    sotrudnik.pol,'
      '    sotrudnik.data_rozhden,'
      '    sotrudnik.mesto_zhit,'
      '    sotrudnik.id_dolzh,'
      '    sotrudnik.ser_pasport,'
      '    sotrudnik.nom_pasport,'
      '    sotrudnik.data_vidachi_pasport,'
      '    sotrudnik.kem_vidan_pasport,'
      '    sotrudnik.agres_reg,'
      '    sotrudnik.id_otdela,'
      '    sotrudnik.dom_telefon,'
      '    sotrudnik.rab_telefon'
      'from sotrudnik'
      '   inner join otdel on (sotrudnik.id_otdela = otdel.id_otdela)'
      
        '   inner join dolzhnost on (sotrudnik.id_dolzh = dolzhnost.id_do' +
        'lzh)')
    Left = 472
    Top = 88
    object Q_SotrudNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'DOLZHNOST.NAIMEN'
      Size = 50
    end
    object Q_SotrudNAIMEN1: TIBStringField
      FieldName = 'NAIMEN1'
      Origin = 'OTDEL.NAIMEN'
      Size = 35
    end
    object Q_SotrudRUKOVOD: TIBStringField
      FieldName = 'RUKOVOD'
      Origin = 'OTDEL.RUKOVOD'
      Size = 90
    end
    object Q_SotrudID_SOTRUD: TIntegerField
      FieldName = 'ID_SOTRUD'
      Origin = 'SOTRUDNIK.ID_SOTRUD'
      Required = True
    end
    object Q_SotrudFAMILY: TIBStringField
      FieldName = 'FAMILY'
      Origin = 'SOTRUDNIK.FAMILY'
      Size = 35
    end
    object Q_SotrudIMYA: TIBStringField
      FieldName = 'IMYA'
      Origin = 'SOTRUDNIK.IMYA'
      Size = 25
    end
    object Q_SotrudOTCHESTVO: TIBStringField
      FieldName = 'OTCHESTVO'
      Origin = 'SOTRUDNIK.OTCHESTVO'
      Size = 30
    end
    object Q_SotrudPOL: TIBStringField
      FieldName = 'POL'
      Origin = 'SOTRUDNIK.POL'
      Size = 5
    end
    object Q_SotrudDATA_ROZHDEN: TIBStringField
      FieldName = 'DATA_ROZHDEN'
      Origin = 'SOTRUDNIK.DATA_ROZHDEN'
      Size = 15
    end
    object Q_SotrudMESTO_ZHIT: TIBStringField
      FieldName = 'MESTO_ZHIT'
      Origin = 'SOTRUDNIK.MESTO_ZHIT'
      Size = 120
    end
    object Q_SotrudID_DOLZH: TIntegerField
      FieldName = 'ID_DOLZH'
      Origin = 'SOTRUDNIK.ID_DOLZH'
    end
    object Q_SotrudSER_PASPORT: TIBStringField
      FieldName = 'SER_PASPORT'
      Origin = 'SOTRUDNIK.SER_PASPORT'
      Size = 5
    end
    object Q_SotrudNOM_PASPORT: TIBStringField
      FieldName = 'NOM_PASPORT'
      Origin = 'SOTRUDNIK.NOM_PASPORT'
      Size = 7
    end
    object Q_SotrudDATA_VIDACHI_PASPORT: TIBStringField
      FieldName = 'DATA_VIDACHI_PASPORT'
      Origin = 'SOTRUDNIK.DATA_VIDACHI_PASPORT'
      Size = 15
    end
    object Q_SotrudKEM_VIDAN_PASPORT: TIBStringField
      FieldName = 'KEM_VIDAN_PASPORT'
      Origin = 'SOTRUDNIK.KEM_VIDAN_PASPORT'
      Size = 80
    end
    object Q_SotrudAGRES_REG: TIBStringField
      FieldName = 'AGRES_REG'
      Origin = 'SOTRUDNIK.AGRES_REG'
      Size = 120
    end
    object Q_SotrudID_OTDELA: TIntegerField
      FieldName = 'ID_OTDELA'
      Origin = 'SOTRUDNIK.ID_OTDELA'
    end
    object Q_SotrudDOM_TELEFON: TIBStringField
      FieldName = 'DOM_TELEFON'
      Origin = 'SOTRUDNIK.DOM_TELEFON'
    end
    object Q_SotrudRAB_TELEFON: TIBStringField
      FieldName = 'RAB_TELEFON'
      Origin = 'SOTRUDNIK.RAB_TELEFON'
    end
  end
  object Q_Otdel: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from OTDEL')
    Left = 536
    Top = 88
    object Q_OtdelID_OTDELA: TIntegerField
      FieldName = 'ID_OTDELA'
      Origin = 'OTDEL.ID_OTDELA'
      Required = True
    end
    object Q_OtdelNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'OTDEL.NAIMEN'
      Size = 35
    end
    object Q_OtdelRUKOVOD: TIBStringField
      FieldName = 'RUKOVOD'
      Origin = 'OTDEL.RUKOVOD'
      Size = 90
    end
  end
  object Q_Dolzhn: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'Select * from DOLZHNOST')
    Left = 600
    Top = 88
    object Q_DolzhnID_DOLZH: TIntegerField
      FieldName = 'ID_DOLZH'
      Origin = 'DOLZHNOST.ID_DOLZH'
      Required = True
    end
    object Q_DolzhnNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'DOLZHNOST.NAIMEN'
      Size = 50
    end
  end
  object Q_Sobit: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select '
      '    sobitie.id_sobit,'
      '    sobitie.naimen,'
      '    sobitie.tip_docum,'
      '    sobitie.soderj_docum,'
      '    sobitie.data_sobit,'
      '    sobitie.otvetstv_lico,'
      '    sobitie.id_sotrud,'
      '    sotrudnik.family,'
      '    sotrudnik.imya,'
      '    sotrudnik.otchestvo'
      'from sotrudnik'
      
        '   inner join sobitie on (sotrudnik.id_sotrud = sobitie.id_sotru' +
        'd)')
    Left = 664
    Top = 88
    object Q_SobitID_SOBIT: TIntegerField
      FieldName = 'ID_SOBIT'
      Origin = 'SOBITIE.ID_SOBIT'
      Required = True
    end
    object Q_SobitNAIMEN: TIBStringField
      FieldName = 'NAIMEN'
      Origin = 'SOBITIE.NAIMEN'
      Size = 40
    end
    object Q_SobitTIP_DOCUM: TIBStringField
      FieldName = 'TIP_DOCUM'
      Origin = 'SOBITIE.TIP_DOCUM'
      Size = 15
    end
    object Q_SobitSODERJ_DOCUM: TIBStringField
      FieldName = 'SODERJ_DOCUM'
      Origin = 'SOBITIE.SODERJ_DOCUM'
      Size = 180
    end
    object Q_SobitDATA_SOBIT: TIBStringField
      FieldName = 'DATA_SOBIT'
      Origin = 'SOBITIE.DATA_SOBIT'
      Size = 15
    end
    object Q_SobitOTVETSTV_LICO: TIBStringField
      FieldName = 'OTVETSTV_LICO'
      Origin = 'SOBITIE.OTVETSTV_LICO'
      Size = 90
    end
    object Q_SobitID_SOTRUD: TIntegerField
      FieldName = 'ID_SOTRUD'
      Origin = 'SOBITIE.ID_SOTRUD'
    end
    object Q_SobitFAMILY: TIBStringField
      FieldName = 'FAMILY'
      Origin = 'SOTRUDNIK.FAMILY'
      Size = 35
    end
    object Q_SobitIMYA: TIBStringField
      FieldName = 'IMYA'
      Origin = 'SOTRUDNIK.IMYA'
      Size = 25
    end
    object Q_SobitOTCHESTVO: TIBStringField
      FieldName = 'OTCHESTVO'
      Origin = 'SOTRUDNIK.OTCHESTVO'
      Size = 30
    end
  end
  object Nedvizh_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'NEDVIZH_ADD'
    Left = 184
    Top = 144
  end
  object Nedvizh_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'NEDVIZH_UPD'
    Left = 184
    Top = 200
  end
  object Nedvizh_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'NEDVIZH_DEL'
    Left = 184
    Top = 256
  end
  object Adres_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'ADRES_ADD'
    Left = 256
    Top = 144
  end
  object Adres_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'ADRES_UPD'
    Left = 256
    Top = 200
  end
  object Adres_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'NEDVIZH_DEL'
    Left = 256
    Top = 256
  end
  object Remont_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'REMONT_ADD'
    Left = 320
    Top = 144
  end
  object Remont_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'REMONT_DEL'
    Left = 320
    Top = 200
  end
  object Sotrud_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SOTRUD_ADD'
    Left = 472
    Top = 144
  end
  object Sotrud_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'NEW_PROCEDURE'
    Left = 472
    Top = 200
  end
  object Sotrud_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SOTRUD_DEL'
    Left = 472
    Top = 256
  end
  object Otdel_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'OTDEL_ADD'
    Left = 536
    Top = 144
  end
  object Otdel_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'OTDEL_UPD'
    Left = 536
    Top = 200
  end
  object Otdel_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'OTDEL_DEL'
    Left = 536
    Top = 256
  end
  object Sobit_Add: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'SOBIT_ADD'
    Left = 664
    Top = 144
  end
  object Sobit_Upd: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    Left = 664
    Top = 200
  end
  object Sobit_Del: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    Left = 664
    Top = 256
  end
end
