unit NedvizhUpd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, Mask, Buttons, DBCtrls;

type
  TNedvizhEdit = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit11: TEdit;
    GroupBox5: TGroupBox;
    Label6: TLabel;
    Label11: TLabel;
    Label16: TLabel;
    Edit9: TEdit;
    Edit10: TEdit;
    Edit1: TEdit;
    GroupBox3: TGroupBox;
    Label7: TLabel;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    GroupBox6: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label33: TLabel;
    MaskEdit8: TMaskEdit;
    GroupBox11: TGroupBox;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    MaskEdit9: TMaskEdit;
    MaskEdit10: TMaskEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit12: TEdit;
    Edit13: TEdit;
    Edit14: TEdit;
    Edit15: TEdit;
    Edit16: TEdit;
    Edit17: TEdit;
    Edit18: TEdit;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    Label5: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Edit19: TEdit;
    Edit20: TEdit;
    Edit21: TEdit;
    Edit22: TEdit;
    BitBtn10: TBitBtn;
    GroupBox9: TGroupBox;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    ComboBox2: TComboBox;
    Edit23: TEdit;
    Edit24: TEdit;
    BitBtn4: TBitBtn;
    GroupBox10: TGroupBox;
    Label25: TLabel;
    BitBtn8: TBitBtn;
    GroupBox12: TGroupBox;
    Label30: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit4: TMaskEdit;
    SpinEdit7: TSpinEdit;
    SpinEdit8: TSpinEdit;
    GroupBox13: TGroupBox;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label31: TLabel;
    MaskEdit3: TMaskEdit;
    ComboBox9: TComboBox;
    SpinEdit4: TSpinEdit;
    SpinEdit5: TSpinEdit;
    SpinEdit6: TSpinEdit;
    GroupBox14: TGroupBox;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    MaskEdit5: TMaskEdit;
    MaskEdit1: TMaskEdit;
    ComboBox8: TComboBox;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    SpinEdit3: TSpinEdit;
    BitBtn9: TBitBtn;
    DBLookupComboBox1: TDBLookupComboBox;
    procedure FormShow(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure DBLookupComboBox1CloseUp(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NedvizhEdit: TNedvizhEdit;

implementation

uses DM, Adres, Remont;

{$R *.dfm}

procedure TNedvizhEdit.FormShow(Sender: TObject);
begin if Edit5.Text='��������' then
begin
 GroupBox11.Visible:=True;
 GroupBox6.Visible:=False;
 GroupBox4.Visible:=False;
 DBLookupComboBox1.KeyValue:=1;
 GroupBox12.Visible:=False;
 GroupBox13.Visible:=False;
 GroupBox14.Visible:=True;
 //
 MaskEdit1.Text:=MaskEdit10.Text;
 SpinEdit1.Value:=StrToInt(Edit14.Text);
 ComboBox8.Text:=Edit6.Text;
 SpinEdit2.Value:=StrToInt(Edit15.Text);
 MaskEdit5.Text:=MaskEdit9.Text;
 SpinEdit3.Value:=StrToInt(Edit16.Text);
end;
 if Edit5.Text='����' then
begin
 GroupBox6.Visible:=True;
 GroupBox11.Visible:=False;
 GroupBox4.Visible:=False;
 DBLookupComboBox1.KeyValue:=3;
 GroupBox12.Visible:=False;
 GroupBox13.Visible:=True;
 GroupBox14.Visible:=False;
 //
 MaskEdit3.Text:=MaskEdit8.Text;
 SpinEdit4.Value:=StrToInt(Edit8.Text);
 ComboBox9.Text:=Edit7.Text;
 SpinEdit5.Value:=StrToInt(Edit12.Text);
 SpinEdit6.Value:=StrToInt(Edit13.Text);
end;
 if Edit5.Text='�����' then
begin
 GroupBox4.Visible:=True;
 GroupBox6.Visible:=False;
 GroupBox11.Visible:=False;
 DBLookupComboBox1.KeyValue:=2;
 GroupBox12.Visible:=True;
 GroupBox13.Visible:=False;
 GroupBox14.Visible:=False;
 //
 MaskEdit2.Text:=MaskEdit6.Text;
 MaskEdit4.Text:=MaskEdit7.Text;
 SpinEdit7.Value:=StrToInt(Edit17.Text);
 SpinEdit8.Value:=StrToInt(Edit18.Text);
end;
  Edit19.Text:=Edit2.Text;
  Edit20.Text:=Edit3.Text;
  Edit21.Text:=Edit4.Text;
  Edit22.Text:=Edit11.Text;
  if Edit1.Text='�����������' then
  begin
  ComboBox2.ItemIndex:=0;
  BitBtn4.Visible:=False;
  Edit23.Visible:=False;
  Edit24.Visible:=False;
  Label22.Visible:=False;
  Label23.Visible:=False;
  end
  else
  begin
  ComboBox2.ItemIndex:=1;
  Edit23.Text:='';
  Edit24.Text:='';
  BitBtn4.Visible:=True;
  Edit23.Visible:=True;
  Edit24.Visible:=True;
  Label22.Visible:=True;
  Label23.Visible:=True;
  Edit23.Text:=Edit9.Text;
  Edit24.Text:=Edit10.Text;
  end;
end;

procedure TNedvizhEdit.BitBtn9Click(Sender: TObject);
begin
  NedvizhEdit.Close;
end;

procedure TNedvizhEdit.BitBtn8Click(Sender: TObject);
begin
if (Edit19.Text<>'') then
    begin
      DMod.Nedvizh_Upd.ParamByName('ID_ADRESA').AsInteger:=DMod.Q_AdresID_ADRESA.AsInteger;
      if ComboBox2.ItemIndex=1 then
      DMod.Nedvizh_Upd.ParamByName('ID_REMONT').AsInteger:=DMod.Q_RemontID_REMONT.AsInteger
      else
      DMod.Nedvizh_Upd.ParamByName('ID_REMONT').AsInteger:=1;
      DMod.Nedvizh_Upd.ParamByName('ID_VID_NEDVIZH').AsInteger:=DMod.Q_Vid_NedvizhID_VID_NEDVIZH.AsInteger;
      case DBLookupComboBox1.KeyValue of
      1: begin
      if (MaskEdit1.Text<>'') and (SpinEdit1.Value<>0) and (ComboBox8.Text<>'') and (SpinEdit2.Value<>0) and (MaskEdit5.Text<>'') and (SpinEdit3.Value<>0)
      then begin
      DMod.Nedvizh_Upd.ParamByName('OBSH_PLOSHAD').AsString:=MaskEdit1.Text;
      DMod.Nedvizh_Upd.ParamByName('ETAZH').AsInteger:=SpinEdit1.Value;
      DMod.Nedvizh_Upd.ParamByName('PARKING').AsString:=ComboBox8.Text;
      DMod.Nedvizh_Upd.ParamByName('NOM_KVARTIRI').AsInteger:=SpinEdit2.Value;
      DMod.Nedvizh_Upd.ParamByName('POCHT_INDEX').AsString:=MaskEdit5.Text;
      DMod.Nedvizh_Upd.ParamByName('CENA').AsInteger:=SpinEdit3.Value;
      DMod.Nedvizh_Upd.ParamByName('PLOSHAD_ADM_POMESHEN').AsString:='���';
      DMod.Nedvizh_Upd.ParamByName('ETAZHNOST').AsInteger:=0;
      DMod.Nedvizh_Upd.ParamByName('SROK_EXPLUATAC').AsInteger:=0;
      DMod.Nedvizh_Upd.ParamByName('ID_NEDVIZH').AsInteger:=DMod.Q_NedvizhID_NEDVIZH.AsInteger;
      end
      else
      begin
      MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
      Exit;
      end;
      end;
      2: begin
      if (MaskEdit2.Text<>'') and (MaskEdit4.Text<>'') and (SpinEdit7.Value<>0) and (SpinEdit8.Value<>0)
      then begin
      DMod.Nedvizh_Upd.ParamByName('OBSH_PLOSHAD').AsString:=MaskEdit2.Text;
      DMod.Nedvizh_Upd.ParamByName('ETAZH').AsInteger:=0;
      DMod.Nedvizh_Upd.ParamByName('PARKING').AsString:='���';
      DMod.Nedvizh_Upd.ParamByName('NOM_KVARTIRI').AsInteger:=0;
      DMod.Nedvizh_Upd.ParamByName('POCHT_INDEX').AsString:='���';
      DMod.Nedvizh_Upd.ParamByName('CENA').AsInteger:=SpinEdit8.Value;
      DMod.Nedvizh_Upd.ParamByName('PLOSHAD_ADM_POMESHEN').AsString:=MaskEdit4.Text;
      DMod.Nedvizh_Upd.ParamByName('ETAZHNOST').AsInteger:=0;
      DMod.Nedvizh_Upd.ParamByName('SROK_EXPLUATAC').AsInteger:=SpinEdit7.Value;
      DMod.Nedvizh_Upd.ParamByName('ID_NEDVIZH').AsInteger:=DMod.Q_NedvizhID_NEDVIZH.AsInteger;
      end
      else
      begin
      MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
      Exit;
      end;
      end;
      3: begin
      if (MaskEdit3.Text<>'') and (ComboBox9.Text<>'') and (SpinEdit4.Value<>0) and (SpinEdit5.Value<>0) and (SpinEdit6.Value<>0)
      then begin
      DMod.Nedvizh_Upd.ParamByName('OBSH_PLOSHAD').AsString:=MaskEdit3.Text;
      DMod.Nedvizh_Upd.ParamByName('ETAZH').AsInteger:=0;
      DMod.Nedvizh_Upd.ParamByName('PARKING').AsString:=ComboBox9.Text;
      DMod.Nedvizh_Upd.ParamByName('NOM_KVARTIRI').AsInteger:=0;
      DMod.Nedvizh_Upd.ParamByName('POCHT_INDEX').AsString:='���';
      DMod.Nedvizh_Upd.ParamByName('CENA').AsInteger:=SpinEdit6.Value;
      DMod.Nedvizh_Upd.ParamByName('PLOSHAD_ADM_POMESHEN').AsString:='���';
      DMod.Nedvizh_Upd.ParamByName('ETAZHNOST').AsInteger:=SpinEdit4.Value;
      DMod.Nedvizh_Upd.ParamByName('SROK_EXPLUATAC').AsInteger:=SpinEdit5.Value;
      DMod.Nedvizh_Upd.ParamByName('ID_NEDVIZH').AsInteger:=DMod.Q_NedvizhID_NEDVIZH.AsInteger;
      end
      else
      begin
      MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
      Exit;
      end;
      end;
      end;
      DMod.Nedvizh_Upd.ExecProc;
      DMod.IBTransaction1.CommitRetaining;
      DMod.Q_Nedvizh.Close;
      DMod.Q_Nedvizh.Open;
  MessageBox(Self.Handle, PChar('������ ������� ��������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  NedvizhEdit.Close;
end;
end;

procedure TNedvizhEdit.DBLookupComboBox1CloseUp(Sender: TObject);
begin
  if DBLookupComboBox1.KeyValue<>null then
  begin
   case DBLookupComboBox1.KeyValue of
  1: begin
      GroupBox14.Visible:=True;
      GroupBox12.Visible:=False;
      GroupBox13.Visible:=False;
     end;
  2: begin
      GroupBox14.Visible:=False;
      GroupBox13.Visible:=False;
      GroupBox12.Visible:=True;
     end;
  3: begin
      GroupBox13.Visible:=False;
      GroupBox13.Visible:=True;
      GroupBox14.Visible:=False;
     end;
  end
  end;
end;

procedure TNedvizhEdit.ComboBox2Change(Sender: TObject);
begin
  if ComboBox2.ItemIndex=1
  then begin
  Edit23.Text:='';
  Edit24.Text:='';
  BitBtn4.Visible:=True;
  Edit23.Visible:=True;
  Edit24.Visible:=True;
  Label22.Visible:=True;
  Label23.Visible:=True;
  end
  else
  begin
  Edit23.Text:='1';
  Edit24.Text:='1';
  BitBtn4.Visible:=False;
  Edit23.Visible:=False;
  Edit24.Visible:=False;
  Label22.Visible:=False;
  Label23.Visible:=False;
  end;
end;

procedure TNedvizhEdit.BitBtn10Click(Sender: TObject);
begin
  FormAdres.CheckBox1.Checked:=True;
  FormAdres.Show;
end;

procedure TNedvizhEdit.BitBtn4Click(Sender: TObject);
begin
  FormRemont.CheckBox1.Checked:=False;
  FormRemont.Show;
end;

end.
