object PokupUpd: TPokupUpd
  Left = 135
  Top = 140
  BorderStyle = bsDialog
  Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1077
  ClientHeight = 500
  ClientWidth = 1053
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox10: TGroupBox
    Left = -1
    Top = 2
    Width = 521
    Height = 495
    Caption = #1048#1089#1093#1086#1076#1085#1099#1077
    TabOrder = 0
    object GroupBox5: TGroupBox
      Left = 256
      Top = 16
      Width = 257
      Height = 281
      Caption = #1055#1072#1089#1087#1086#1088#1090#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      TabOrder = 0
      object Label16: TLabel
        Left = 88
        Top = 108
        Width = 75
        Height = 13
        Caption = #1052#1077#1089#1090#1086' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label17: TLabel
        Left = 24
        Top = 84
        Width = 69
        Height = 13
        Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label18: TLabel
        Left = 13
        Top = 20
        Width = 84
        Height = 13
        Caption = #1057#1077#1088#1080#1103' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object Label19: TLabel
        Left = 8
        Top = 52
        Width = 87
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object GroupBox7: TGroupBox
        Left = 16
        Top = 152
        Width = 225
        Height = 121
        Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
        TabOrder = 0
        object RichEdit2: TRichEdit
          Left = 16
          Top = 24
          Width = 193
          Height = 81
          ReadOnly = True
          TabOrder = 0
        end
      end
      object Edit30: TEdit
        Left = 16
        Top = 128
        Width = 225
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit31: TEdit
        Left = 104
        Top = 80
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit10: TEdit
        Left = 104
        Top = 16
        Width = 120
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object Edit11: TEdit
        Left = 104
        Top = 48
        Width = 120
        Height = 21
        ReadOnly = True
        TabOrder = 4
      end
    end
    object GroupBox1: TGroupBox
      Left = 16
      Top = 168
      Width = 225
      Height = 129
      Caption = #1056#1072#1073#1086#1090#1072
      TabOrder = 1
      object Label5: TLabel
        Left = 72
        Top = 24
        Width = 75
        Height = 13
        Caption = #1052#1077#1089#1090#1086' '#1088#1072#1073#1086#1090#1099':'
      end
      object Label6: TLabel
        Left = 56
        Top = 72
        Width = 107
        Height = 13
        Caption = #1045#1078#1077#1084#1077#1089#1103#1095#1085#1099#1081' '#1076#1086#1093#1086#1076':'
      end
      object Edit5: TEdit
        Left = 8
        Top = 40
        Width = 209
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit6: TEdit
        Left = 40
        Top = 88
        Width = 145
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
    end
    object GroupBox2: TGroupBox
      Left = 112
      Top = 312
      Width = 337
      Height = 121
      Caption = #1041#1072#1085#1082#1086#1074#1089#1082#1080#1081' '#1089#1095#1077#1090
      TabOrder = 2
      object Label7: TLabel
        Left = 16
        Top = 27
        Width = 68
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1089#1095#1077#1090#1072':'
      end
      object Label8: TLabel
        Left = 16
        Top = 59
        Width = 118
        Height = 13
        Caption = #1044#1072#1090#1072' '#1080#1089#1090#1077#1095#1077#1085#1080#1103' '#1082#1072#1088#1090#1099':'
      end
      object Label9: TLabel
        Left = 16
        Top = 90
        Width = 132
        Height = 13
        Caption = #1055#1077#1088#1089#1086#1085#1072#1083#1100#1085#1099#1077' '#1080#1085#1080#1094#1080#1072#1083#1099':'
      end
      object Edit7: TEdit
        Left = 152
        Top = 88
        Width = 161
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit8: TEdit
        Left = 144
        Top = 56
        Width = 89
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit9: TEdit
        Left = 96
        Top = 24
        Width = 217
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
    end
    object GroupBox3: TGroupBox
      Left = 16
      Top = 16
      Width = 225
      Height = 145
      Caption = #1055#1077#1088#1089#1086#1085#1072#1083#1100#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      TabOrder = 3
      object Label4: TLabel
        Left = 16
        Top = 116
        Width = 48
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085':'
      end
      object Label3: TLabel
        Left = 16
        Top = 84
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label2: TLabel
        Left = 16
        Top = 56
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label1: TLabel
        Left = 16
        Top = 24
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Edit3: TEdit
        Left = 72
        Top = 84
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit2: TEdit
        Left = 72
        Top = 56
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit1: TEdit
        Left = 72
        Top = 24
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit4: TEdit
        Left = 72
        Top = 116
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
    end
  end
  object GroupBox4: TGroupBox
    Left = 535
    Top = 2
    Width = 521
    Height = 495
    Caption = #1048#1079#1084#1077#1085#1077#1085#1085#1099#1077
    TabOrder = 1
    object GroupBox6: TGroupBox
      Left = 256
      Top = 16
      Width = 257
      Height = 281
      Caption = #1055#1072#1089#1087#1086#1088#1090#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      TabOrder = 0
      object Label10: TLabel
        Left = 88
        Top = 108
        Width = 75
        Height = 13
        Caption = #1052#1077#1089#1090#1086' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label11: TLabel
        Left = 24
        Top = 84
        Width = 69
        Height = 13
        Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label12: TLabel
        Left = 13
        Top = 20
        Width = 84
        Height = 13
        Caption = #1057#1077#1088#1080#1103' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object Label13: TLabel
        Left = 8
        Top = 52
        Width = 87
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object GroupBox8: TGroupBox
        Left = 16
        Top = 152
        Width = 225
        Height = 121
        Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
        TabOrder = 0
        object RichEdit1: TRichEdit
          Left = 16
          Top = 24
          Width = 193
          Height = 81
          TabOrder = 0
        end
      end
      object Edit12: TEdit
        Left = 16
        Top = 128
        Width = 225
        Height = 21
        TabOrder = 1
      end
      object DateTimePicker2: TDateTimePicker
        Left = 104
        Top = 80
        Width = 119
        Height = 21
        Date = 42115.449661446760000000
        Time = 42115.449661446760000000
        TabOrder = 2
      end
      object MaskEdit2: TMaskEdit
        Left = 104
        Top = 48
        Width = 119
        Height = 21
        EditMask = '999999;1;_'
        MaxLength = 6
        TabOrder = 3
        Text = '      '
      end
      object MaskEdit1: TMaskEdit
        Left = 104
        Top = 16
        Width = 120
        Height = 21
        EditMask = '9999;1;_'
        MaxLength = 4
        TabOrder = 4
        Text = '    '
      end
    end
    object GroupBox9: TGroupBox
      Left = 16
      Top = 168
      Width = 225
      Height = 129
      Caption = #1056#1072#1073#1086#1090#1072
      TabOrder = 1
      object Label14: TLabel
        Left = 72
        Top = 24
        Width = 75
        Height = 13
        Caption = #1052#1077#1089#1090#1086' '#1088#1072#1073#1086#1090#1099':'
      end
      object Label15: TLabel
        Left = 56
        Top = 72
        Width = 107
        Height = 13
        Caption = #1045#1078#1077#1084#1077#1089#1103#1095#1085#1099#1081' '#1076#1086#1093#1086#1076':'
      end
      object Edit16: TEdit
        Left = 8
        Top = 40
        Width = 209
        Height = 21
        TabOrder = 0
      end
      object SpinEdit1: TSpinEdit
        Left = 40
        Top = 96
        Width = 145
        Height = 22
        MaxValue = 999999999
        MinValue = 1
        TabOrder = 1
        Value = 1
      end
    end
    object GroupBox11: TGroupBox
      Left = 112
      Top = 312
      Width = 337
      Height = 121
      Caption = #1041#1072#1085#1082#1086#1074#1089#1082#1080#1081' '#1089#1095#1077#1090
      TabOrder = 2
      object Label20: TLabel
        Left = 16
        Top = 27
        Width = 68
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1089#1095#1077#1090#1072':'
      end
      object Label21: TLabel
        Left = 16
        Top = 59
        Width = 118
        Height = 13
        Caption = #1044#1072#1090#1072' '#1080#1089#1090#1077#1095#1077#1085#1080#1103' '#1082#1072#1088#1090#1099':'
      end
      object Label22: TLabel
        Left = 16
        Top = 90
        Width = 132
        Height = 13
        Caption = #1055#1077#1088#1089#1086#1085#1072#1083#1100#1085#1099#1077' '#1080#1085#1080#1094#1080#1072#1083#1099':'
      end
      object Edit18: TEdit
        Left = 152
        Top = 88
        Width = 161
        Height = 21
        TabOrder = 0
        OnKeyPress = Edit18KeyPress
      end
      object MaskEdit3: TMaskEdit
        Left = 96
        Top = 24
        Width = 217
        Height = 21
        EditMask = '0000 0000 0000 0000;1;_'
        MaxLength = 19
        TabOrder = 1
        Text = '                   '
      end
      object DateTimePicker1: TDateTimePicker
        Left = 144
        Top = 56
        Width = 89
        Height = 21
        Date = 42115.118196377310000000
        Format = 'MM.yy'
        Time = 42115.118196377310000000
        TabOrder = 2
      end
    end
    object GroupBox12: TGroupBox
      Left = 16
      Top = 16
      Width = 225
      Height = 145
      Caption = #1055#1077#1088#1089#1086#1085#1072#1083#1100#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      TabOrder = 3
      object Label23: TLabel
        Left = 16
        Top = 116
        Width = 48
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085':'
      end
      object Label24: TLabel
        Left = 16
        Top = 84
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label25: TLabel
        Left = 16
        Top = 56
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label26: TLabel
        Left = 16
        Top = 24
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Edit21: TEdit
        Left = 72
        Top = 84
        Width = 121
        Height = 21
        TabOrder = 0
      end
      object Edit22: TEdit
        Left = 72
        Top = 56
        Width = 121
        Height = 21
        TabOrder = 1
      end
      object Edit23: TEdit
        Left = 72
        Top = 24
        Width = 121
        Height = 21
        TabOrder = 2
      end
      object MaskEdit7: TMaskEdit
        Left = 72
        Top = 112
        Width = 121
        Height = 21
        EditMask = '+7(999)999-99-99;1;_'
        MaxLength = 16
        TabOrder = 3
        Text = '+7(   )   -  -  '
      end
    end
    object BitBtn5: TBitBtn
      Left = 296
      Top = 456
      Width = 83
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 4
      OnClick = BitBtn5Click
    end
    object BitBtn3: TBitBtn
      Left = 432
      Top = 456
      Width = 83
      Height = 33
      Caption = #1043#1086#1090#1086#1074#1086
      TabOrder = 5
      OnClick = BitBtn3Click
    end
  end
end
