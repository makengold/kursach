object RemontAdd: TRemontAdd
  Left = 529
  Top = 202
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1088#1077#1084#1086#1085#1090#1072
  ClientHeight = 148
  ClientWidth = 257
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 7
    Top = 0
    Width = 242
    Height = 145
    Caption = '='
    TabOrder = 0
    object Label1: TLabel
      Left = 40
      Top = 24
      Width = 22
      Height = 13
      Caption = #1042#1080#1076':'
    end
    object Label2: TLabel
      Left = 40
      Top = 64
      Width = 29
      Height = 13
      Caption = #1044#1072#1090#1072':'
    end
    object ComboBox1: TComboBox
      Left = 80
      Top = 24
      Width = 129
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        #1050#1086#1089#1084#1077#1090#1080#1095#1077#1089#1082#1080#1081
        #1050#1072#1087#1080#1090#1072#1083#1100#1085#1099#1081
        #1055#1077#1088#1077#1087#1083#1072#1085#1080#1088#1086#1074#1082#1072' '#1087#1086#1084#1077#1097#1077#1085#1080#1081
        #1045#1074#1088#1086#1088#1077#1084#1086#1085#1090)
    end
    object DateTimePicker1: TDateTimePicker
      Left = 80
      Top = 56
      Width = 128
      Height = 21
      Date = 36892.948311828700000000
      Format = 'dd.MM.yy'
      Time = 36892.948311828700000000
      TabOrder = 1
    end
    object BitBtn1: TBitBtn
      Left = 16
      Top = 96
      Width = 65
      Height = 33
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 2
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 144
      Top = 96
      Width = 65
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 3
      OnClick = BitBtn2Click
    end
  end
end
