program MainPr;

uses
  Forms,
  Main in 'Main.pas' {MainForm},
  Remont in 'Remont.pas' {FormRemont},
  Adres in 'Adres.pas' {FormAdres},
  AddStreet in 'AddStreet.pas' {AdresAdd},
  UnitZastavka in 'UnitZastavka.pas' {Zastavka},
  UnitRemAdd in 'UnitRemAdd.pas' {RemontAdd},
  UnitAdresUpd in 'UnitAdresUpd.pas' {AdresUpd},
  NedvizhUpd in 'NedvizhUpd.pas' {NedvizhEdit},
  UnitOtdel in 'UnitOtdel.pas' {FormOtdel},
  AddOtdel in 'AddOtdel.pas' {OtdelAdd},
  UpdOtdel in 'UpdOtdel.pas' {OtdelUpd},
  UnitSobitie in 'UnitSobitie.pas' {FormSobitie},
  AddSobit in 'AddSobit.pas' {SobitAdd},
  UnitSotrUpd in 'UnitSotrUpd.pas' {SotrudUpd},
  DM in 'DM.pas' {DMod},
  UnitSobitUpd in 'UnitSobitUpd.pas' {SobitUpd},
  UnitNedvizhChng in 'UnitNedvizhChng.pas' {ChangeNedvizh},
  UnitProdavec in 'UnitProdavec.pas' {FormProdavec},
  AddProdavec in 'AddProdavec.pas' {ProdavecAdd},
  UnitProdavecEdit in 'UnitProdavecEdit.pas' {ProdavecUpd},
  UnitAgentChng in 'UnitAgentChng.pas' {ChangeAgent},
  UnitPokup in 'UnitPokup.pas' {FormPokup},
  UnitPokupAdd in 'UnitPokupAdd.pas' {PokupAdd},
  UnitSdelkaUpd in 'UnitSdelkaUpd.pas' {SdelkaUpd},
  UnitViplata in 'UnitViplata.pas' {FormViplata},
  UnitViplataAdd in 'UnitViplataAdd.pas' {ViplataAdd},
  Autoriz in 'Autoriz.pas' {FormAutoriz},
  UnitPokupUpd in 'UnitPokupUpd.pas' {PokupUpd},
  UnitOtchet in 'UnitOtchet.pas' {FormOtchet},
  UnitProsmotr in 'UnitProsmotr.pas' {FormProsmotr},
  sort in 'Sort.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TZastavka, Zastavka);
  Application.CreateForm(TFormAutoriz, FormAutoriz);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TFormRemont, FormRemont);
  Application.CreateForm(TFormAdres, FormAdres);
  Application.CreateForm(TAdresAdd, AdresAdd);
  Application.CreateForm(TRemontAdd, RemontAdd);
  Application.CreateForm(TAdresUpd, AdresUpd);
  Application.CreateForm(TNedvizhEdit, NedvizhEdit);
  Application.CreateForm(TFormOtdel, FormOtdel);
  Application.CreateForm(TOtdelAdd, OtdelAdd);
  Application.CreateForm(TOtdelUpd, OtdelUpd);
  Application.CreateForm(TFormSobitie, FormSobitie);
  Application.CreateForm(TSobitAdd, SobitAdd);
  Application.CreateForm(TSotrudUpd, SotrudUpd);
  Application.CreateForm(TDMod, DMod);
  Application.CreateForm(TSobitUpd, SobitUpd);
  Application.CreateForm(TChangeNedvizh, ChangeNedvizh);
  Application.CreateForm(TFormProdavec, FormProdavec);
  Application.CreateForm(TProdavecAdd, ProdavecAdd);
  Application.CreateForm(TProdavecUpd, ProdavecUpd);
  Application.CreateForm(TChangeAgent, ChangeAgent);
  Application.CreateForm(TFormPokup, FormPokup);
  Application.CreateForm(TPokupAdd, PokupAdd);
  Application.CreateForm(TSdelkaUpd, SdelkaUpd);
  Application.CreateForm(TFormViplata, FormViplata);
  Application.CreateForm(TViplataAdd, ViplataAdd);
  Application.CreateForm(TPokupUpd, PokupUpd);
  Application.CreateForm(TFormOtchet, FormOtchet);
  Application.CreateForm(TFormProsmotr, FormProsmotr);
  Application.Run;
end.
