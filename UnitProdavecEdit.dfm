object ProdavecUpd: TProdavecUpd
  Left = 398
  Top = 98
  ActiveControl = Edit5
  BorderStyle = bsDialog
  Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1077
  ClientHeight = 480
  ClientWidth = 585
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 7
    Top = 0
    Width = 281
    Height = 433
    Caption = #1048#1089#1093#1086#1076#1085#1099#1077
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 24
      Width = 52
      Height = 13
      Caption = #1060#1072#1084#1080#1083#1080#1103':'
    end
    object Label2: TLabel
      Left = 24
      Top = 56
      Width = 25
      Height = 13
      Caption = #1048#1084#1103':'
    end
    object Label3: TLabel
      Left = 24
      Top = 88
      Width = 50
      Height = 13
      Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
    end
    object Label4: TLabel
      Left = 24
      Top = 120
      Width = 48
      Height = 13
      Caption = #1058#1077#1083#1077#1092#1086#1085':'
    end
    object GroupBox5: TGroupBox
      Left = 16
      Top = 152
      Width = 257
      Height = 281
      Caption = #1055#1072#1089#1087#1086#1088#1090#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      TabOrder = 0
      object Label16: TLabel
        Left = 88
        Top = 108
        Width = 75
        Height = 13
        Caption = #1052#1077#1089#1090#1086' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label17: TLabel
        Left = 24
        Top = 84
        Width = 69
        Height = 13
        Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label18: TLabel
        Left = 13
        Top = 20
        Width = 84
        Height = 13
        Caption = #1057#1077#1088#1080#1103' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object Label19: TLabel
        Left = 8
        Top = 52
        Width = 87
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object MaskEdit1: TMaskEdit
        Left = 104
        Top = 16
        Width = 120
        Height = 21
        EditMask = '9999;1;_'
        MaxLength = 4
        ReadOnly = True
        TabOrder = 0
        Text = '    '
      end
      object MaskEdit2: TMaskEdit
        Left = 104
        Top = 48
        Width = 120
        Height = 21
        EditMask = '999999;1;_'
        MaxLength = 6
        ReadOnly = True
        TabOrder = 1
        Text = '      '
      end
      object GroupBox7: TGroupBox
        Left = 16
        Top = 152
        Width = 225
        Height = 121
        Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
        TabOrder = 2
        object RichEdit2: TRichEdit
          Left = 16
          Top = 24
          Width = 193
          Height = 81
          ReadOnly = True
          TabOrder = 0
        end
      end
      object Edit30: TEdit
        Left = 16
        Top = 128
        Width = 225
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object Edit31: TEdit
        Left = 104
        Top = 80
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 4
      end
    end
    object Edit1: TEdit
      Left = 96
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object Edit2: TEdit
      Left = 96
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object Edit3: TEdit
      Left = 96
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object Edit4: TEdit
      Left = 96
      Top = 112
      Width = 121
      Height = 21
      TabOrder = 4
    end
  end
  object GroupBox2: TGroupBox
    Left = 296
    Top = 0
    Width = 281
    Height = 433
    Caption = #1048#1079#1084#1077#1085#1077#1085#1085#1099#1077
    TabOrder = 1
    object Label5: TLabel
      Left = 16
      Top = 32
      Width = 52
      Height = 13
      Caption = #1060#1072#1084#1080#1083#1080#1103':'
    end
    object Label6: TLabel
      Left = 16
      Top = 64
      Width = 25
      Height = 13
      Caption = #1048#1084#1103':'
    end
    object Label7: TLabel
      Left = 16
      Top = 96
      Width = 50
      Height = 13
      Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
    end
    object Label8: TLabel
      Left = 16
      Top = 128
      Width = 48
      Height = 13
      Caption = #1058#1077#1083#1077#1092#1086#1085':'
    end
    object Edit5: TEdit
      Left = 120
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object Edit6: TEdit
      Left = 120
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object Edit7: TEdit
      Left = 120
      Top = 88
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object MaskEdit7: TMaskEdit
      Left = 120
      Top = 120
      Width = 121
      Height = 21
      EditMask = '+7(999)999-99-99;1;_'
      MaxLength = 16
      TabOrder = 3
      Text = '+7(   )   -  -  '
    end
    object GroupBox3: TGroupBox
      Left = 8
      Top = 152
      Width = 265
      Height = 281
      Caption = #1055#1072#1089#1087#1086#1088#1090#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      TabOrder = 4
      object Label9: TLabel
        Left = 88
        Top = 108
        Width = 75
        Height = 13
        Caption = #1052#1077#1089#1090#1086' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label10: TLabel
        Left = 24
        Top = 84
        Width = 69
        Height = 13
        Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label11: TLabel
        Left = 13
        Top = 20
        Width = 84
        Height = 13
        Caption = #1057#1077#1088#1080#1103' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object Label12: TLabel
        Left = 8
        Top = 52
        Width = 87
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object MaskEdit3: TMaskEdit
        Left = 104
        Top = 16
        Width = 120
        Height = 21
        EditMask = '9999;1;_'
        MaxLength = 4
        TabOrder = 0
        Text = '    '
      end
      object MaskEdit4: TMaskEdit
        Left = 104
        Top = 48
        Width = 120
        Height = 21
        EditMask = '999999;1;_'
        MaxLength = 6
        TabOrder = 1
        Text = '      '
      end
      object Edit8: TEdit
        Left = 16
        Top = 128
        Width = 225
        Height = 21
        TabOrder = 2
      end
      object Edit9: TEdit
        Left = 104
        Top = 80
        Width = 121
        Height = 21
        TabOrder = 3
      end
      object GroupBox4: TGroupBox
        Left = 16
        Top = 152
        Width = 225
        Height = 121
        Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
        TabOrder = 4
        object RichEdit1: TRichEdit
          Left = 16
          Top = 24
          Width = 193
          Height = 81
          TabOrder = 0
        end
      end
    end
  end
  object BitBtn1: TBitBtn
    Left = 312
    Top = 440
    Width = 97
    Height = 33
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    TabOrder = 2
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 168
    Top = 440
    Width = 97
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 3
    OnClick = BitBtn2Click
  end
end
