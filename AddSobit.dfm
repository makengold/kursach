object SobitAdd: TSobitAdd
  Left = 503
  Top = 145
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1086#1073#1099#1090#1080#1077
  ClientHeight = 313
  ClientWidth = 402
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 0
    Width = 385
    Height = 313
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 68
      Height = 13
      Caption = #1058#1080#1087' '#1089#1086#1073#1099#1090#1080#1103':'
    end
    object Label2: TLabel
      Left = 184
      Top = 24
      Width = 79
      Height = 13
      Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072':'
    end
    object Label3: TLabel
      Left = 192
      Top = 88
      Width = 123
      Height = 13
      Caption = #1057#1086#1076#1077#1088#1078#1072#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072':'
    end
    object Label4: TLabel
      Left = 16
      Top = 88
      Width = 75
      Height = 13
      Caption = #1044#1072#1090#1072' '#1089#1086#1073#1099#1090#1080#1103':'
    end
    object Label5: TLabel
      Left = 136
      Top = 200
      Width = 107
      Height = 13
      Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086':'
    end
    object RichEdit1: TRichEdit
      Left = 192
      Top = 112
      Width = 185
      Height = 73
      TabOrder = 0
    end
    object ComboBox1: TComboBox
      Left = 16
      Top = 48
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = ComboBox1Change
      Items.Strings = (
        #1055#1088#1080#1077#1084' '#1085#1072' '#1088#1072#1073#1086#1090#1091
        #1059#1074#1086#1083#1100#1085#1077#1085#1080#1077)
    end
    object ComboBox2: TComboBox
      Left = 184
      Top = 48
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = ComboBox2Change
      Items.Strings = (
        #1055#1088#1080#1082#1072#1079
        #1047#1072#1103#1074#1083#1077#1085#1080#1077)
    end
    object DateTimePicker1: TDateTimePicker
      Left = 16
      Top = 112
      Width = 145
      Height = 21
      Date = 42105.754053599540000000
      Time = 42105.754053599540000000
      TabOrder = 3
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 96
      Top = 224
      Width = 193
      Height = 21
      KeyField = 'ID_SOTRUD'
      ListField = 'FIO'
      ListSource = DMod.DS_Sotrud1
      TabOrder = 4
      OnEnter = DBLookupComboBox1Enter
    end
    object BitBtn4: TBitBtn
      Left = 256
      Top = 264
      Width = 99
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 5
      OnClick = BitBtn4Click
    end
    object BitBtn1: TBitBtn
      Left = 24
      Top = 264
      Width = 99
      Height = 33
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 6
      OnClick = BitBtn1Click
    end
  end
end
