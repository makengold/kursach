object FormOtchet: TFormOtchet
  Left = 550
  Top = 235
  BorderStyle = bsDialog
  Caption = #1054#1090#1095#1077#1090#1099
  ClientHeight = 204
  ClientWidth = 221
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 64
    Width = 178
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1086#1088#1084#1072#1090' '#1076#1083#1103' '#1089#1086#1093#1088#1072#1085#1077#1085#1080#1103':'
  end
  object BitBtn1: TBitBtn
    Left = 72
    Top = 120
    Width = 83
    Height = 33
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 64
    Top = 16
    Width = 97
    Height = 33
    Caption = #1055#1088#1077#1076#1087#1088#1086#1089#1084#1086#1090#1088
    TabOrder = 1
    OnClick = BitBtn2Click
  end
  object ComboBox1: TComboBox
    Left = 40
    Top = 88
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    Items.Strings = (
      'PDF'
      'BMP'
      'JPEG')
  end
  object BitBtn3: TBitBtn
    Left = 72
    Top = 168
    Width = 81
    Height = 25
    Caption = #1043#1086#1090#1086#1074#1086
    TabOrder = 3
    OnClick = BitBtn3Click
  end
  object frxBMPExport1: TfrxBMPExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    Left = 168
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 200
  end
  object frxJPEGExport1: TfrxJPEGExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    Left = 136
  end
end
