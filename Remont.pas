unit Remont;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, Grids, DBGrids, Buttons, ComCtrls;

type
  TFormRemont = class(TForm)
    DBGrid1: TDBGrid;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    CheckBox1: TCheckBox;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BitBtn4Click(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
    sort:boolean;
  end;

var
  FormRemont: TFormRemont;

implementation

uses DM, Main, UnitRemAdd, NedvizhUpd;

{$R *.dfm}

procedure TFormRemont.BitBtn2Click(Sender: TObject);
begin
 Edit1.Clear;
 Edit2.Clear;
 FormRemont.Close;
end;

procedure TFormRemont.BitBtn1Click(Sender: TObject);
begin
 RemontAdd.ShowModal;
end;

procedure TFormRemont.BitBtn5Click(Sender: TObject);
begin
  if CheckBox1.Checked=False then
  begin
  MainForm.Edit9.Text:=Edit2.Text;
  MainForm.Edit10.Text:=Edit1.Text;
  Edit1.Clear;
  Edit2.Clear;
  end
  else
  begin
  NedvizhEdit.Edit23.Text:=Edit2.Text;
  NedvizhEdit.Edit24.Text:=Edit1.Text;
  Edit1.Clear;
  Edit2.Clear;
  end;
  FormRemont.Close;
end;

procedure TFormRemont.DBGrid1CellClick(Column: TColumn);
begin
 Edit1.Text:=DMod.Q_RemontNAIMEN.AsString;
 Edit2.Text:=DMod.Q_RemontDATA.AsString
end;

procedure TFormRemont.BitBtn4Click(Sender: TObject);
begin
    case MessageBox(Handle,'�� �������, ��� ������ ������� ����?','��������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    DMod.Remont_Del.ParamByName('ID_REMONT').AsInteger:=DMod.Q_RemontID_REMONT.AsInteger;
    try
    DMod.Remont_Del.ExecProc;
    MessageBox(Self.Handle, PChar('������ �������'), PChar('������'), MB_OK+MB_ICONINFORMATION);
    except
    MessageBox(Self.Handle, PChar('�������� ����������, ��� ��� ������ ������������ � ������ �������'), PChar(''), MB_OK+MB_ICONWARNING);
    end;
    end;
    IDNO: Exit;
    end;
    DMod.Q_Remont.Close;
    DMod.Q_Remont.Open;
end;

procedure TFormRemont.DBGrid1TitleClick(Column: TColumn);
begin
  if sort=true then
  begin
  DMod.Q_Remont.Close;
  DMod.Q_Remont.SQL.Clear;
  DMod.Q_Remont.SQL.Text:='Select * FROM Vid_Remonta where ID_REMONT<>1 order by DATA ASC ';
  DMod.Q_Remont.Close;
  DMod.Q_Remont.Open;
  sort:=false;
  end
  else
  begin
  DMod.Q_Remont.Close;
  DMod.Q_Remont.SQL.Clear;
  DMod.Q_Remont.SQL.Text:='Select * FROM Vid_Remonta where ID_REMONT<>1 order by DATA DESC';
  DMod.Q_Remont.Close;
  DMod.Q_Remont.Open;
  sort:=true;
  end;
end;

end.
