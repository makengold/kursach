unit UnitPokupUpd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, Spin, Mask;

type
  TPokupUpd = class(TForm)
    GroupBox10: TGroupBox;
    GroupBox5: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    GroupBox7: TGroupBox;
    RichEdit2: TRichEdit;
    Edit30: TEdit;
    Edit31: TEdit;
    Edit10: TEdit;
    Edit11: TEdit;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Edit5: TEdit;
    Edit6: TEdit;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    Edit3: TEdit;
    Edit2: TEdit;
    Edit1: TEdit;
    Edit4: TEdit;
    GroupBox4: TGroupBox;
    GroupBox6: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    GroupBox8: TGroupBox;
    RichEdit1: TRichEdit;
    Edit12: TEdit;
    GroupBox9: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Edit16: TEdit;
    GroupBox11: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Edit18: TEdit;
    GroupBox12: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Edit21: TEdit;
    Edit22: TEdit;
    Edit23: TEdit;
    BitBtn5: TBitBtn;
    BitBtn3: TBitBtn;
    MaskEdit7: TMaskEdit;
    DateTimePicker2: TDateTimePicker;
    MaskEdit2: TMaskEdit;
    MaskEdit1: TMaskEdit;
    SpinEdit1: TSpinEdit;
    MaskEdit3: TMaskEdit;
    DateTimePicker1: TDateTimePicker;
    procedure BitBtn5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure Edit18KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PokupUpd: TPokupUpd;

implementation

uses DM, UnitPokupAdd;

{$R *.dfm}

procedure TPokupUpd.BitBtn5Click(Sender: TObject);
var
i:integer;
begin
  for i:=0 to GroupBox3.ControlCount-1 do
  if GroupBox3.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox3.Controls[i]).Clear;
  for i:=0 to GroupBox5.ControlCount-1 do
  if GroupBox5.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox5.Controls[i]).Clear;
  for i:=0 to GroupBox1.ControlCount-1 do
  if GroupBox1.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox1.Controls[i]).Clear;
  for i:=0 to GroupBox2.ControlCount-1 do
  if GroupBox2.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox2.Controls[i]).Clear;
  RichEdit2.Lines.Clear;
  PokupUpd.Close;
end;

procedure TPokupUpd.FormShow(Sender: TObject);
begin
  Edit23.Text:=Edit1.Text;
  Edit22.Text:=Edit2.Text;
  Edit21.Text:=Edit3.Text;
  MaskEdit7.Text:=Edit4.Text;
  Edit16.Text:=Edit5.Text;
  SpinEdit1.Value:=StrToInt(Edit6.Text);
  MaskEdit1.Text:=Edit10.Text;
  MaskEdit2.Text:=Edit11.Text;
  DateTimePicker2.Date:=StrToDate(Edit31.Text);
  Edit12.Text:=Edit30.Text;
  RichEdit1.Text:=RichEdit2.Text;
  MaskEdit3.Text:=Edit9.Text;
  DateTimePicker1.Date:=StrToDate(Edit8.Text);
  Edit18.Text:=Edit7.Text;
end;

procedure TPokupUpd.BitBtn3Click(Sender: TObject);
begin
  if (Edit23.Text<>'') and (Edit22.Text<>'') and (Edit21.Text<>'') and (MaskEdit7.Text<>'') and (Edit16.Text<>'') and (SpinEdit1.Text<>'') and (MaskEdit1.Text<>'') and (MaskEdit2.Text<>'') and (Edit12.Text<>'') and (RichEdit1.Lines.Text<>'') and (MaskEdit3.Text<>'') and (Edit18.Text<>'')
  then begin
  DMod.Pokup_Upd.ParamByName('FAMILY').AsString:=Edit23.Text;
  DMod.Pokup_Upd.ParamByName('IMYA').AsString:=Edit22.Text;
  DMod.Pokup_Upd.ParamByName('OTCHESTVO').AsString:=Edit21.Text;
  DMod.Pokup_Upd.ParamByName('TEL').AsString:=MaskEdit7.Text;
  DMod.Pokup_Upd.ParamByName('SER_PASPORT').AsString:=MaskEdit1.Text;
  DMod.Pokup_Upd.ParamByName('NOM_PASPORT').AsString:=MaskEdit2.Text;
  DMod.Pokup_Upd.ParamByName('DATA_VIDACHI_PASPORT').AsString:=DateToStr(DateTimePicker2.Date);
  DMod.Pokup_Upd.ParamByName('KEM_VIDAN_PASPORT').AsString:=Edit12.Text;
  DMod.Pokup_Upd.ParamByName('ADRES_REG').AsString:=RichEdit1.Lines.Text;
  DMod.Pokup_Upd.ParamByName('MEST_RABOTI').AsString:=Edit16.Text;
  DMod.Pokup_Upd.ParamByName('MES_DOHOD').AsString:=SpinEdit1.Text;
  DMod.Pokup_Upd.ParamByName('NOM_BANK_KARTI').AsString:=MaskEdit3.Text;
  DMod.Pokup_Upd.ParamByName('DATA_ISTECH_KARTI').AsString:=DateToStr(DateTimePicker1.Date);
  DMod.Pokup_Upd.ParamByName('NAIMEN_VLADELCA').AsString:=Edit18.Text;
  DMod.Pokup_Upd.ParamByName('ID_POKUP').AsInteger:=DMod.Q_PokupID_POKUP.AsInteger;
  DMod.Pokup_Upd.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Pokup.Close;
  DMod.Q_Pokup.Open;
  MessageBox(Self.Handle, PChar('������ ������� ��������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  PokupUpd.Close;
  Exit;
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
end;

procedure TPokupUpd.Edit18KeyPress(Sender: TObject; var Key: Char);
begin
if not (Key in ['A'..'Z','a'..'z',#8,#32]) then
 Key:=#0;
end;

end.
