unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ImgList, StdCtrls, Buttons, Grids, DBGrids, Mask, Spin,
  DBCtrls, sSkinManager;

type
  TMainForm = class(TForm)
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    ImageList1: TImageList;
    BitBtn1: TBitBtn;
    TabSheet4: TTabSheet;
    GroupBox3: TGroupBox;
    Label17: TLabel;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    BitBtn10: TBitBtn;
    GroupBox5: TGroupBox;
    Label6: TLabel;
    Label11: TLabel;
    Label16: TLabel;
    ComboBox2: TComboBox;
    Edit9: TEdit;
    Edit10: TEdit;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    GroupBox7: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    BitBtn8: TBitBtn;
    GroupBox10: TGroupBox;
    Label30: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit4: TMaskEdit;
    SpinEdit7: TSpinEdit;
    SpinEdit8: TSpinEdit;
    GroupBox9: TGroupBox;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label31: TLabel;
    MaskEdit3: TMaskEdit;
    ComboBox9: TComboBox;
    SpinEdit4: TSpinEdit;
    SpinEdit5: TSpinEdit;
    SpinEdit6: TSpinEdit;
    GroupBox8: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    MaskEdit5: TMaskEdit;
    MaskEdit1: TMaskEdit;
    ComboBox8: TComboBox;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    SpinEdit3: TSpinEdit;
    BitBtn9: TBitBtn;
    DBGrid2: TDBGrid;
    DBLookupComboBox1: TDBLookupComboBox;
    BitBtn2: TBitBtn;
    BitBtn6: TBitBtn;
    DBGrid3: TDBGrid;
    GroupBox6: TGroupBox;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Edit11: TEdit;
    Edit12: TEdit;
    Edit13: TEdit;
    ComboBox3: TComboBox;
    DateTimePicker1: TDateTimePicker;
    GroupBox11: TGroupBox;
    Label41: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Edit14: TEdit;
    Edit15: TEdit;
    Edit16: TEdit;
    GroupBox13: TGroupBox;
    GroupBox15: TGroupBox;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label54: TLabel;
    MaskEdit8: TMaskEdit;
    MaskEdit9: TMaskEdit;
    DateTimePicker4: TDateTimePicker;
    GroupBox12: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    BitBtn7: TBitBtn;
    Label14: TLabel;
    Edit17: TEdit;
    GroupBox14: TGroupBox;
    Label15: TLabel;
    Label33: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Edit18: TEdit;
    Edit19: TEdit;
    Edit20: TEdit;
    Edit21: TEdit;
    Edit22: TEdit;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    GroupBox16: TGroupBox;
    BitBtn13: TBitBtn;
    BitBtn14: TBitBtn;
    Label44: TLabel;
    DBLookupComboBox2: TDBLookupComboBox;
    GroupBox17: TGroupBox;
    Label48: TLabel;
    Label49: TLabel;
    Edit23: TEdit;
    Edit24: TEdit;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label51: TLabel;
    Edit3: TEdit;
    BitBtn19: TBitBtn;
    BitBtn3: TBitBtn;
    GroupBox18: TGroupBox;
    Edit4: TEdit;
    Edit25: TEdit;
    Edit26: TEdit;
    Edit27: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label50: TLabel;
    Label52: TLabel;
    BitBtn20: TBitBtn;
    GroupBox19: TGroupBox;
    BitBtn22: TBitBtn;
    GroupBox21: TGroupBox;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label53: TLabel;
    Edit31: TEdit;
    Edit32: TEdit;
    Edit33: TEdit;
    Edit34: TEdit;
    BitBtn23: TBitBtn;
    Edit28: TEdit;
    GroupBox20: TGroupBox;
    Label55: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label64: TLabel;
    Edit29: TEdit;
    Edit30: TEdit;
    Edit35: TEdit;
    Edit36: TEdit;
    BitBtn21: TBitBtn;
    BitBtn24: TBitBtn;
    Label65: TLabel;
    Label66: TLabel;
    ComboBox1: TComboBox;
    RichEdit1: TRichEdit;
    Edit1: TEdit;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    BitBtn25: TBitBtn;
    BitBtn26: TBitBtn;
    BitBtn27: TBitBtn;
    DBGrid1: TDBGrid;
    BitBtn28: TBitBtn;
    TabSheet3: TTabSheet;
    DBGrid4: TDBGrid;
    BitBtn29: TBitBtn;
    BitBtn30: TBitBtn;
    BitBtn31: TBitBtn;
    GroupBox22: TGroupBox;
    BitBtn32: TBitBtn;
    Edit2: TEdit;
    Edit37: TEdit;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    ComboBox4: TComboBox;
    Label73: TLabel;
    BitBtn34: TBitBtn;
    BitBtn33: TBitBtn;
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure DBLookupComboBox1CloseUp(Sender: TObject);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure sDBComboBox1Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn17Click(Sender: TObject);
    procedure DBLookupComboBox2Enter(Sender: TObject);
    procedure DBLookupComboBox1Enter(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
    procedure DBGrid3CellClick(Column: TColumn);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn24Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure BitBtn20Click(Sender: TObject);
    procedure BitBtn21Click(Sender: TObject);
    procedure BitBtn23Click(Sender: TObject);
    procedure BitBtn22Click(Sender: TObject);
    procedure BitBtn25Click(Sender: TObject);
    procedure BitBtn27Click(Sender: TObject);
    procedure BitBtn26Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BitBtn28Click(Sender: TObject);
    procedure Edit37Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn31Click(Sender: TObject);
    procedure BitBtn29Click(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
    procedure BitBtn30Click(Sender: TObject);
    procedure DBGrid4CellClick(Column: TColumn);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure Edit37KeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BitBtn33Click(Sender: TObject);
    procedure BitBtn34Click(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure DBGrid3TitleClick(Column: TColumn);
    procedure DBGrid2TitleClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  a:integer;
  end;

var
  MainForm: TMainForm;

implementation

uses AddStreet, Remont, Adres, DM, ViNedvizh, NedvizhUpd, UnitOtdel,
  UnitSobitie, UnitSotrUpd, AddSobit, AddProdavec, UnitNedvizhChng, UnitProdavec,
  UnitAgentChng, UnitPokup, UnitSdelkaUpd, UnitViplata, Autoriz,
  UnitZastavka, UnitOtchet, sort;

{$R *.dfm}

procedure TMainForm.BitBtn6Click(Sender: TObject);
begin
  if NedvizhEdit.Edit2.Text<>'' then
  NedvizhEdit.ShowModal
  else
   MessageBox(Self.Handle, PChar('�������� ���� ��� ���������'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TMainForm.BitBtn4Click(Sender: TObject);
begin
FormRemont.CheckBox1.Checked:=False;
FormRemont.ShowModal;
end;

procedure TMainForm.BitBtn5Click(Sender: TObject);
begin
GroupBox3.Visible:=False;
GroupBox7.Visible:=True;
end;

procedure TMainForm.BitBtn8Click(Sender: TObject);
var
i:integer;
begin
     if (Edit5.Text<>'') and (Edit9.Text<>'') then
    begin
      DMod.Nedvizh_Add.ParamByName('ID_ADRESA').AsInteger:=DMod.Q_AdresID_ADRESA.AsInteger;
      if ComboBox2.ItemIndex=1 then
      DMod.Nedvizh_Add.ParamByName('ID_REMONT').AsInteger:=DMod.Q_RemontID_REMONT.AsInteger
      else
      DMod.Nedvizh_Add.ParamByName('ID_REMONT').AsInteger:=1;
      DMod.Nedvizh_Add.ParamByName('ID_VID_NEDVIZH').AsInteger:=DMod.Q_Vid_NedvizhID_VID_NEDVIZH.AsInteger;
      case DBLookupComboBox1.KeyValue of
      1: begin
      if (MaskEdit1.Text<>'') and (SpinEdit1.Value<>0) and (ComboBox8.Text<>'') and (SpinEdit2.Value<>0) and (MaskEdit5.Text<>'') and (SpinEdit3.Value<>0)
      then begin
      DMod.Nedvizh_Add.ParamByName('OBSH_PLOSHAD').AsString:=MaskEdit1.Text;
      DMod.Nedvizh_Add.ParamByName('ETAZH').AsInteger:=SpinEdit1.Value;
      DMod.Nedvizh_Add.ParamByName('PARKING').AsString:=ComboBox8.Text;
      DMod.Nedvizh_Add.ParamByName('NOM_KVARTIRI').AsInteger:=SpinEdit2.Value;
      DMod.Nedvizh_Add.ParamByName('POCHT_INDEX').AsString:=MaskEdit5.Text;
      DMod.Nedvizh_Add.ParamByName('CENA').AsInteger:=SpinEdit3.Value;
      DMod.Nedvizh_Add.ParamByName('PLOSHAD_ADM_POMESHEN').AsString:='���';
      DMod.Nedvizh_Add.ParamByName('ETAZHNOST').AsInteger:=0;
      DMod.Nedvizh_Add.ParamByName('SROK_EXPLUATAC').AsInteger:=0;
      end
      else
      begin
       MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
      Exit;
      end;
      end;
      2: begin
      if (MaskEdit2.Text<>'') and (MaskEdit4.Text<>'') and (SpinEdit7.Value<>0) and (SpinEdit8.Value<>0)
      then begin
      DMod.Nedvizh_Add.ParamByName('OBSH_PLOSHAD').AsString:=MaskEdit2.Text;
      DMod.Nedvizh_Add.ParamByName('ETAZH').AsInteger:=0;
      DMod.Nedvizh_Add.ParamByName('PARKING').AsString:='���';
      DMod.Nedvizh_Add.ParamByName('NOM_KVARTIRI').AsInteger:=0;
      DMod.Nedvizh_Add.ParamByName('POCHT_INDEX').AsString:='���';
      DMod.Nedvizh_Add.ParamByName('CENA').AsInteger:=SpinEdit8.Value;
      DMod.Nedvizh_Add.ParamByName('PLOSHAD_ADM_POMESHEN').AsString:=MaskEdit4.Text;
      DMod.Nedvizh_Add.ParamByName('ETAZHNOST').AsInteger:=0;
      DMod.Nedvizh_Add.ParamByName('SROK_EXPLUATAC').AsInteger:=SpinEdit7.Value;
      end
      else
      begin
       MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
      Exit;
      end;
      end;
      3: begin
      if (MaskEdit3.Text<>'') and (ComboBox9.Text<>'') and (SpinEdit4.Value<>0) and (SpinEdit5.Value<>0) and (SpinEdit6.Value<>0)
      then begin
      DMod.Nedvizh_Add.ParamByName('OBSH_PLOSHAD').AsString:=MaskEdit3.Text;
      DMod.Nedvizh_Add.ParamByName('ETAZH').AsInteger:=0;
      DMod.Nedvizh_Add.ParamByName('PARKING').AsString:=ComboBox9.Text;
      DMod.Nedvizh_Add.ParamByName('NOM_KVARTIRI').AsInteger:=0;
      DMod.Nedvizh_Add.ParamByName('POCHT_INDEX').AsString:='���';
      DMod.Nedvizh_Add.ParamByName('CENA').AsInteger:=SpinEdit6.Value;
      DMod.Nedvizh_Add.ParamByName('PLOSHAD_ADM_POMESHEN').AsString:='���';
      DMod.Nedvizh_Add.ParamByName('ETAZHNOST').AsInteger:=SpinEdit4.Value;
      DMod.Nedvizh_Add.ParamByName('SROK_EXPLUATAC').AsInteger:=SpinEdit5.Value;
      end
      else
      begin
       MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
      Exit;
      end;
      end;
      end;
      DMod.Nedvizh_Add.ExecProc;
      DMod.IBTransaction1.CommitRetaining;
      DMod.Q_Nedvizh.Close;
      DMod.Q_Nedvizh.Open;
       MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
      DBLookupComboBox1.KeyValue:=-1;
      GroupBox8.Visible:=False;
      GroupBox9.Visible:=False;
      GroupBox10.Visible:=False;
      //
      ComboBox2.ItemIndex:=0;
      BitBtn4.Visible:=False;
      Edit10.Visible:=False;
      Edit9.Visible:=False;
      Label11.Visible:=False;
      Label16.Visible:=False;
      //
    for i:=0 to GroupBox4.ControlCount-1 do
      if GroupBox4.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox4.Controls[i]).Clear;
      for i:=0 to GroupBox5.ControlCount-1 do
      if GroupBox5.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox5.Controls[i]).Clear;
      for i:=0 to GroupBox8.ControlCount-1 do
      if GroupBox8.Controls[i].ClassName = 'TSpinEdit' then TSpinEdit(GroupBox8.Controls[i]).Value:=0;
      for i:=0 to GroupBox8.ControlCount-1 do
      if GroupBox8.Controls[i].ClassName = 'TMaskEdit' then  TMaskEdit(GroupBox8.Controls[i]).Clear;
      for i:=0 to GroupBox9.ControlCount-1 do
      if GroupBox9.Controls[i].ClassName = 'TSpinEdit' then TSpinEdit(GroupBox9.Controls[i]).Value:=0;
      for i:=0 to GroupBox9.ControlCount-1 do
      if GroupBox9.Controls[i].ClassName = 'TMaskEdit' then  TMaskEdit(GroupBox9.Controls[i]).Clear;
      for i:=0 to GroupBox10.ControlCount-1 do
      if GroupBox10.Controls[i].ClassName = 'TSpinEdit' then TSpinEdit(GroupBox10.Controls[i]).Value:=0;
      for i:=0 to GroupBox10.ControlCount-1 do
      if GroupBox10.Controls[i].ClassName = 'TMaskEdit' then  TMaskEdit(GroupBox10.Controls[i]).Clear;
      //
      ComboBox8.ItemIndex:=0;
      ComboBox9.ItemIndex:=0;
      GroupBox3.Visible:=True;
      GroupBox7.Visible:=False;
    end
    else
     MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TMainForm.BitBtn9Click(Sender: TObject);
begin
GroupBox3.Visible:=True;
GroupBox7.Visible:=False;
end;

procedure TMainForm.BitBtn10Click(Sender: TObject);
begin
 FormAdres.CheckBox1.Checked:=False;
 FormAdres.ShowModal;
end;

procedure TMainForm.BitBtn2Click(Sender: TObject);
begin
    case MessageBox(Handle,'�� �������, ��� ������ ������� ����?','��������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    DMod.Nedvizh_Del.ParamByName('ID_NEDVIZH').AsInteger:=DMod.Q_NedvizhID_NEDVIZH.AsInteger;
    try
    DMod.Nedvizh_Del.ExecProc;
    MessageBox(Self.Handle, PChar('������ �������'), PChar('������'), MB_OK+MB_ICONINFORMATION);
    except
    MessageBox(Self.Handle, PChar('�������� ����������, ��� ��� ������ ������������ � ������ �������'), PChar(''), MB_OK+MB_ICONWARNING);
    end;
    end;
    IDNO: Exit;
    end;
    DMod.Q_Nedvizh.Close;
    DMod.Q_Nedvizh.Open;
end;

procedure TMainForm.DBLookupComboBox1CloseUp(Sender: TObject);
begin
  if DBLookupComboBox1.KeyValue<>null then
  begin
   case DBLookupComboBox1.KeyValue of
  1: begin
      GroupBox8.Visible:=True;
      GroupBox9.Visible:=False;
      GroupBox10.Visible:=False;
     end;
  2: begin
      GroupBox8.Visible:=False;
      GroupBox9.Visible:=False;
      GroupBox10.Visible:=True;
     end;
  3: begin
      GroupBox8.Visible:=False;
      GroupBox9.Visible:=True;
      GroupBox10.Visible:=False;
     end;
  end
  end;
end;

procedure TMainForm.DBGrid2CellClick(Column: TColumn);
begin
  NedvizhEdit.Edit2.Text:=DMod.Q_NedvizhGOROD.AsString;
  NedvizhEdit.Edit3.Text:=DMod.Q_NedvizhRAYON.AsString;
  NedvizhEdit.Edit4.Text:=DMod.Q_NedvizhULITCA.AsString;
  NedvizhEdit.Edit11.Text:=DMod.Q_NedvizhDOM.AsString;
  if DMod.Q_NedvizhNAIMEN.AsString='�����������' then
  begin
  NedvizhEdit.Edit1.Text:='�����������';
  NedvizhEdit.Edit9.Text:=DMod.Q_NedvizhDATA.AsString;
  NedvizhEdit.Edit10.Text:=DMod.Q_NedvizhNAIMEN1.AsString
  end
  else
  begin
  NedvizhEdit.Edit1.Text:='����������';
  NedvizhEdit.Edit9.Text:=DMod.Q_NedvizhDATA.AsString;
  NedvizhEdit.Edit10.Text:=DMod.Q_NedvizhNAIMEN1.AsString;
  end;
  NedvizhEdit.Edit5.Text:=DMod.Q_NedvizhNAIMEN.AsString;
  if DMod.Q_NedvizhNAIMEN.AsString='��������' then
  begin
  NedvizhEdit.MaskEdit10.Text:=DMod.Q_NedvizhOBSH_PLOSHAD.AsString;
  NedvizhEdit.Edit14.Text:=DMod.Q_NedvizhETAZH.AsString;
  NedvizhEdit.Edit6.Text:=DMod.Q_NedvizhPARKING.AsString;
  NedvizhEdit.Edit15.Text:=DMod.Q_NedvizhNOM_KVARTIRI.AsString;
  NedvizhEdit.MaskEdit9.Text:=DMod.Q_NedvizhPOCHT_INDEX.AsString;
  NedvizhEdit.Edit16.Text:=DMod.Q_NedvizhCENA.AsString;
  end;
  if DMod.Q_NedvizhNAIMEN.AsString='����' then
  begin
  NedvizhEdit.MaskEdit8.Text:=DMod.Q_NedvizhOBSH_PLOSHAD.AsString;
  NedvizhEdit.Edit8.Text:=DMod.Q_NedvizhETAZHNOST.AsString;
  NedvizhEdit.Edit7.Text:=DMod.Q_NedvizhPARKING.AsString;
  NedvizhEdit.Edit12.Text:=DMod.Q_NedvizhSROK_EXPLUATAC.AsString;
  NedvizhEdit.Edit13.Text:=DMod.Q_NedvizhCENA.AsString;
  end;
  if DMod.Q_NedvizhNAIMEN.AsString='�����' then
  begin
  NedvizhEdit.MaskEdit6.Text:=DMod.Q_NedvizhOBSH_PLOSHAD.AsString;
  NedvizhEdit.MaskEdit7.Text:=DMod.Q_NedvizhPLOSHAD_ADM_POMESHEN.AsString;
  NedvizhEdit.Edit17.Text:=DMod.Q_NedvizhSROK_EXPLUATAC.AsString;
  NedvizhEdit.Edit18.Text:=DMod.Q_NedvizhCENA.AsString;
  end;
end;




procedure TMainForm.sDBComboBox1Change(Sender: TObject);
begin
  if ComboBox2.ItemIndex=1
  then begin
  Edit9.Text:='';
  Edit10.Text:='';
  BitBtn4.Visible:=True;
  Edit10.Visible:=True;
  Edit9.Visible:=True;
  Label11.Visible:=True;
  Label16.Visible:=True;
  end
  else
  begin
  BitBtn4.Visible:=False;
  Edit10.Visible:=False;
  Edit9.Visible:=False;
  Label11.Visible:=False;
  Label16.Visible:=False;
  end;
end;

procedure TMainForm.ComboBox2Change(Sender: TObject);
begin
  if ComboBox2.ItemIndex=1
  then begin
  Edit9.Text:='';
  Edit10.Text:='';
  BitBtn4.Visible:=True;
  Edit10.Visible:=True;
  Edit9.Visible:=True;
  Label11.Visible:=True;
  Label16.Visible:=True;
  end
  else
  begin
  BitBtn4.Visible:=False;
  Edit10.Visible:=False;
  Edit9.Visible:=False;
  Label11.Visible:=False;
  Label16.Visible:=False;
  end;
end;

procedure TMainForm.BitBtn7Click(Sender: TObject);
begin
 GroupBox6.Visible:=False;
 GroupBox13.Visible:=True;
end;

procedure TMainForm.BitBtn12Click(Sender: TObject);
begin
 GroupBox6.Visible:=True;
 GroupBox13.Visible:=False;
end;

procedure TMainForm.BitBtn14Click(Sender: TObject);
begin
 GroupBox16.Visible:=False;
 GroupBox13.Visible:=True;
end;

procedure TMainForm.BitBtn11Click(Sender: TObject);
begin
 GroupBox13.Visible:=False;
 GroupBox16.Visible:=True;
end;

procedure TMainForm.BitBtn15Click(Sender: TObject);
begin
  FormOtdel.Show;
end;

procedure TMainForm.BitBtn16Click(Sender: TObject);
begin
  if FormSobitie.Edit5.Text<>'' then
  FormSobitie.ShowModal
  else
   MessageBox(Self.Handle, PChar('�������� ����������, ������� �������� �� ������ �����������'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TMainForm.BitBtn13Click(Sender: TObject);
var
j:integer;
begin

     if (Edit11.Text<>'') and (Edit12.Text<>'') and (Edit13.Text<>'') and (Edit14.Text<>'') and (Edit15.Text<>'') and (Edit16.Text<>'') and (Edit17.Text<>'') and (MaskEdit8.Text<>'') and (MaskEdit9.Text<>'') and (Edit22.Text<>'') and (Edit18.Text<>'') and (Edit19.Text<>'') and (Edit20.Text<>'') and (Edit21.Text<>'') and (Edit23.Text<>'') and (DBLookupComboBox2.KeyValue<>null)  then
    begin
      DMod.Sotrud_Add.ParamByName('ID_DOLZH').AsInteger:=DMod.Q_DolzhnID_DOLZH.AsInteger;
      DMod.Sotrud_Add.ParamByName('ID_OTDELA').AsInteger:=DMod.Q_OtdelID_OTDELA.AsInteger;
      DMod.Sotrud_Add.ParamByName('FAMILY').AsString:=Edit11.Text;
      DMod.Sotrud_Add.ParamByName('IMYA').AsString:=Edit12.Text;
      DMod.Sotrud_Add.ParamByName('OTCHESTVO').AsString:=Edit13.Text;
      DMod.Sotrud_Add.ParamByName('POL').AsString:=ComboBox3.Text;;
      DMod.Sotrud_Add.ParamByName('DATA_ROZHDEN').AsString:=DateToStr(DateTimePicker1.Date);
      DMod.Sotrud_Add.ParamByName('MESTO_ZHIT').AsString:='�.'+Edit14.Text+', ��.'+Edit15.Text+', ��� '+Edit16.Text+', ��.'+Edit17.Text;
      DMod.Sotrud_Add.ParamByName('SER_PASPORT').AsString:=MaskEdit8.Text;
      DMod.Sotrud_Add.ParamByName('NOM_PASPORT').AsString:=MaskEdit9.Text;
      DMod.Sotrud_Add.ParamByName('DATA_VIDACHI_PASPORT').AsString:=DateToStr(DateTimePicker4.Date);
      DMod.Sotrud_Add.ParamByName('KEM_VIDAN_PASPORT').AsString:=Edit22.Text;
      DMod.Sotrud_Add.ParamByName('AGRES_REG').AsString:='�.'+Edit18.Text+', ��.'+Edit19.Text+', ��� '+Edit20.Text+', ��.'+Edit21.Text;
      DMod.Sotrud_Add.ParamByName('DOM_TELEFON').AsString:=MaskEdit7.Text;
      DMod.Sotrud_Add.ParamByName('RAB_TELEFON').AsString:=MaskEdit6.Text;
      DMod.Sotrud_Add.ExecProc;
      DMod.IBTransaction1.CommitRetaining;
      DMod.Q_Sotrud.Close;
      DMod.Q_Sotrud.Open;
       MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
      for j:=0 to GroupBox6.ControlCount-1 do
      if GroupBox6.Controls[j].ClassName = 'TEdit' then TEdit(GroupBox6.Controls[j]).Clear;
      ComboBox3.ItemIndex:=0;
      DateTimePicker1.Date:=now;
      for j:=0 to GroupBox11.ControlCount-1 do
      if GroupBox11.Controls[j].ClassName = 'TEdit' then TEdit(GroupBox11.Controls[j]).Clear;
      MaskEdit6.Clear;
      MaskEdit7.Clear;
      //
      MaskEdit8.Clear;
      MaskEdit9.Clear;
      DateTimePicker4.Date:=now;
      Edit22.Text:='';
      for j:=0 to GroupBox14.ControlCount-1 do
      if GroupBox14.Controls[j].ClassName = 'TEdit' then TEdit(GroupBox14.Controls[j]).Clear;
      //
      DBLookupComboBox2.KeyValue:=null;
      Edit23.Clear;
      Edit24.Clear;
      //
      GroupBox6.Visible:=True;
      GroupBox13.Visible:=False;
      GroupBox16.Visible:=False;
      end
    else
     MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
    Exit;
end;

procedure TMainForm.BitBtn17Click(Sender: TObject);
begin
    case MessageBox(Handle,'�� �������, ��� ������ ������� ����?','��������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    DMod.Sotrud_Del.ParamByName('ID_SOTRUD').AsInteger:=DMod.Q_SotrudID_SOTRUD.AsInteger;
    try
    DMod.Sotrud_Del.ExecProc;
    MessageBox(Self.Handle, PChar('������ �������'), PChar('������'), MB_OK+MB_ICONINFORMATION);
    except
    MessageBox(Self.Handle, PChar('�������� ����������, ��� ��� ������ ������������ � ������ �������'), PChar(''), MB_OK+MB_ICONWARNING);
    end;
    end;
    IDNO: Exit;
    end;
    DMod.Q_Sotrud.Close;
    DMod.Q_Sotrud.Open;
end;

procedure TMainForm.DBLookupComboBox2Enter(Sender: TObject);
begin
MainForm.DBLookupComboBox2.ListSource.DataSet.Last;
end;

procedure TMainForm.DBLookupComboBox1Enter(Sender: TObject);
begin
MainForm.DBLookupComboBox1.ListSource.DataSet.Last;
end;

procedure TMainForm.BitBtn18Click(Sender: TObject);
begin
  if SotrudUpd.Edit1.Text<>'' then
  SotrudUpd.ShowModal
  else
   MessageBox(Self.Handle, PChar('�������� ���� ��� ���������'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TMainForm.DBGrid3CellClick(Column: TColumn);
begin
  SotrudUpd.Edit1.Text:=DMod.Q_SotrudFAMILY.AsString;
  SotrudUpd.Edit2.Text:=DMod.Q_SotrudIMYA.AsString;
  SotrudUpd.Edit3.Text:=DMod.Q_SotrudOTCHESTVO.AsString;
  FormSobitie.Edit5.Text:=DMod.Q_SotrudFAMILY.AsString;
  FormSobitie.Edit6.Text:=DMod.Q_SotrudIMYA.AsString;
  FormSobitie.Edit7.Text:=DMod.Q_SotrudOTCHESTVO.AsString;
  FormSobitie.Edit8.Text:=DMod.Q_SotrudNAIMEN.AsString;
  SotrudUpd.Edit8.Text:=DMod.Q_SotrudPOL.AsString;
  SotrudUpd.Edit9.Text:=DMod.Q_SotrudDATA_ROZHDEN.AsString;
  SotrudUpd.RichEdit1.Lines.Add(DMod.Q_SotrudMESTO_ZHIT.AsString);
  SotrudUpd.Edit10.Text:=DMod.Q_SotrudRAB_TELEFON.AsString;
  SotrudUpd.Edit25.Text:=DMod.Q_SotrudDOM_TELEFON.AsString;
  SotrudUpd.MaskEdit1.Text:=DMod.Q_SotrudSER_PASPORT.AsString;
  SotrudUpd.MaskEdit2.Text:=DMod.Q_SotrudNOM_PASPORT.AsString;
  SotrudUpd.Edit31.Text:=DMod.Q_SotrudDATA_VIDACHI_PASPORT.AsString;
  SotrudUpd.Edit30.Text:=DMod.Q_SotrudKEM_VIDAN_PASPORT.AsString;
  SotrudUpd.RichEdit2.Text:=DMod.Q_SotrudAGRES_REG.AsString;
  SotrudUpd.Edit32.Text:=DMod.Q_DolzhnNAIMEN.AsString;
  SotrudUpd.Edit33.Text:=DMod.Q_OtdelNAIMEN.AsString;
  SotrudUpd.Edit34.Text:=DMod.Q_OtdelRUKOVOD.AsString;
  DMod.Q_Vibor.ParamByName('GRISHA_TSAR').Value:=DMod.Q_SotrudID_SOTRUD.Value;
  DMod.Q_Vibor.Close;
  DMod.Q_Vibor.Open;
end;

procedure TMainForm.BitBtn3Click(Sender: TObject);
begin
  GroupBox1.Visible:=False;
  GroupBox19.Visible:=True;
end;

procedure TMainForm.BitBtn24Click(Sender: TObject);
begin
  GroupBox1.Visible:=True;
  GroupBox19.Visible:=False;;
end;

procedure TMainForm.ComboBox1Change(Sender: TObject);
begin
  GroupBox2.Height:=177;
  RichEdit1.Visible:=True;
  Edit3.Visible:=True;
  Label4.Visible:=True;
  Label51.Visible:=True;
  DMod.Q_NedvizhChange.Close;
  DMod.Q_NedvizhChange.ParamByName('naimenovanie').AsString:=ComboBox1.Text;
  DMod.Q_NedvizhChange.Open;
  RichEdit1.Clear;
  Edit3.Clear;
end;

procedure TMainForm.BitBtn19Click(Sender: TObject);
begin
  ChangeNedvizh.BitBtn1.Tag:=0;
  if ComboBox1.Text<>'' then
  ChangeNedvizh.ShowModal
  else
  ShowMessage('�������� ��� ������������!');
  Exit;
end;

procedure TMainForm.BitBtn20Click(Sender: TObject);
begin
  FormProdavec.BitBtn3.Tag:=0;
  FormProdavec.ShowModal;
end;

procedure TMainForm.BitBtn21Click(Sender: TObject);
begin
  ChangeAgent.BitBtn1.Tag:=0;
  ChangeAgent.ShowModal;
end;

procedure TMainForm.BitBtn23Click(Sender: TObject);
begin
  FormPokup.BitBtn3.Tag:=0;
  FormPokup.ShowModal;
end;

procedure TMainForm.BitBtn22Click(Sender: TObject);
var
i:integer;
begin
  if (Edit1.Text<>'') and (RichEdit1.Text<>'') and (Edit31.Text<>'') and (Edit29.Text<>'') then
  begin
  DMod.Sdelka_Add.ParamByName('ID_PRODAVCA').AsInteger:=DMod.Q_ProdavecID_PRODAVCA.AsInteger;
  DMod.Sdelka_Add.ParamByName('ID_POKUP').AsInteger:=DMod.Q_PokupID_POKUP.AsInteger;
  DMod.Sdelka_Add.ParamByName('ID_SOTRUD').AsInteger:=DMod.Q_AgentChangeID_SOTRUD.AsInteger;
  DMod.Sdelka_Add.ParamByName('ID_NEDVIZH').AsInteger:=DMod.Q_NedvizhChangeID_NEDVIZH.AsInteger;
  DMod.Sdelka_Add.ParamByName('NOM_DOGV').AsString:=Edit1.Text;
  DMod.Sdelka_Add.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Sdelka.Close;
  DMod.Q_Sdelka.Open;
  Edit1.Clear;
  ComboBox1.ItemIndex:=-1;
  RichEdit1.Clear;
  Edit3.Clear;
  for i:=0 to GroupBox18.ControlCount-1 do
  if GroupBox18.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox18.Controls[i]).Clear;
  for i:=0 to GroupBox21.ControlCount-1 do
  if GroupBox21.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox21.Controls[i]).Clear;
  for i:=0 to GroupBox20.ControlCount-1 do
  if GroupBox20.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox20.Controls[i]).Clear;
  GroupBox19.Visible:=False;
  GroupBox1.Visible:=True;
  GroupBox2.Height:=49;
  MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  end
  else
   MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TMainForm.BitBtn25Click(Sender: TObject);
begin
  DMod.Q_Sdelka.Last;
  Edit1.Text:=IntToStr(242000+DMod.Q_SdelkaID_USL.AsInteger);
end;

procedure TMainForm.BitBtn27Click(Sender: TObject);
begin
    case MessageBox(Handle,'�� �������, ��� ������ ������� ����?','��������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    DMod.Sdelka_Del.ParamByName('ID_USL').AsInteger:=DMod.Q_SdelkaID_USL.AsInteger;
    DMod.Sdelka_Del.ExecProc;
    end;
    IDNO: Exit;
    end;
    DMod.Q_Sdelka.Close;
    DMod.Q_Sdelka.Open;
    MessageBox(Self.Handle, PChar('������ �������'), PChar(''), MB_OK+MB_ICONINFORMATION);
end;

procedure TMainForm.BitBtn26Click(Sender: TObject);
begin
  if SdelkaUpd.Edit1.Text<>'' then
  SdelkaUpd.ShowModal
  else
  MessageBox(Self.Handle, PChar('�������� ���� ��� ���������'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TMainForm.DBGrid1CellClick(Column: TColumn);
begin
  SdelkaUpd.Edit1.Text:=DMod.Q_SdelkaNOM_DOGV.AsString;
  SdelkaUpd.Edit2.Text:=DMod.Q_SdelkaNAIMEN.AsString;
  SdelkaUpd.RichEdit1.Text:='�.'+DMod.Q_SdelkaGOROD.AsString+', ����� '+DMod.Q_SdelkaRAYON.AsString+', ��. '+DMod.Q_SdelkaULITCA.AsString+', ��� '+DMod.Q_SdelkaDOM.AsString+',��. '+DMod.Q_SdelkaNOM_KVARTIRI.AsString;
  SdelkaUpd.Edit3.Text:=DMod.Q_SdelkaCENA.AsString;
  SdelkaUpd.Edit4.Text:=DMod.Q_SdelkaFAMILY1.AsString;
  SdelkaUpd.Edit25.Text:=DMod.Q_SdelkaIMYA1.AsString;
  SdelkaUpd.Edit26.Text:=DMod.Q_SdelkaOTCHESTVO1.AsString;
  SdelkaUpd.Edit27.Text:=DMod.Q_SdelkaTEL.AsString;
  SdelkaUpd.Edit31.Text:=DMod.Q_SdelkaFAMILY2.AsString;
  SdelkaUpd.Edit32.Text:=DMod.Q_SdelkaIMYA2.AsString;
  SdelkaUpd.Edit33.Text:=DMod.Q_SdelkaOTCHESTVO2.AsString;
  SdelkaUpd.Edit34.Text:=DMod.Q_SdelkaTEL1.AsString;
  SdelkaUpd.Edit28.Text:=DMod.Q_SdelkaNOM_BANK_KARTI.AsString;
  SdelkaUpd.Edit29.Text:=DMod.Q_SdelkaFAMILY.AsString;
  SdelkaUpd.Edit30.Text:=DMod.Q_SdelkaIMYA.AsString;
  SdelkaUpd.Edit35.Text:=DMod.Q_SdelkaOTCHESTVO.AsString;
  SdelkaUpd.Edit36.Text:=DMod.Q_SdelkaRAB_TELEFON.AsString;
  FormViplata.Edit1.Text:=DMod.Q_SdelkaNOM_DOGV.AsString;
  FormViplata.Edit5.Text:=DMod.Q_SdelkaCENA.AsString+' ���';
  DMod.Q_Vibor2.ParamByName('GRISHA_NETSAR').Value:=DMod.Q_SdelkaID_USL.Value;
  DMod.Q_Vibor2.Close;
  DMod.Q_Vibor2.Open;
end;

procedure TMainForm.BitBtn28Click(Sender: TObject);
begin
  if FormViplata.Edit1.Text<>'' then
  FormViplata.ShowModal
  else
  MessageBox(Self.Handle, PChar('�������� ������'), PChar(''), MB_OK+MB_ICONWARNING);
end;

procedure TMainForm.Edit37Change(Sender: TObject);
begin
  DMod.Q_Sotrud.Close;
  DMod.Q_Sotrud.SQL.Clear;
  DMod.Q_Sotrud.SQL.Add('');
  DMod.Q_Sotrud.Open;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 Application.Terminate;
end;

procedure TMainForm.BitBtn1Click(Sender: TObject);
begin
    case MessageBox(Handle,'�� ����� ������ ������� ������������?','�����������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    MainForm.Hide;
    Zastavka.ProgressBar1.Visible:=True;
    Zastavka.ProgressBar1.Position:=0;
    FormAutoriz.Show;
    end;
    IDNO: Exit;
    end;
end;

procedure TMainForm.BitBtn31Click(Sender: TObject);
begin
  Edit2.Text:='';
  Edit37.Text:='';
  ComboBox4.ItemIndex:=-1;
  GroupBox22.Caption:='����������';
end;

procedure TMainForm.BitBtn29Click(Sender: TObject);
begin
  GroupBox22.Caption:='���������';
  if Edit2.Text='' then
  MessageBox(Self.Handle, PChar('�������� ���� ��� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
end;

procedure TMainForm.BitBtn32Click(Sender: TObject);
begin
if GroupBox22.Caption='����������' then
begin
if (ComboBox4.Text<>'') and (Edit2.Text<>'') and (Edit37.Text<>'') then
  begin
  if DMod.Q_Users.Locate('LOGIN',Edit2.Text,[]) then begin
  MessageBox(Self.Handle, PChar('������ ����� ��� �����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
  DMod.Users_Add.ParamByName('LOGIN').AsString:=Edit2.Text;
  DMod.Users_Add.ParamByName('PASSWORD').AsString:=Edit37.Text;
    if ComboBox4.Text='�������������' then
  DMod.Users_Add.ParamByName('PRAVA').AsInteger:=0;
    if ComboBox4.Text='����������� ��������' then
  DMod.Users_Add.ParamByName('PRAVA').AsInteger:=1;
    if ComboBox4.Text='��������� ������ ������' then
  DMod.Users_Add.ParamByName('PRAVA').AsInteger:=2;
    if ComboBox4.Text='����������� �������������' then
  DMod.Users_Add.ParamByName('PRAVA').AsInteger:=3;
  DMod.Users_Add.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Users.Close;
  DMod.Q_Users.Open;
  MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
  end;
  if GroupBox22.Caption='���������' then
  begin
  if (ComboBox4.Text<>'') and (Edit2.Text<>'') and (Edit37.Text<>'') then
  begin
  DMod.Users_Upd.ParamByName('LOGIN').AsString:=Edit2.Text;
  DMod.Users_Upd.ParamByName('PASSWORD').AsString:=Edit37.Text;
    if ComboBox4.Text='�������������' then
  DMod.Users_Upd.ParamByName('PRAVA').AsInteger:=0;
    if ComboBox4.Text='����������� ��������' then
  DMod.Users_Upd.ParamByName('PRAVA').AsInteger:=1;
    if ComboBox4.Text='��������� ������ ������' then
  DMod.Users_Upd.ParamByName('PRAVA').AsInteger:=2;
    if ComboBox4.Text='����������� �������������' then
  DMod.Users_Upd.ParamByName('PRAVA').AsInteger:=3;
  DMod.Users_Upd.ParamByName('ID_AUTOR').Value:=DMod.Q_UsersID_AUTOR.Value;
  DMod.Users_Upd.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Users.Close;
  DMod.Q_Users.Open;
  MessageBox(Self.Handle, PChar('������ ������� ��������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
  end;

end;

procedure TMainForm.BitBtn30Click(Sender: TObject);
begin
    case MessageBox(Handle,'�� �������, ��� ������ ������� ����?','��������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    DMod.Users_Del.ParamByName('ID_AUTOR').AsInteger:=DMod.Q_UsersID_AUTOR.AsInteger;
    DMod.Users_Del.ExecProc;
    end;
    IDNO: Exit;
    end;
    DMod.Q_Users.Close;
    DMod.Q_Users.Open;
    MessageBox(Self.Handle, PChar('������ �������'), PChar(''), MB_OK+MB_ICONINFORMATION);
end;

procedure TMainForm.DBGrid4CellClick(Column: TColumn);
begin
  if GroupBox22.Caption='���������' then
  begin
  Edit2.Text:=DMod.Q_UsersLOGIN.AsString;
  Edit37.Text:=DMod.Q_UsersPASSWORD.AsString;
    if DMod.Q_UsersPRAVA.AsInteger=0 then
  ComboBox4.ItemIndex:=0;
    if DMod.Q_UsersPRAVA.AsInteger=1 then
  ComboBox4.ItemIndex:=1;
    if DMod.Q_UsersPRAVA.AsInteger=2 then
  ComboBox4.ItemIndex:=2;
    if DMod.Q_UsersPRAVA.AsInteger=3 then
  ComboBox4.ItemIndex:=3;
  end;
end;

procedure TMainForm.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
if not (Key in ['A'..'Z','a'..'z',#8,#32,'0'..'9']) then
 Key:=#0;
end;

procedure TMainForm.Edit37KeyPress(Sender: TObject; var Key: Char);
begin
if not (Key in ['A'..'Z','a'..'z',#8,#32,'0'..'9']) then
 Key:=#0;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    case MessageBox(Handle,'������ ������� ������������?','�����', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    CanClose:=False;
    MainForm.Hide;
    Zastavka.ProgressBar1.Visible:=True;
    Zastavka.ProgressBar1.Position:=0;
    FormAutoriz.Show;
    end;
    IDNO: Application.Terminate;
    end;
end;

procedure TMainForm.BitBtn33Click(Sender: TObject);
begin
  FormOtchet.BitBtn2.Tag:=1;
  FormOtchet.ShowModal;
end;

procedure TMainForm.BitBtn34Click(Sender: TObject);
begin
  FormOtchet.BitBtn2.Tag:=0;
  FormOtchet.ShowModal;
end;

procedure TMainForm.DBGrid1TitleClick(Column: TColumn);
begin
  DBSort(DMod.Q_Sdelka,DBGrid1,Column);
end;

procedure TMainForm.DBGrid3TitleClick(Column: TColumn);
begin
   DBSort(DMod.Q_Sotrud,DBGrid3,Column);
end;

procedure TMainForm.DBGrid2TitleClick(Column: TColumn);
begin
  DBSort(DMod.Q_Nedvizh,DBGrid2,Column);
end;

end.
