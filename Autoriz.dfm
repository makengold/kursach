object FormAutoriz: TFormAutoriz
  Left = 567
  Top = 254
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = #1040#1074#1090#1086#1088#1080#1079#1072#1094#1080#1103
  ClientHeight = 211
  ClientWidth = 255
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 64
    Top = 16
    Width = 77
    Height = 13
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1083#1086#1075#1080#1085':'
  end
  object Label2: TLabel
    Left = 64
    Top = 64
    Width = 84
    Height = 13
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1087#1072#1088#1086#1083#1100':'
  end
  object Edit1: TEdit
    Left = 64
    Top = 32
    Width = 137
    Height = 21
    TabOrder = 0
    OnKeyPress = Edit1KeyPress
  end
  object Edit2: TEdit
    Left = 64
    Top = 80
    Width = 137
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    OnKeyPress = Edit2KeyPress
  end
  object BitBtn1: TBitBtn
    Left = 96
    Top = 112
    Width = 81
    Height = 25
    Caption = #1042#1093#1086#1076
    TabOrder = 2
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 96
    Top = 152
    Width = 81
    Height = 25
    Caption = #1042#1099#1073#1086#1088' '#1041#1044
    TabOrder = 3
    OnClick = BitBtn2Click
  end
  object Edit3: TEdit
    Left = 8
    Top = 184
    Width = 153
    Height = 21
    TabOrder = 4
  end
  object BitBtn3: TBitBtn
    Left = 168
    Top = 184
    Width = 83
    Height = 25
    Caption = #1055#1086#1076#1082#1083#1102#1095#1080#1090#1100#1089#1103
    TabOrder = 5
    OnClick = BitBtn3Click
  end
  object OpenDialog1: TOpenDialog
    FilterIndex = 0
    Left = 65512
    Top = 200
  end
  object OpenDialog2: TOpenDialog
    Left = 224
    Top = 8
  end
end
