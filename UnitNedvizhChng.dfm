object ChangeNedvizh: TChangeNedvizh
  Left = 265
  Top = 115
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088' '#1085#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1080
  ClientHeight = 354
  ClientWidth = 768
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 769
    Height = 297
    DataSource = DMod.DS_NedvizhChange
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NAIMEN'
        Title.Caption = #1042#1080#1076' '#1085#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1080
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GOROD'
        Title.Caption = #1043#1086#1088#1086#1076
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RAYON'
        Title.Caption = #1056#1072#1081#1086#1085
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ULITCA'
        Title.Caption = #1059#1083#1080#1094#1072
        Width = 135
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DOM'
        Title.Caption = #1044#1086#1084
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OBSH_PLOSHAD'
        Title.Caption = #1054#1073#1097#1072#1103' '#1087#1083#1086#1097#1072#1076#1100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CENA'
        Title.Caption = #1062#1077#1085#1072
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PLOSHAD_ADM_POMESHEN'
        Title.Caption = #1055#1083#1086#1097#1072#1076#1100' '#1072#1076#1084'. '#1087#1086#1084#1077#1097#1077#1085#1080#1081
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ETAZH'
        Title.Caption = #1069#1090#1072#1078
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ETAZHNOST'
        Title.Caption = #1069#1090#1072#1078#1085#1086#1089#1090#1100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PARKING'
        Title.Caption = #1055#1072#1088#1082#1080#1085#1075
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SROK_EXPLUATAC'
        Title.Caption = #1057#1088#1086#1082' '#1101#1082#1089#1087#1083#1091#1072#1090#1072#1094#1080#1080
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOM_KVARTIRI'
        Title.Caption = #1053#1086#1084#1077#1088' '#1082#1074#1072#1088#1090#1080#1088#1099
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'POCHT_INDEX'
        Title.Caption = #1055#1086#1095#1090#1086#1074#1099#1081' '#1080#1085#1076#1077#1082#1089
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAIMEN1'
        Title.Caption = #1042#1080#1076' '#1088#1077#1084#1086#1085#1090#1072
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA'
        Title.Caption = #1044#1072#1090#1072' '#1088#1077#1084#1086#1085#1090#1072
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ID_NEDVIZH'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ID_VID_NEDVIZH'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ID_ADRESA'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ID_REMONT'
        Visible = False
      end>
  end
  object BitBtn1: TBitBtn
    Left = 664
    Top = 312
    Width = 91
    Height = 33
    Caption = #1043#1086#1090#1086#1074#1086
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 544
    Top = 312
    Width = 91
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = BitBtn2Click
  end
end
