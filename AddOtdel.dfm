object OtdelAdd: TOtdelAdd
  Left = 485
  Top = 212
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1086#1090#1076#1077#1083
  ClientHeight = 152
  ClientWidth = 368
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 32
    Width = 104
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1085#1072#1079#1074#1072#1085#1080#1077':'
  end
  object Label2: TLabel
    Left = 8
    Top = 72
    Width = 126
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1088#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1103':'
  end
  object BitBtn1: TBitBtn
    Left = 40
    Top = 104
    Width = 81
    Height = 33
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 248
    Top = 104
    Width = 83
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    OnClick = BitBtn2Click
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 144
    Top = 64
    Width = 201
    Height = 21
    KeyField = 'ID_SOTRUD'
    ListField = 'FIO'
    ListSource = DMod.DS_Sotrud
    TabOrder = 2
    OnEnter = DBLookupComboBox1Enter
  end
  object ComboBox1: TComboBox
    Left = 144
    Top = 24
    Width = 201
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    Items.Strings = (
      #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085#1085#1099#1081' '#1086#1090#1076#1077#1083
      #1054#1090#1076#1077#1083' '#1073#1077#1079#1086#1087#1072#1089#1085#1086#1089#1090#1080
      #1054#1090#1076#1077#1083' '#1082#1072#1076#1088#1086#1074
      #1054#1090#1076#1077#1083' '#1091#1095#1105#1090#1072
      'IT-'#1086#1090#1076#1077#1083)
  end
end
