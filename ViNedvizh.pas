unit ViNedvizh;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids;

type
  TVidNedvizh = class(TForm)
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn4: TBitBtn;
    Label1: TLabel;
    Edit1: TEdit;
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  VidNedvizh: TVidNedvizh;

implementation

uses DM, VidNevizhAdd;

{$R *.dfm}

procedure TVidNedvizh.DBGrid1CellClick(Column: TColumn);
begin
 //Edit1.Text:=DMod.Q_Vid_NedvizhNAIMEN.AsString;
end;

procedure TVidNedvizh.BitBtn3Click(Sender: TObject);
begin
 Edit1.Clear;
 VidNedvizhAdd.Close;
end;

procedure TVidNedvizh.BitBtn4Click(Sender: TObject);
begin
 Edit1.Clear;
 VidNedvizh.Close;
end;

procedure TVidNedvizh.BitBtn1Click(Sender: TObject);
begin
 //VidNevizhAdd.Show;
end;

end.
