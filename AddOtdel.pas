unit AddOtdel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, StdCtrls, Buttons;

type
  TOtdelAdd = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    ComboBox1: TComboBox;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBLookupComboBox1Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OtdelAdd: TOtdelAdd;

implementation

uses DM, UnitRemAdd;

{$R *.dfm}

procedure TOtdelAdd.BitBtn2Click(Sender: TObject);
begin
  OtdelAdd.Close;
  ComboBox1.ItemIndex:=-1;
  DBLookupComboBox1.KeyValue:=null;
end;

procedure TOtdelAdd.BitBtn1Click(Sender: TObject);
begin
 if (ComboBox1.Text<>'') and (DBLookupComboBox1.KeyValue<>null)
  then begin
  DMod.Otdel_Add.ParamByName('NAIMEN').AsString:=ComboBox1.Text;
  DMod.Otdel_Add.ParamByName('RUKOVOD').AsString:=DBLookupComboBox1.Text;
  DMod.Otdel_Add.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Otdel.Close;
  DMod.Q_Otdel.Open;
  MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
  RemontAdd.Close;
  OtdelAdd.Close;
  ComboBox1.ItemIndex:=-1;
  DBLookupComboBox1.KeyValue:=null;
end;

procedure TOtdelAdd.DBLookupComboBox1Enter(Sender: TObject);
begin
OtdelAdd.DBLookupComboBox1.ListSource.DataSet.Last;
end;

end.
