unit UnitViplataAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Spin, StdCtrls, Buttons, ComCtrls;

type
  TViplataAdd = class(TForm)
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit4: TEdit;
    DateTimePicker1: TDateTimePicker;
    BitBtn1: TBitBtn;
    SpinEdit2: TSpinEdit;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ViplataAdd: TViplataAdd;

implementation

uses DM;

{$R *.dfm}

procedure TViplataAdd.BitBtn3Click(Sender: TObject);
begin
  ViplataAdd.Close;
  DateTimePicker1.Date:=Now;
  SpinEdit2.Value:=1;
  Edit4.Clear;
end;

procedure TViplataAdd.BitBtn1Click(Sender: TObject);
begin
  if DMod.Q_Vibor2.RecordCount<1 then
  try
  Edit4.Text:=IntToStr(DMod.Q_SdelkaCENA.AsInteger-SpinEdit2.Value)
  except
  else
  DMod.Q_Vibor2.Last;
  Edit4.Text:=IntToStr(DMod.Q_Vibor2OSTAT_VIPL.AsInteger-SpinEdit2.Value);
  MessageBox(Self.Handle, PChar('������ ������� ������������'), PChar(''), MB_OK+MB_ICONINFORMATION);
end;
end;

procedure TViplataAdd.BitBtn2Click(Sender: TObject);
begin
  if Edit4.Text<>'' then
  begin
  DMod.Viplata_Add.ParamByName('DATA_VIPL').AsString:=DateToStr(DateTimePicker1.Date);
  DMod.Viplata_Add.ParamByName('SUM_VIPL').AsString:=SpinEdit2.Text;
  DMod.Viplata_Add.ParamByName('OSTAT_VIPL').AsString:=Edit4.Text;
  DMod.Viplata_Add.ParamByName('ID_USL').AsInteger:=DMod.Q_SdelkaID_USL.AsInteger;
  DMod.Viplata_Add.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Vibor2.Close;
  DMod.Q_Vibor2.Open;
  MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  DateTimePicker1.Date:=Now;
  SpinEdit2.Value:=1;
  Edit4.Clear;
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
  ViplataAdd.Close;
end;

end.
