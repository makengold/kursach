unit UnitZastavka;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, XPMan, jpeg, ComCtrls;

type
  TZastavka = class(TForm)
    ProgressBar1: TProgressBar;
    Image1: TImage;
    Timer1: TTimer;
    Label1: TLabel;
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Zastavka: TZastavka;

implementation

uses Main, Autoriz;

{$R *.dfm}

procedure TZastavka.Timer1Timer(Sender: TObject);
begin
randomize;
if ProgressBar1.Position = 100 then
begin
  Timer1.Enabled:=false;
  ProgressBar1.Visible:=false;
  Zastavka.Hide;
  sleep(1000);
  FormAutoriz.Show;
  end
else
  begin
  ProgressBar1.Position:=progressbar1.Position+(1);
  Label1.Caption:='Загрузка '+ inttostr(progressbar1.Position)+' %';
  end;
end;

procedure TZastavka.FormShow(Sender: TObject);
begin
  Timer1.Enabled:=True;
end;

procedure TZastavka.FormCreate(Sender: TObject);
begin
 DoubleBuffered:=true;
end;

end.
