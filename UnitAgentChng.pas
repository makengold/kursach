unit UnitAgentChng;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids;

type
  TChangeAgent = class(TForm)
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ChangeAgent: TChangeAgent;

implementation

uses UnitAdresUpd, DM, Main, UnitSdelkaUpd;

{$R *.dfm}

procedure TChangeAgent.BitBtn2Click(Sender: TObject);
begin
  ChangeAgent.Close;
end;

procedure TChangeAgent.BitBtn1Click(Sender: TObject);
begin
  if BitBtn1.Tag=0 then
  begin
  MainForm.Edit29.Text:=DMod.Q_AgentChangeFAMILY.AsString;
  MainForm.Edit30.Text:=DMod.Q_AgentChangeIMYA.AsString;
  MainForm.Edit35.Text:=DMod.Q_AgentChangeOTCHESTVO.AsString;
  MainForm.Edit36.Text:=DMod.Q_AgentChangeRAB_TELEFON.AsString;
  ChangeAgent.Close;
  end
  else
  begin
  SdelkaUpd.Edit16.Text:=DMod.Q_AgentChangeFAMILY.AsString;
  SdelkaUpd.Edit17.Text:=DMod.Q_AgentChangeIMYA.AsString;
  SdelkaUpd.Edit18.Text:=DMod.Q_AgentChangeOTCHESTVO.AsString;
  SdelkaUpd.Edit19.Text:=DMod.Q_AgentChangeRAB_TELEFON.AsString;
  ChangeAgent.Close;
  end;
end;

end.
