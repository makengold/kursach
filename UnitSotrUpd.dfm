object SotrudUpd: TSotrudUpd
  Left = 215
  Top = 107
  BorderStyle = bsDialog
  Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1077
  ClientHeight = 479
  ClientWidth = 967
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 569
    Height = 473
    Caption = #1048#1089#1093#1086#1076#1085#1099#1077
    TabOrder = 0
    object GroupBox2: TGroupBox
      Left = 8
      Top = 24
      Width = 281
      Height = 433
      Caption = #1055#1077#1088#1089#1086#1085#1072#1083#1100#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label1: TLabel
        Left = 40
        Top = 32
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Label2: TLabel
        Left = 64
        Top = 64
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label3: TLabel
        Left = 40
        Top = 96
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label4: TLabel
        Left = 64
        Top = 128
        Width = 23
        Height = 13
        Caption = #1055#1086#1083':'
      end
      object Label5: TLabel
        Left = 8
        Top = 160
        Width = 82
        Height = 13
        Caption = #1044#1072#1090#1072' '#1088#1086#1078#1076#1077#1085#1080#1103':'
      end
      object Edit1: TEdit
        Left = 96
        Top = 24
        Width = 125
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit2: TEdit
        Left = 96
        Top = 56
        Width = 125
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit3: TEdit
        Left = 96
        Top = 88
        Width = 125
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object GroupBox3: TGroupBox
        Left = 8
        Top = 200
        Width = 225
        Height = 121
        Caption = #1052#1077#1089#1090#1086' '#1078#1080#1090#1077#1083#1100#1089#1090#1074#1072
        TabOrder = 3
        object RichEdit1: TRichEdit
          Left = 16
          Top = 24
          Width = 193
          Height = 81
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 344
        Width = 265
        Height = 81
        Caption = #1050#1086#1085#1090#1072#1082#1090#1099
        TabOrder = 4
        object Label10: TLabel
          Left = 24
          Top = 24
          Width = 91
          Height = 13
          Caption = #1056#1072#1073#1086#1095#1080#1081' '#1090#1077#1083#1077#1092#1086#1085':'
        end
        object Label11: TLabel
          Left = 16
          Top = 56
          Width = 104
          Height = 13
          Caption = #1044#1086#1084#1072#1096#1085#1080#1081' '#1090#1077#1083#1077#1092#1086#1085':'
        end
        object Edit10: TEdit
          Left = 124
          Top = 20
          Width = 125
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
        object Edit25: TEdit
          Left = 124
          Top = 52
          Width = 125
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
      end
      object Edit8: TEdit
        Left = 96
        Top = 120
        Width = 125
        Height = 21
        ReadOnly = True
        TabOrder = 5
      end
      object Edit9: TEdit
        Left = 96
        Top = 152
        Width = 125
        Height = 21
        ReadOnly = True
        TabOrder = 6
      end
    end
    object GroupBox5: TGroupBox
      Left = 296
      Top = 24
      Width = 257
      Height = 281
      Caption = #1055#1072#1089#1087#1086#1088#1090#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      TabOrder = 1
      object Label16: TLabel
        Left = 88
        Top = 108
        Width = 75
        Height = 13
        Caption = #1052#1077#1089#1090#1086' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label17: TLabel
        Left = 24
        Top = 84
        Width = 69
        Height = 13
        Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080':'
      end
      object Label18: TLabel
        Left = 13
        Top = 20
        Width = 84
        Height = 13
        Caption = #1057#1077#1088#1080#1103' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object Label19: TLabel
        Left = 8
        Top = 52
        Width = 87
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072':'
      end
      object MaskEdit1: TMaskEdit
        Left = 104
        Top = 16
        Width = 120
        Height = 21
        EditMask = '9999;1;_'
        MaxLength = 4
        ReadOnly = True
        TabOrder = 0
        Text = '    '
      end
      object MaskEdit2: TMaskEdit
        Left = 104
        Top = 48
        Width = 120
        Height = 21
        EditMask = '999999;1;_'
        MaxLength = 6
        ReadOnly = True
        TabOrder = 1
        Text = '      '
      end
      object GroupBox7: TGroupBox
        Left = 16
        Top = 152
        Width = 225
        Height = 121
        Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
        TabOrder = 2
        object RichEdit2: TRichEdit
          Left = 16
          Top = 24
          Width = 193
          Height = 81
          ReadOnly = True
          TabOrder = 0
        end
      end
      object Edit30: TEdit
        Left = 16
        Top = 128
        Width = 225
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object Edit31: TEdit
        Left = 104
        Top = 80
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 4
      end
    end
    object GroupBox8: TGroupBox
      Left = 296
      Top = 312
      Width = 257
      Height = 145
      TabOrder = 2
      object Label24: TLabel
        Left = 20
        Top = 20
        Width = 61
        Height = 13
        Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100':'
      end
      object GroupBox9: TGroupBox
        Left = 0
        Top = 56
        Width = 257
        Height = 89
        Caption = #1054#1090#1076#1077#1083
        TabOrder = 0
        object Label26: TLabel
          Left = 8
          Top = 52
          Width = 74
          Height = 13
          Caption = #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100':'
        end
        object Label25: TLabel
          Left = 24
          Top = 24
          Width = 53
          Height = 13
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077':'
        end
        object Edit33: TEdit
          Left = 84
          Top = 20
          Width = 149
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
        object Edit34: TEdit
          Left = 84
          Top = 52
          Width = 149
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
      end
      object Edit32: TEdit
        Left = 84
        Top = 12
        Width = 149
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
    end
  end
  object GroupBox10: TGroupBox
    Left = 584
    Top = 0
    Width = 377
    Height = 473
    Caption = #1048#1079#1084#1077#1085#1077#1085#1085#1099#1077
    TabOrder = 1
    object GroupBox6: TGroupBox
      Left = 24
      Top = 27
      Width = 329
      Height = 438
      Caption = #1055#1077#1088#1089#1086#1085#1072#1083#1100#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label36: TLabel
        Left = 88
        Top = 32
        Width = 52
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103':'
      end
      object Label37: TLabel
        Left = 112
        Top = 64
        Width = 25
        Height = 13
        Caption = #1048#1084#1103':'
      end
      object Label38: TLabel
        Left = 88
        Top = 96
        Width = 50
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086':'
      end
      object Label39: TLabel
        Left = 112
        Top = 128
        Width = 23
        Height = 13
        Caption = #1055#1086#1083':'
      end
      object Label40: TLabel
        Left = 56
        Top = 160
        Width = 82
        Height = 13
        Caption = #1044#1072#1090#1072' '#1088#1086#1078#1076#1077#1085#1080#1103':'
      end
      object Edit11: TEdit
        Left = 168
        Top = 24
        Width = 125
        Height = 21
        TabOrder = 0
      end
      object Edit12: TEdit
        Left = 168
        Top = 56
        Width = 125
        Height = 21
        TabOrder = 1
      end
      object Edit13: TEdit
        Left = 168
        Top = 88
        Width = 125
        Height = 21
        TabOrder = 2
      end
      object ComboBox3: TComboBox
        Left = 168
        Top = 120
        Width = 125
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 3
        Items.Strings = (
          #1052#1091#1078'.'
          #1046#1077#1085'.')
      end
      object DateTimePicker1: TDateTimePicker
        Left = 168
        Top = 152
        Width = 125
        Height = 21
        Date = 42104.553231273140000000
        Time = 42104.553231273140000000
        TabOrder = 4
      end
      object GroupBox11: TGroupBox
        Left = 32
        Top = 192
        Width = 265
        Height = 121
        Caption = #1052#1077#1089#1090#1086' '#1078#1080#1090#1077#1083#1100#1089#1090#1074#1072
        TabOrder = 5
        object RichEdit3: TRichEdit
          Left = 32
          Top = 24
          Width = 201
          Height = 81
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
      end
      object GroupBox12: TGroupBox
        Left = 32
        Top = 320
        Width = 265
        Height = 81
        Caption = #1050#1086#1085#1090#1072#1082#1090#1099
        TabOrder = 6
        object Label12: TLabel
          Left = 24
          Top = 24
          Width = 91
          Height = 13
          Caption = #1056#1072#1073#1086#1095#1080#1081' '#1090#1077#1083#1077#1092#1086#1085':'
        end
        object Label13: TLabel
          Left = 16
          Top = 56
          Width = 104
          Height = 13
          Caption = #1044#1086#1084#1072#1096#1085#1080#1081' '#1090#1077#1083#1077#1092#1086#1085':'
        end
        object MaskEdit6: TMaskEdit
          Left = 128
          Top = 16
          Width = 123
          Height = 21
          EditMask = '+7(999)999-99-99;1;_'
          MaxLength = 16
          TabOrder = 0
          Text = '+7(   )   -  -  '
        end
        object MaskEdit7: TMaskEdit
          Left = 128
          Top = 48
          Width = 123
          Height = 21
          EditMask = '+7(999)999-99-99;1;_'
          MaxLength = 16
          TabOrder = 1
          Text = '+7(   )   -  -  '
        end
      end
      object BitBtn7: TBitBtn
        Left = 224
        Top = 408
        Width = 91
        Height = 25
        Caption = #1044#1072#1083#1077#1077
        TabOrder = 7
        OnClick = BitBtn7Click
      end
    end
    object GroupBox13: TGroupBox
      Left = 24
      Top = 26
      Width = 353
      Height = 431
      Caption = #1055#1072#1089#1087#1086#1088#1090#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Visible = False
      object GroupBox15: TGroupBox
        Left = 24
        Top = 40
        Width = 305
        Height = 305
        TabOrder = 0
        object Label45: TLabel
          Left = 104
          Top = 108
          Width = 75
          Height = 13
          Caption = #1052#1077#1089#1090#1086' '#1074#1099#1076#1072#1095#1080':'
        end
        object Label46: TLabel
          Left = 40
          Top = 84
          Width = 69
          Height = 13
          Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080':'
        end
        object Label47: TLabel
          Left = 29
          Top = 20
          Width = 84
          Height = 13
          Caption = #1057#1077#1088#1080#1103' '#1087#1072#1089#1087#1086#1088#1090#1072':'
        end
        object Label54: TLabel
          Left = 24
          Top = 52
          Width = 87
          Height = 13
          Caption = #1053#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072':'
        end
        object MaskEdit8: TMaskEdit
          Left = 136
          Top = 16
          Width = 120
          Height = 21
          EditMask = '9999;1;_'
          MaxLength = 4
          TabOrder = 0
          Text = '    '
        end
        object MaskEdit9: TMaskEdit
          Left = 136
          Top = 48
          Width = 120
          Height = 21
          EditMask = '999999;1;_'
          MaxLength = 6
          TabOrder = 1
          Text = '      '
        end
        object DateTimePicker4: TDateTimePicker
          Left = 136
          Top = 80
          Width = 125
          Height = 21
          Date = 42104.899212222220000000
          Time = 42104.899212222220000000
          TabOrder = 2
        end
        object GroupBox14: TGroupBox
          Left = 8
          Top = 168
          Width = 289
          Height = 121
          Caption = #1040#1076#1088#1077#1089' '#1088#1077#1075#1080#1089#1090#1088#1072#1094#1080#1080
          TabOrder = 3
          object RichEdit4: TRichEdit
            Left = 40
            Top = 24
            Width = 217
            Height = 81
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
        object Edit22: TEdit
          Left = 48
          Top = 128
          Width = 209
          Height = 21
          TabOrder = 4
        end
      end
      object BitBtn11: TBitBtn
        Left = 240
        Top = 390
        Width = 91
        Height = 33
        Caption = #1044#1072#1083#1077#1077
        TabOrder = 1
        OnClick = BitBtn11Click
      end
      object BitBtn12: TBitBtn
        Left = 24
        Top = 390
        Width = 91
        Height = 33
        Caption = #1053#1072#1079#1072#1076
        TabOrder = 2
        OnClick = BitBtn12Click
      end
    end
  end
  object GroupBox16: TGroupBox
    Left = 600
    Top = 22
    Width = 353
    Height = 433
    Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100' '#1080' '#1086#1090#1076#1077#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = 11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Label44: TLabel
      Left = 56
      Top = 48
      Width = 61
      Height = 13
      Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100':'
    end
    object BitBtn13: TBitBtn
      Left = 216
      Top = 390
      Width = 123
      Height = 33
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
      TabOrder = 0
      OnClick = BitBtn13Click
    end
    object BitBtn14: TBitBtn
      Left = 16
      Top = 390
      Width = 91
      Height = 33
      Caption = #1053#1072#1079#1072#1076
      TabOrder = 1
      OnClick = BitBtn14Click
    end
    object DBLookupComboBox2: TDBLookupComboBox
      Left = 136
      Top = 40
      Width = 145
      Height = 21
      KeyField = 'ID_DOLZH'
      ListField = 'NAIMEN'
      ListSource = DMod.DS_Dolzhn
      TabOrder = 2
      OnEnter = DBLookupComboBox2Enter
    end
    object GroupBox17: TGroupBox
      Left = 32
      Top = 96
      Width = 289
      Height = 121
      Caption = #1054#1090#1076#1077#1083
      TabOrder = 3
      object Label48: TLabel
        Left = 48
        Top = 32
        Width = 53
        Height = 13
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077':'
      end
      object Label49: TLabel
        Left = 32
        Top = 64
        Width = 74
        Height = 13
        Caption = #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100':'
      end
      object Edit23: TEdit
        Left = 120
        Top = 24
        Width = 161
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 0
      end
      object Edit24: TEdit
        Left = 120
        Top = 56
        Width = 161
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 1
      end
      object BitBtn15: TBitBtn
        Left = 112
        Top = 88
        Width = 75
        Height = 25
        Caption = #1042#1099#1073#1088#1072#1090#1100
        TabOrder = 2
      end
    end
  end
end
