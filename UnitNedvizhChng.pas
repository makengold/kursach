unit UnitNedvizhChng;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids;

type
  TChangeNedvizh = class(TForm)
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ChangeNedvizh: TChangeNedvizh;

implementation

uses DM, Main, UnitSdelkaUpd;

{$R *.dfm}

procedure TChangeNedvizh.BitBtn2Click(Sender: TObject);
begin
ChangeNedvizh.Close;
end;

procedure TChangeNedvizh.BitBtn1Click(Sender: TObject);
begin
  if BitBtn1.Tag=0 then
  begin
  MainForm.RichEdit1.Lines.Text:='�.'+DMod.Q_NedvizhChangeGOROD.AsString+', ����� '+DMod.Q_NedvizhChangeRAYON.AsString+', ��. '+DMod.Q_NedvizhChangeULITCA.AsString+', ��� '+DMod.Q_NedvizhChangeDOM.AsString+',��. '+DMod.Q_NedvizhChangeNOM_KVARTIRI.AsString;
  MainForm.Edit3.Text:=DMod.Q_NedvizhChangeCENA.AsString+' ���.';
  end
  else
  begin
  SdelkaUpd.RichEdit2.Lines.Text:='�.'+DMod.Q_NedvizhChangeGOROD.AsString+', ����� '+DMod.Q_NedvizhChangeRAYON.AsString+', ��. '+DMod.Q_NedvizhChangeULITCA.AsString+', ��� '+DMod.Q_NedvizhChangeDOM.AsString+',��. '+DMod.Q_NedvizhChangeNOM_KVARTIRI.AsString;
  SdelkaUpd.Edit5.Text:=DMod.Q_NedvizhChangeCENA.AsString+' ���.';
  end;
  ChangeNedvizh.Close;
end;

end.
