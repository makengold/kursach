unit AddProdavec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, Mask;

type
  TProdavecAdd = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    MaskEdit7: TMaskEdit;
    Label47: TLabel;
    MaskEdit8: TMaskEdit;
    Label54: TLabel;
    MaskEdit9: TMaskEdit;
    Label46: TLabel;
    DateTimePicker4: TDateTimePicker;
    Label45: TLabel;
    Edit22: TEdit;
    GroupBox14: TGroupBox;
    Label15: TLabel;
    Label33: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Edit18: TEdit;
    Edit19: TEdit;
    Edit20: TEdit;
    Edit21: TEdit;
    BitBtn5: TBitBtn;
    BitBtn3: TBitBtn;
    GroupBox7: TGroupBox;
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProdavecAdd: TProdavecAdd;

implementation

uses DM;

{$R *.dfm}

procedure TProdavecAdd.BitBtn3Click(Sender: TObject);
var
i:integer;
begin
  if (Edit1.Text<>'') and (Edit2.Text<>'') and (Edit3.Text<>'') and (MaskEdit8.Text<>'') and (MaskEdit9.Text<>'') and (Edit22.Text<>'') and (Edit18.Text<>'') and (Edit19.Text<>'') and (Edit20.Text<>'') and (Edit21.Text<>'')
  then begin
  DMod.Prodavec_Add.ParamByName('FAMILY').AsString:=Edit1.Text;
  DMod.Prodavec_Add.ParamByName('IMYA').AsString:=Edit2.Text;
  DMod.Prodavec_Add.ParamByName('OTCHESTVO').AsString:=Edit3.Text;
  DMod.Prodavec_Add.ParamByName('TEL').AsString:=MaskEdit7.Text;
  DMod.Prodavec_Add.ParamByName('SER_PASPORT').AsString:=MaskEdit8.Text;
  DMod.Prodavec_Add.ParamByName('NOM_PASPORT').AsString:=MaskEdit9.Text;
  DMod.Prodavec_Add.ParamByName('DATA_VIDACH_PASPORT').AsString:=DateToStr(DateTimePicker4.Date);
  DMod.Prodavec_Add.ParamByName('KEM_VIDAN_PASPORT').AsString:=Edit22.Text;
  DMod.Prodavec_Add.ParamByName('ADRES_REG').AsString:='�.'+Edit18.Text+', ��.'+Edit19.Text+', ��� '+Edit20.Text+', ��.'+Edit21.Text;
  DMod.Prodavec_Add.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Prodavec.Close;
  DMod.Q_Prodavec.Open;
  MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
  for i:=0 to GroupBox7.ControlCount-1 do
  if GroupBox7.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox7.Controls[i]).Clear;
  for i:=0 to GroupBox7.ControlCount-1 do
  if GroupBox7.Controls[i].ClassName = 'TMaskEdit' then TMaskEdit(GroupBox7.Controls[i]).Clear;
  for i:=0 to GroupBox14.ControlCount-1 do
  if GroupBox14.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox14.Controls[i]).Clear;
  DateTimePicker4.Date:=now;
  ProdavecAdd.Close;
end;

procedure TProdavecAdd.BitBtn5Click(Sender: TObject);
var
i:integer;
begin
    for i:=0 to GroupBox7.ControlCount-1 do
      if GroupBox7.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox7.Controls[i]).Clear;
    for i:=0 to GroupBox7.ControlCount-1 do
      if GroupBox7.Controls[i].ClassName = 'TMaskEdit' then TMaskEdit(GroupBox7.Controls[i]).Clear;
    for i:=0 to GroupBox14.ControlCount-1 do
      if GroupBox14.Controls[i].ClassName = 'TEdit' then TEdit(GroupBox14.Controls[i]).Clear;
    DateTimePicker4.Date:=now;
    ProdavecAdd.Close;
end;

end.
