unit UnitOtdel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids;

type
  TFormOtdel = class(TForm)
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    Edit23: TEdit;
    Label48: TLabel;
    Label49: TLabel;
    Edit24: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BitBtn5Click(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
  private
    { Private declarations }
  public
    { Public declarations }
  sort:boolean;
  end;

var
  FormOtdel: TFormOtdel;

implementation

uses AddOtdel, Main, DM, UpdOtdel;

{$R *.dfm}

procedure TFormOtdel.BitBtn1Click(Sender: TObject);
begin
  FormOtdel.Close;
  Edit23.Text:='';
  Edit24.Text:='';
end;

procedure TFormOtdel.BitBtn2Click(Sender: TObject);
begin
  FormOtdel.Close;
  MainForm.Edit23.Text:=Edit23.Text;
  MainForm.Edit24.Text:=Edit24.Text;
  Edit23.Clear;
  Edit24.Clear;
end;

procedure TFormOtdel.BitBtn3Click(Sender: TObject);
begin
  OtdelAdd.ShowModal;
end;

procedure TFormOtdel.BitBtn4Click(Sender: TObject);
begin
        case MessageBox(Handle,'�� �������, ��� ������ ������� ����?','��������', MB_YESNO or mb_iconquestion) of
    IDYES: begin
    DMod.Otdel_Del.ParamByName('ID_OTDELA').AsInteger:=DMod.Q_OtdelID_OTDELA.AsInteger;
    try
    DMod.Otdel_Del.ExecProc;
    MessageBox(Self.Handle, PChar('������ �������'), PChar('������'), MB_OK+MB_ICONINFORMATION);
    except
    MessageBox(Self.Handle, PChar('�������� ����������, ��� ��� ������ ������������ � ������ �������'), PChar(''), MB_OK+MB_ICONWARNING);
    end;
    end;
    IDNO: Exit;
    end;
    DMod.Q_Otdel.Close;
    DMod.Q_Otdel.Open;
end;

procedure TFormOtdel.DBGrid1CellClick(Column: TColumn);
begin
 Edit23.Text:=DMod.Q_OtdelNAIMEN.AsString;
 Edit24.Text:=DMod.Q_OtdelRUKOVOD.AsString;
end;

procedure TFormOtdel.BitBtn5Click(Sender: TObject);
begin
  if (Edit23.Text<>'') and (Edit24.Text<>'') then
  begin
  OtdelUpd.Edit1.Text:=Edit23.Text;
  OtdelUpd.ShowModal;
  end
  else
  MessageBox(Self.Handle, PChar('�������� ���� ��� ���������'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
end;

procedure TFormOtdel.DBGrid1TitleClick(Column: TColumn);
begin
  if sort=true then
  begin
  DMod.Q_Otdel.Close;
  DMod.Q_Otdel.SQL.Clear;
  DMod.Q_Otdel.SQL.Text:='Select * FROM otdel order by NAIMEN ASC ';
  DMod.Q_Otdel.Close;
  DMod.Q_Otdel.Open;
  sort:=false;
  end
  else
  begin
  DMod.Q_Otdel.Close;
  DMod.Q_Otdel.SQL.Clear;
  DMod.Q_Otdel.SQL.Text:='Select * FROM otdel order by NAIMEN DESC';
  DMod.Q_Otdel.Close;
  DMod.Q_Otdel.Open;
  sort:=true;
  end;
end;

end.
