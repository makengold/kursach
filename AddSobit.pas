unit AddSobit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DBCtrls, ComCtrls;

type
  TSobitAdd = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    RichEdit1: TRichEdit;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    DateTimePicker1: TDateTimePicker;
    DBLookupComboBox1: TDBLookupComboBox;
    BitBtn4: TBitBtn;
    BitBtn1: TBitBtn;
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure DBLookupComboBox1Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SobitAdd: TSobitAdd;

implementation

uses DM, Main, UnitRemAdd, UnitSobitie;

{$R *.dfm}

procedure TSobitAdd.BitBtn4Click(Sender: TObject);
begin
  ComboBox1.Text:='';
  ComboBox2.Text:='';
  DateTimePicker1.Date:=Now;
  DBLookupComboBox1.KeyValue:=null;
  RichEdit1.Clear;
  SobitAdd.Close;
end;

procedure TSobitAdd.BitBtn1Click(Sender: TObject);
begin
  if (ComboBox1.Text<>'') and (ComboBox2.Text<>'') and (RichEdit1.Lines.Text<>'') and (DBLookupComboBox1.KeyValue<>null) then
  begin
  DMod.Sobit_Add.ParamByName('ID_SOTRUD').AsInteger:=DMod.Q_SotrudID_SOTRUD.AsInteger;
  DMod.Sobit_Add.ParamByName('NAIMEN').AsString:=ComboBox1.Text;
  DMod.Sobit_Add.ParamByName('TIP_DOCUM').AsString:=ComboBox2.Text;
  DMod.Sobit_Add.ParamByName('SODERJ_DOCUM').AsString:=RichEdit1.Lines.Text;
  DMod.Sobit_Add.ParamByName('DATA_SOBIT').AsString:=DateToStr(DateTimePicker1.Date);
  DMod.Sobit_Add.ParamByName('OTVETSTV_LICO').AsString:=DBLookupComboBox1.Text;
  DMod.Sobit_Add.ParamByName('ID_DOLZH').AsInteger:=DMod.Q_DolzhnID_DOLZH.AsInteger;
  DMod.Sobit_Add.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Vibor.Close;
  DMod.Q_Vibor.Open;
  MessageBox(Self.Handle, PChar('������ ������� ���������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  ComboBox1.ItemIndex:=-1;
  ComboBox2.ItemIndex:=-1;
  DateTimePicker1.Date:=Now;
  DBLookupComboBox1.KeyValue:=null;
  RichEdit1.Clear;
  SobitAdd.Close;
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
end;

procedure TSobitAdd.ComboBox1Change(Sender: TObject);
begin
  if (ComboBox1.ItemIndex=0) or (ComboBox1.ItemIndex=2) or (ComboBox1.ItemIndex=4) then
  ComboBox2.Items.Delete(1);
  if (ComboBox1.ItemIndex=1) or (ComboBox1.ItemIndex=3) then
  begin
  if ComboBox2.Items.Count<2 then
  ComboBox2.Items.Add('���������');
  end;
end;

procedure TSobitAdd.ComboBox2Change(Sender: TObject);
begin
  RichEdit1.Clear;
  if (ComboBox1.ItemIndex=0) and (ComboBox2.ItemIndex=0) then
  RichEdit1.Lines.Add('������ � �������� �� ������ ����������, '+FormSobitie.Edit5.Text+' '+FormSobitie.Edit6.Text+' '+FormSobitie.Edit7.Text+', �� ��������� '+FormSobitie.Edit8.Text);
  if (ComboBox1.ItemIndex=1) and (ComboBox2.ItemIndex=0) then
  RichEdit1.Lines.Add('������ �� ���������� ����������, '+FormSobitie.Edit5.Text+' '+FormSobitie.Edit6.Text+' '+FormSobitie.Edit7.Text+', � ��������� '+FormSobitie.Edit8.Text);
  if (ComboBox1.ItemIndex=1) and (ComboBox2.ItemIndex=1) then
  RichEdit1.Lines.Add('��������� � ���������� ����������, '+FormSobitie.Edit5.Text+' '+FormSobitie.Edit6.Text+' '+FormSobitie.Edit7.Text+', � ��������� '+FormSobitie.Edit8.Text+' �� ������������ �������');
end;

procedure TSobitAdd.DBLookupComboBox1Enter(Sender: TObject);
begin
SobitAdd.DBLookupComboBox1.ListSource.DataSet.Last;
end;

end.
