object NedvizhEdit: TNedvizhEdit
  Left = 115
  Top = 101
  BorderStyle = bsDialog
  Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1077
  ClientHeight = 439
  ClientWidth = 1217
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 609
    Height = 441
    Caption = #1048#1089#1093#1086#1076#1085#1099#1077
    TabOrder = 0
    object GroupBox2: TGroupBox
      Left = 8
      Top = 24
      Width = 273
      Height = 145
      Caption = #1040#1076#1088#1077#1089
      TabOrder = 0
      object Label1: TLabel
        Left = 40
        Top = 24
        Width = 33
        Height = 13
        Caption = #1043#1086#1088#1086#1076':'
      end
      object Label2: TLabel
        Left = 40
        Top = 48
        Width = 34
        Height = 13
        Caption = #1056#1072#1081#1086#1085':'
      end
      object Label3: TLabel
        Left = 40
        Top = 72
        Width = 35
        Height = 13
        Caption = #1059#1083#1080#1094#1072':'
      end
      object Label4: TLabel
        Left = 48
        Top = 96
        Width = 26
        Height = 13
        Caption = #1044#1086#1084':'
      end
      object Edit2: TEdit
        Left = 88
        Top = 24
        Width = 177
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit3: TEdit
        Left = 88
        Top = 48
        Width = 177
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit4: TEdit
        Left = 88
        Top = 72
        Width = 177
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object Edit11: TEdit
        Left = 88
        Top = 96
        Width = 177
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
    end
    object GroupBox5: TGroupBox
      Left = 8
      Top = 184
      Width = 273
      Height = 97
      Caption = #1056#1077#1084#1086#1085#1090
      TabOrder = 1
      object Label6: TLabel
        Left = 24
        Top = 24
        Width = 57
        Height = 13
        Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077':'
      end
      object Label11: TLabel
        Left = 48
        Top = 48
        Width = 29
        Height = 13
        Caption = #1044#1072#1090#1072':'
        Visible = False
      end
      object Label16: TLabel
        Left = 16
        Top = 72
        Width = 68
        Height = 13
        Caption = #1058#1080#1087' '#1088#1077#1084#1086#1085#1090#1072':'
        Visible = False
      end
      object Edit9: TEdit
        Left = 96
        Top = 48
        Width = 161
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object Edit10: TEdit
        Left = 96
        Top = 72
        Width = 161
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object Edit1: TEdit
        Left = 96
        Top = 20
        Width = 161
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
    end
    object GroupBox3: TGroupBox
      Left = 288
      Top = 8
      Width = 305
      Height = 393
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object Label7: TLabel
        Left = 8
        Top = 40
        Width = 100
        Height = 13
        Caption = #1042#1080#1076' '#1085#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1080':'
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 80
        Width = 289
        Height = 225
        Enabled = False
        TabOrder = 0
        Visible = False
        object Label8: TLabel
          Left = 64
          Top = 48
          Width = 86
          Height = 13
          Caption = #1054#1073#1097#1072#1103' '#1087#1083#1086#1097#1072#1076#1100':'
        end
        object Label9: TLabel
          Left = 8
          Top = 88
          Width = 150
          Height = 13
          Caption = #1055#1083#1086#1097#1072#1076#1100' '#1072#1076#1084#1080#1085'. '#1087#1086#1084#1077#1097#1077#1085#1080#1081':'
        end
        object Label10: TLabel
          Left = 24
          Top = 128
          Width = 127
          Height = 13
          Caption = #1057#1088#1086#1082' '#1101#1082#1089#1087#1083#1091#1072#1090#1072#1094#1080#1080' ('#1083#1077#1090'):'
        end
        object Label12: TLabel
          Left = 120
          Top = 168
          Width = 29
          Height = 13
          Caption = #1062#1077#1085#1072':'
        end
        object MaskEdit6: TMaskEdit
          Left = 160
          Top = 40
          Width = 121
          Height = 21
          EditMask = '9999'#1082#1074'.'#1084'.;1;_'
          MaxLength = 9
          ReadOnly = True
          TabOrder = 0
          Text = '    '#1082#1074'.'#1084'.'
        end
        object MaskEdit7: TMaskEdit
          Left = 160
          Top = 80
          Width = 120
          Height = 21
          EditMask = '9999'#1082#1074'.'#1084'.;1;_'
          MaxLength = 9
          ReadOnly = True
          TabOrder = 1
          Text = '    '#1082#1074'.'#1084'.'
        end
        object Edit17: TEdit
          Left = 160
          Top = 120
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
        object Edit18: TEdit
          Left = 160
          Top = 160
          Width = 121
          Height = 21
          ReadOnly = True
          TabOrder = 3
        end
      end
      object GroupBox6: TGroupBox
        Left = 24
        Top = 80
        Width = 265
        Height = 233
        TabOrder = 1
        Visible = False
        object Label13: TLabel
          Left = 48
          Top = 32
          Width = 86
          Height = 13
          Caption = #1054#1073#1097#1072#1103' '#1087#1083#1086#1097#1072#1076#1100':'
        end
        object Label14: TLabel
          Left = 88
          Top = 64
          Width = 41
          Height = 13
          Caption = #1069#1090#1072#1078#1077#1081':'
        end
        object Label15: TLabel
          Left = 88
          Top = 96
          Width = 46
          Height = 13
          Caption = #1055#1072#1088#1082#1080#1085#1075':'
        end
        object Label17: TLabel
          Left = 8
          Top = 128
          Width = 124
          Height = 13
          Caption = #1057#1088#1086#1082' '#1101#1082#1089#1087#1083#1091#1072#1090#1072#1094#1080#1080'('#1083#1077#1090'):'
        end
        object Label33: TLabel
          Left = 104
          Top = 160
          Width = 29
          Height = 13
          Caption = #1062#1077#1085#1072':'
        end
        object MaskEdit8: TMaskEdit
          Left = 136
          Top = 24
          Width = 125
          Height = 21
          EditMask = '9999'#1082#1074'.'#1084'.;1;_'
          MaxLength = 9
          ReadOnly = True
          TabOrder = 0
          Text = '    '#1082#1074'.'#1084'.'
        end
        object Edit7: TEdit
          Left = 136
          Top = 88
          Width = 125
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object Edit8: TEdit
          Left = 136
          Top = 56
          Width = 125
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
        object Edit12: TEdit
          Left = 136
          Top = 120
          Width = 125
          Height = 21
          ReadOnly = True
          TabOrder = 3
        end
        object Edit13: TEdit
          Left = 136
          Top = 152
          Width = 125
          Height = 21
          ReadOnly = True
          TabOrder = 4
        end
      end
      object GroupBox11: TGroupBox
        Left = 24
        Top = 72
        Width = 257
        Height = 257
        TabOrder = 2
        Visible = False
        object Label36: TLabel
          Left = 24
          Top = 32
          Width = 86
          Height = 13
          Caption = #1054#1073#1097#1072#1103' '#1087#1083#1086#1097#1072#1076#1100':'
        end
        object Label37: TLabel
          Left = 80
          Top = 64
          Width = 29
          Height = 13
          Caption = #1069#1090#1072#1078':'
        end
        object Label38: TLabel
          Left = 64
          Top = 96
          Width = 46
          Height = 13
          Caption = #1055#1072#1088#1082#1080#1085#1075':'
        end
        object Label39: TLabel
          Left = 24
          Top = 128
          Width = 89
          Height = 13
          Caption = #1053#1086#1084#1077#1088' '#1082#1074#1072#1088#1090#1080#1088#1099':'
        end
        object Label40: TLabel
          Left = 24
          Top = 160
          Width = 92
          Height = 13
          Caption = #1055#1086#1095#1090#1086#1074#1099#1081' '#1080#1085#1076#1077#1082#1089':'
        end
        object Label41: TLabel
          Left = 80
          Top = 192
          Width = 29
          Height = 13
          Caption = #1062#1077#1085#1072':'
        end
        object MaskEdit9: TMaskEdit
          Left = 120
          Top = 152
          Width = 125
          Height = 21
          EditMask = '999999;1;_'
          MaxLength = 6
          ReadOnly = True
          TabOrder = 0
          Text = '      '
        end
        object MaskEdit10: TMaskEdit
          Left = 120
          Top = 24
          Width = 125
          Height = 21
          EditMask = '9999'#1082#1074'.'#1084'.;1;_'
          MaxLength = 9
          ReadOnly = True
          TabOrder = 1
          Text = '    '#1082#1074'.'#1084'.'
        end
        object Edit6: TEdit
          Left = 120
          Top = 88
          Width = 125
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
        object Edit14: TEdit
          Left = 120
          Top = 56
          Width = 125
          Height = 21
          ReadOnly = True
          TabOrder = 3
        end
        object Edit15: TEdit
          Left = 120
          Top = 120
          Width = 125
          Height = 21
          ReadOnly = True
          TabOrder = 4
        end
        object Edit16: TEdit
          Left = 120
          Top = 184
          Width = 125
          Height = 21
          ReadOnly = True
          TabOrder = 5
        end
      end
      object Edit5: TEdit
        Left = 120
        Top = 32
        Width = 137
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
    end
  end
  object GroupBox7: TGroupBox
    Left = 608
    Top = 0
    Width = 609
    Height = 447
    Caption = #1048#1079#1084#1077#1085#1077#1085#1085#1099#1077
    TabOrder = 1
    object GroupBox8: TGroupBox
      Left = 8
      Top = 32
      Width = 273
      Height = 169
      Caption = #1040#1076#1088#1077#1089
      TabOrder = 0
      object Label5: TLabel
        Left = 40
        Top = 24
        Width = 33
        Height = 13
        Caption = #1043#1086#1088#1086#1076':'
      end
      object Label18: TLabel
        Left = 40
        Top = 48
        Width = 34
        Height = 13
        Caption = #1056#1072#1081#1086#1085':'
      end
      object Label19: TLabel
        Left = 40
        Top = 72
        Width = 35
        Height = 13
        Caption = #1059#1083#1080#1094#1072':'
      end
      object Label20: TLabel
        Left = 48
        Top = 96
        Width = 26
        Height = 13
        Caption = #1044#1086#1084':'
      end
      object Edit19: TEdit
        Left = 88
        Top = 24
        Width = 177
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 0
      end
      object Edit20: TEdit
        Left = 88
        Top = 48
        Width = 177
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 1
      end
      object Edit21: TEdit
        Left = 88
        Top = 72
        Width = 177
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 2
      end
      object Edit22: TEdit
        Left = 88
        Top = 96
        Width = 177
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 3
      end
      object BitBtn10: TBitBtn
        Left = 104
        Top = 128
        Width = 75
        Height = 25
        Caption = #1042#1099#1073#1088#1072#1090#1100
        TabOrder = 4
        OnClick = BitBtn10Click
      end
    end
    object GroupBox9: TGroupBox
      Left = 8
      Top = 208
      Width = 273
      Height = 97
      Caption = #1056#1077#1084#1086#1085#1090
      TabOrder = 1
      object Label21: TLabel
        Left = 24
        Top = 24
        Width = 57
        Height = 13
        Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077':'
      end
      object Label22: TLabel
        Left = 48
        Top = 48
        Width = 29
        Height = 13
        Caption = #1044#1072#1090#1072':'
        Visible = False
      end
      object Label23: TLabel
        Left = 16
        Top = 72
        Width = 68
        Height = 13
        Caption = #1058#1080#1087' '#1088#1077#1084#1086#1085#1090#1072':'
        Visible = False
      end
      object ComboBox2: TComboBox
        Left = 96
        Top = 16
        Width = 121
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 0
        Text = #1054#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
        OnChange = ComboBox2Change
        Items.Strings = (
          #1054#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
          #1055#1088#1086#1074#1086#1076#1080#1083#1089#1103)
      end
      object Edit23: TEdit
        Left = 96
        Top = 48
        Width = 121
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 1
        Visible = False
      end
      object Edit24: TEdit
        Left = 96
        Top = 72
        Width = 161
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 2
        Visible = False
      end
      object BitBtn4: TBitBtn
        Left = 224
        Top = 40
        Width = 41
        Height = 25
        Caption = #1042#1099#1073#1086#1088
        TabOrder = 3
        Visible = False
        OnClick = BitBtn4Click
      end
    end
    object GroupBox10: TGroupBox
      Left = 288
      Top = 32
      Width = 313
      Height = 393
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object Label25: TLabel
        Left = 8
        Top = 40
        Width = 100
        Height = 13
        Caption = #1042#1080#1076' '#1085#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1080':'
      end
      object BitBtn8: TBitBtn
        Left = 184
        Top = 344
        Width = 99
        Height = 33
        Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
        TabOrder = 0
        OnClick = BitBtn8Click
      end
      object GroupBox12: TGroupBox
        Left = 16
        Top = 64
        Width = 289
        Height = 225
        TabOrder = 1
        Visible = False
        object Label30: TLabel
          Left = 64
          Top = 48
          Width = 86
          Height = 13
          Caption = #1054#1073#1097#1072#1103' '#1087#1083#1086#1097#1072#1076#1100':'
        end
        object Label32: TLabel
          Left = 8
          Top = 88
          Width = 150
          Height = 13
          Caption = #1055#1083#1086#1097#1072#1076#1100' '#1072#1076#1084#1080#1085'. '#1087#1086#1084#1077#1097#1077#1085#1080#1081':'
        end
        object Label34: TLabel
          Left = 24
          Top = 128
          Width = 127
          Height = 13
          Caption = #1057#1088#1086#1082' '#1101#1082#1089#1087#1083#1091#1072#1090#1072#1094#1080#1080' ('#1083#1077#1090'):'
        end
        object Label35: TLabel
          Left = 120
          Top = 160
          Width = 29
          Height = 13
          Caption = #1062#1077#1085#1072':'
        end
        object MaskEdit2: TMaskEdit
          Left = 160
          Top = 40
          Width = 121
          Height = 21
          EditMask = '9999'#1082#1074'.'#1084'.;1;_'
          MaxLength = 9
          TabOrder = 0
          Text = '    '#1082#1074'.'#1084'.'
        end
        object MaskEdit4: TMaskEdit
          Left = 160
          Top = 80
          Width = 120
          Height = 21
          EditMask = '9999'#1082#1074'.'#1084'.;1;_'
          MaxLength = 9
          TabOrder = 1
          Text = '    '#1082#1074'.'#1084'.'
        end
        object SpinEdit7: TSpinEdit
          Left = 160
          Top = 120
          Width = 125
          Height = 22
          MaxValue = 1000
          MinValue = 1
          TabOrder = 2
          Value = 1
        end
        object SpinEdit8: TSpinEdit
          Left = 160
          Top = 152
          Width = 125
          Height = 22
          MaxValue = 99999999
          MinValue = 1
          TabOrder = 3
          Value = 1
        end
      end
      object GroupBox13: TGroupBox
        Left = 16
        Top = 64
        Width = 289
        Height = 233
        TabOrder = 2
        Visible = False
        object Label26: TLabel
          Left = 48
          Top = 32
          Width = 86
          Height = 13
          Caption = #1054#1073#1097#1072#1103' '#1087#1083#1086#1097#1072#1076#1100':'
        end
        object Label27: TLabel
          Left = 88
          Top = 64
          Width = 41
          Height = 13
          Caption = #1069#1090#1072#1078#1077#1081':'
        end
        object Label28: TLabel
          Left = 88
          Top = 96
          Width = 46
          Height = 13
          Caption = #1055#1072#1088#1082#1080#1085#1075':'
        end
        object Label29: TLabel
          Left = 8
          Top = 128
          Width = 124
          Height = 13
          Caption = #1057#1088#1086#1082' '#1101#1082#1089#1087#1083#1091#1072#1090#1072#1094#1080#1080'('#1083#1077#1090'):'
        end
        object Label31: TLabel
          Left = 104
          Top = 160
          Width = 29
          Height = 13
          Caption = #1062#1077#1085#1072':'
        end
        object MaskEdit3: TMaskEdit
          Left = 136
          Top = 24
          Width = 125
          Height = 21
          EditMask = '9999'#1082#1074'.'#1084'.;1;_'
          MaxLength = 9
          TabOrder = 0
          Text = '    '#1082#1074'.'#1084'.'
        end
        object ComboBox9: TComboBox
          Left = 136
          Top = 88
          Width = 125
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 1
          Text = #1053#1077#1090
          Items.Strings = (
            #1053#1077#1090
            #1045#1089#1090#1100)
        end
        object SpinEdit4: TSpinEdit
          Left = 136
          Top = 56
          Width = 125
          Height = 22
          MaxValue = 100
          MinValue = 1
          TabOrder = 2
          Value = 1
        end
        object SpinEdit5: TSpinEdit
          Left = 136
          Top = 120
          Width = 125
          Height = 22
          MaxValue = 1000
          MinValue = 1
          TabOrder = 3
          Value = 1
        end
        object SpinEdit6: TSpinEdit
          Left = 136
          Top = 152
          Width = 125
          Height = 22
          MaxValue = 99999999
          MinValue = 1
          TabOrder = 4
          Value = 1
        end
      end
      object GroupBox14: TGroupBox
        Left = 32
        Top = 64
        Width = 257
        Height = 257
        TabOrder = 3
        Visible = False
        object Label42: TLabel
          Left = 24
          Top = 32
          Width = 86
          Height = 13
          Caption = #1054#1073#1097#1072#1103' '#1087#1083#1086#1097#1072#1076#1100':'
        end
        object Label43: TLabel
          Left = 80
          Top = 64
          Width = 29
          Height = 13
          Caption = #1069#1090#1072#1078':'
        end
        object Label44: TLabel
          Left = 64
          Top = 96
          Width = 46
          Height = 13
          Caption = #1055#1072#1088#1082#1080#1085#1075':'
        end
        object Label45: TLabel
          Left = 24
          Top = 128
          Width = 89
          Height = 13
          Caption = #1053#1086#1084#1077#1088' '#1082#1074#1072#1088#1090#1080#1088#1099':'
        end
        object Label46: TLabel
          Left = 24
          Top = 160
          Width = 92
          Height = 13
          Caption = #1055#1086#1095#1090#1086#1074#1099#1081' '#1080#1085#1076#1077#1082#1089':'
        end
        object Label47: TLabel
          Left = 80
          Top = 192
          Width = 29
          Height = 13
          Caption = #1062#1077#1085#1072':'
        end
        object MaskEdit5: TMaskEdit
          Left = 120
          Top = 152
          Width = 123
          Height = 21
          EditMask = '999999;1;_'
          MaxLength = 6
          TabOrder = 0
          Text = '      '
        end
        object MaskEdit1: TMaskEdit
          Left = 120
          Top = 24
          Width = 125
          Height = 21
          EditMask = '9999'#1082#1074'.'#1084'.;1;_'
          MaxLength = 9
          TabOrder = 1
          Text = '    '#1082#1074'.'#1084'.'
        end
        object ComboBox8: TComboBox
          Left = 120
          Top = 88
          Width = 125
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 2
          Text = #1053#1077#1090
          Items.Strings = (
            #1053#1077#1090
            #1045#1089#1090#1100)
        end
        object SpinEdit1: TSpinEdit
          Left = 120
          Top = 56
          Width = 125
          Height = 22
          MaxValue = 100
          MinValue = 1
          TabOrder = 3
          Value = 1
        end
        object SpinEdit2: TSpinEdit
          Left = 120
          Top = 120
          Width = 125
          Height = 22
          MaxValue = 1000
          MinValue = 1
          TabOrder = 4
          Value = 1
        end
        object SpinEdit3: TSpinEdit
          Left = 120
          Top = 184
          Width = 125
          Height = 22
          MaxValue = 999999999
          MinValue = 1
          TabOrder = 5
          Value = 1
        end
      end
      object BitBtn9: TBitBtn
        Left = 24
        Top = 344
        Width = 99
        Height = 33
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 4
        OnClick = BitBtn9Click
      end
      object DBLookupComboBox1: TDBLookupComboBox
        Left = 120
        Top = 32
        Width = 137
        Height = 21
        KeyField = 'ID_VID_NEDVIZH'
        ListField = 'NAIMEN'
        ListSource = DMod.DS_Vid_Nedvizh
        TabOrder = 5
        OnCloseUp = DBLookupComboBox1CloseUp
      end
    end
  end
end
