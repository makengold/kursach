unit UnitSobitUpd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DBCtrls, ComCtrls;

type
  TSobitUpd = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    RichEdit1: TRichEdit;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    DateTimePicker1: TDateTimePicker;
    DBLookupComboBox1: TDBLookupComboBox;
    BitBtn4: TBitBtn;
    BitBtn1: TBitBtn;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    RichEdit2: TRichEdit;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    procedure BitBtn4Click(Sender: TObject);
    procedure DBLookupComboBox1Enter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SobitUpd: TSobitUpd;

implementation

uses DM, UnitAdresUpd, UnitSobitie;

{$R *.dfm}

procedure TSobitUpd.BitBtn4Click(Sender: TObject);
begin
 SobitUpd.Close;
end;

procedure TSobitUpd.DBLookupComboBox1Enter(Sender: TObject);
begin
SobitUpd.DBLookupComboBox1.ListSource.DataSet.Last;
end;

procedure TSobitUpd.FormShow(Sender: TObject);
begin
  ComboBox1.Text:=DMod.Q_ViborNAIMEN.AsString;
  ComboBox2.Text:=Edit3.Text;
  DateTimePicker1.Date:=StrToDate(Edit4.Text);
  RichEdit1.Lines.Text:=RichEdit2.Lines.Text;
end;
procedure TSobitUpd.ComboBox1Change(Sender: TObject);
begin
  if (ComboBox1.ItemIndex=0) or (ComboBox1.ItemIndex=2) or (ComboBox1.ItemIndex=4) then
  ComboBox2.Items.Delete(1);
  if (ComboBox1.ItemIndex=1) or (ComboBox1.ItemIndex=3) then
  begin
  if ComboBox2.Items.Count<2 then
  ComboBox2.Items.Add('���������');
  end;
end;

procedure TSobitUpd.BitBtn1Click(Sender: TObject);
begin
 if (ComboBox1.Text<>'') and (ComboBox2.Text<>'') and (DBLookupComboBox1.KeyValue<>0)
  then begin
  DMod.Sobit_Add.ParamByName('ID_SOTRUD').AsInteger:=DMod.Q_ViborID_SOTRUD.AsInteger;
  DMod.Sobit_Upd.ParamByName('NAIMEN').AsString:=ComboBox1.Text;
  DMod.Sobit_Upd.ParamByName('TIP_DOCUM').AsString:=ComboBox2.Text;
  DMod.Sobit_Upd.ParamByName('DATA_SOBIT').AsString:=DateToStr(DateTimePicker1.Date);
  DMod.Sobit_Upd.ParamByName('OTVETSTV_LICO').AsString:=DBLookupComboBox1.Text;
  DMod.Sobit_Upd.ParamByName('SODERJ_DOCUM').AsString:=RichEdit1.Lines.Text;
  DMod.Sobit_Upd.ParamByName('ID_SOBIT').AsInteger:=DMod.Q_SobitID_SOBIT.AsInteger;
  DMod.Sobit_Upd.ExecProc;
  DMod.IBTransaction1.CommitRetaining;
  DMod.Q_Vibor.Close;
  DMod.Q_Vibor.Open;
  MessageBox(Self.Handle, PChar('������ ������� ��������'), PChar(''), MB_OK+MB_ICONINFORMATION);
  SobitUpd.Close;
  end
  else begin
  MessageBox(Self.Handle, PChar('���������� ������ ����'), PChar(''), MB_OK+MB_ICONWARNING);
  Exit;
  end;
end;

end.
